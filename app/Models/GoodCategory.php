<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodCategory extends Model
{
    public $primaryKey = 'id';
    protected $table = 'warehouse.good_category';
    protected $connection = MYSQL_MASTER; 
}
