<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemoSign extends Model
{
    public $primaryKey = 'id';
    protected $table = 'oppohr.memo_sign';
    protected $connection = MYSQL_MASTER; 
}
