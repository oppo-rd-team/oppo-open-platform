<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MemoList;
use App\Models\MemoDetail;
use App\Models\MemoSign;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class EmemoController extends Controller
{
    function index(){

    	$user = '5900018';
    	$pass = 'SuperGrean2020#@';

    	$system_path = 'https://hr.oppo.in.th/memo/memo-list';
    	return Redirect::away($system_path)->with(['staff_code'=>$user,'password'=>$pass]);
    }

    function mobileIndex(){
        return view("mobile/memo/memo");
    }

    function mobileView(Request $request){

        $memo = MemoList::select(
            'memo_list.*',
            DB::raw('COUNT(ms.id) AS premission_memo')
        )
        ->leftJoin('memo_sign as ms', 'memo_list.id', 'ms.memo_id')
        ->where('memo_list.id',$request->id)
        ->first();

        $detail = MemoDetail::select('*')->where('memo_id',$memo->id)->get();
        $sign = MemoSign::select('*')->where('memo_id',$memo->id)->get();

        $result["memo"] = $memo;
        $result["detail"] = $detail;
        $result["sign"] = $sign;
        $result["from_click"] = $request->from_click;

        return view("mobile/memo/memoView",$result);
    }

	function redirectToHR(Request $request){

		if ( !empty($request->all()) ) {
			// Decode data from Token
			$temp = $request->all();
			$result = json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $temp['token'])[1]))));
			$staff_code = $result->loginName;
		} else {
			$staff_code = '';
		}

		// Encode
   		$data = rtrim(strtr(base64_encode($staff_code), '+/', '-_'), '=');;
		$system_path = 'http://hr-training.oppo.in.th/memo/external-link/'.$data;
		return Redirect::away($system_path);
	}

}
