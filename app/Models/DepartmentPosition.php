<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepartmentPosition extends Model
{
	public $primaryKey = 'id';
    public $incrementing = true;
    protected $connection = MYSQL_MASTER;
    protected $table = 'oppohr.department_position';
}
