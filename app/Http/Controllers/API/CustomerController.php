<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\GoodColor;
use App\Models\Good;
use App\Models\GoodCategory;
use App\Models\ShippingAddress;
use App\Models\Distributor;
use Illuminate\Support\Facades\DB;
use Response;
use DateTime;

class CustomerController extends Controller
{
    public function create(Request $request)
    {
        $staff_code = $request->staff_code;
        $dealer_sales_sn = null;
        $order_type_id = $request->order_type_id;
        $warehouse_id = $request->warehouse_id;
        $distributor_id = $request->distributor_id;
        $shipping_id = $request->shipping_id;

        $sell_remark = $request->sell_remark;
        $request_date = date('Y-m-d H:i:s');

        $item_order = $request->item_order;

        $use_cn = $request->use_cn;
        $use_cn_list = $request->use_cn_list;

        if (empty($staff_code)) {
            echo json_encode(['status' => 400, 'messege' => "Invalid Data"]);
            exit();
        }

        $result_ds = $this->saveDealerSalesOrder($staff_code, $dealer_sales_sn, $order_type_id, $warehouse_id, $distributor_id, $shipping_id, $use_cn, $sell_remark, $request_date, $item_order, $use_cn_list);
        $row = 0;
        // //print_r($result);die;

        foreach ($result_ds as $i => $v) {
            //print_r($v['dealer_sales_sn']);die;
            if ($v['dealer_sales_sn'] != '') {
                $HOST_WMS = env('HOST_WMS', '');
                file_get_contents($HOST_WMS . "/sales/cron-create-dealer-sales-order?dealer_sales_sn=" . $v['dealer_sales_sn']);

                $result = $this->DealerSalesOrder($v['dealer_sales_sn']);

                foreach ($result as $j => $y) {

                    $url_order = "";
                    if ($y->sales_order_sn != '') {
                        $key = env('ENCRYPT_KET', '');
                        $limit_lifetime = env('ENCRYPT_TOKEN_LIFETIME', ''); //min

                        $ref = $y->sales_order_sn;
                        $lifetime = time();
                        $salt = $this->random_number();


                        $secrets = $ref . $lifetime . $salt;

                        $token = $this->encode($secrets, $key);

                        $url_order = $HOST_WMS . '/sales/print-sale?sn=' . $ref . '&view=1&lifetime=' . $lifetime . '&salt=' . $salt . '&token=' . $token;
                        $result[$j]->url_order = $url_order;
                    }
                }
            }
        }

        return response()->json($result, 200);
    }

    public static function DealerSalesOrder($dealer_sales_sn)
    {
        $select = "SELECT ds.`dealer_sales_sn`,ds.`dealer_sales_no`,ds.`sales_order_sn`,m.`sn_ref`,m.`invoice_number`
      ,m.`sales_confirm_date`,m.`finance_confirm_date`,m.`outmysql_time`,m.`invoice_time`,m.`canceled`
      ,CASE
          WHEN m.`sales_confirm_date` IS NULL THEN 1
          WHEN m.`finance_confirm_date` IS NOT NULL THEN 2
          WHEN m.`outmysql_time` IS NOT NULL THEN 3
          WHEN m.`canceled` =1 THEN 4
          ELSE 1
        END AS order_status,'' as url_order
        FROM warehouse.dealer_sales_order ds 
        LEFT JOIN warehouse.`market` m ON m.sn=ds.`sales_order_sn`
        WHERE dealer_sales_sn='" . $dealer_sales_sn . "'
        GROUP BY m.`sn`;";
        //echo $select;die;
        $result = DB::connection(MYSQL_MASTER)->select($select);
        return $result;
    }

    public function saveDealerSalesOrder($staff_id, $dealer_sales_sn, $order_type_id, $warehouse_id, $distributor_id, $shipping_id, $use_cn, $sell_remark, $request_date, $item_order, $use_cn_list)
    {
        DB::connection(MYSQL_MASTER)->beginTransaction();

        try {
            $date = new DateTime();
            $now = $date->format('Y-m-d H:i:s');

            if (!$dealer_sales_sn) {

                $dealer_sales_sn = date('YmdHis') . substr(microtime(), 2, 4);
                $result_sn = DB::connection(MYSQL_MASTER)->select("call warehouse.gen_new_running_no_ref('DS',$dealer_sales_sn)");
                $dealer_sales_no = $result_sn[0]->running_no;
                //print_r($result_sn[0]->running_no); die;
                //echo "OK1";die;
                $insert_master = array(
                    'dealer_sales_sn' => $dealer_sales_sn,
                    'dealer_sales_no' => $dealer_sales_no,
                    'order_type' => $order_type_id,
                    'warehouse_id' => $warehouse_id,
                    'distributor_id' => $distributor_id,
                    'shipping_id' => $shipping_id,
                    'use_cn' => $use_cn,
                    'status' => 1,        //1 = On Process  2= Create SO  3= Cancel
                    'sell_remark' => $sell_remark,
                    'request_by' => $staff_id,
                    'request_date' => $request_date,
                    'create_date' => $now
                );

                // print_r($insert_master);die;
                DB::connection(MYSQL_MASTER)->table('warehouse.dealer_sales_order')->insert($insert_master);

                $item = json_decode($item_order, true);
                // print_r($item);die;

                foreach ($item as $i => $v) {
                    $insert_item = array(
                        'dealer_sales_sn' => $dealer_sales_sn,
                        'cat_id' => $v['cat_id'],
                        'good_id' => $v['good_id'],
                        'good_color' => $v['good_color'],
                        'qty' => $v['qty']
                    );
                    // print_r($insert_item);die;
                    DB::connection(MYSQL_MASTER)->table('warehouse.dealer_sales_order_item')->insert($insert_item);
                }

                // print_r($use_cn_list);die;
                if ($use_cn_list != "") {
                    $cn_item = json_decode($use_cn_list, true);
                    //print_r($cn_item);die;
                    foreach ($cn_item as $i => $v) {
                        $insert_cn_item = array(
                            'dealer_sales_sn' => $dealer_sales_sn,
                            'creditnote_sn' => $v['creditnote_sn'],
                            'balance_total' => $v['balance_total'],
                            'use_total' => $v['use_total'],
                            'create_date' => $now,
                            'user_id' => $staff_id
                        );
                        //print_r($insert_cn_item);die;
                        DB::connection(MYSQL_MASTER)->table('warehouse.dealer_use_creditnote')->insert($insert_cn_item);
                    }
                }

                DB::connection(MYSQL_MASTER)->commit();
                $so_sales_no = "";
                $result[] = array('code'    => "1", 'dealer_sales_sn'    => $dealer_sales_sn, 'dealer_sales_no'    => $dealer_sales_no, 'so_sales_no'    => $so_sales_no, 'message'   => "COMPLETE");
            } else {
                // Update 

                $check = $this->checkDealerSalesOrder($dealer_sales_sn);
                $check_rs = json_decode($check, true);
                $sales_order_no =  $check_rs[0]['sales_order_no'];
                $dealer_sales_no =  $check_rs[0]['dealer_sales_no'];

                //print_r($sales_order_no);die;
                if ($sales_order_no == '') {

                    $update_master = array(
                        'order_type' => $order_type_id,
                        'warehouse_id' => $warehouse_id,
                        'distributor_id' => $distributor_id,
                        'shipping_id' => $shipping_id,
                        'use_cn' => $use_cn,
                        'sell_remark' => $sell_remark,
                        'request_by' => $staff_id,
                        'request_date' => $request_date,
                        'update_by' => $staff_id,
                        'update_date' => $now
                    );

                    //print_r($update_master);die;
                    $item = json_decode($item_order, true);

                    DB::connection(MYSQL_MASTER)->table('warehouse.dealer_sales_order')
                        ->where('dealer_sales_sn', $dealer_sales_sn)
                        ->update($update_master);


                    // Delete Old Item
                    DB::connection(MYSQL_MASTER)->table('warehouse.dealer_sales_order_item')
                        ->where('dealer_sales_sn', '=', $dealer_sales_sn)
                        ->delete();

                    // Add New Item            
                    foreach ($item as $i => $v) {
                        $insert_item = array(
                            'dealer_sales_sn' => $dealer_sales_sn,
                            'cat_id' => $v['cat_id'],
                            'good_id' => $v['good_id'],
                            'good_color' => $v['good_color'],
                            'qty' => $v['qty']
                        );

                        DB::connection(MYSQL_MASTER)->table('warehouse.dealer_sales_order_item')->insert($insert_item);
                    }

                    // DB::commit();
                    $result[] = array('code'    => "1", 'dealer_sales_sn'    => $dealer_sales_sn, 'dealer_sales_no'    => $dealer_sales_no, 'message'   => "UPDATE COMPLETE");
                } else {
                    DB::connection(MYSQL_MASTER)->rollback();
                    $result[] = array(
                        'code' => 2,
                        'message' => 'มีการบันทึกรายการสั่งซื้อแล้ว ! SO No ->' . $sales_order_no,
                    );
                }
            }
        } catch (\Exception $e) {
            //  DB::rollback();
            $result[] = array(
                'code' => 2,
                'message' => 'Cannot Save, Please try again! Message->' . $e->getMessage(),
            );
        }
        return $result;
    }

    public function checkDealerSalesOrder($dealer_sales_sn)
    {
        //
        $result = DB::connection(MYSQL_MASTER)->table('warehouse.dealer_sales_order AS ps');
        $result->select('ps.sales_order_sn', 'ps.dealer_sales_no');
        $result->selectRaw("(select m.sn_ref from warehouse.market m where m.sn=ps.sales_order_sn group by m.sn) as sales_order_no");
        $result->where('ps.dealer_sales_sn', $dealer_sales_sn);

        return $result->get();
    }

    public function filterItem(Request $request)
    {
        $distributor_id = $request->distributor_id;
        $shipping_id = null;

        $rdid = Distributor::select('id', 'title', 'name', 'add', 'tel', 'store_code', 'group_id')->where('leaf_distributor_id', $distributor_id)->first();

        $result['distributor'] = $rdid;
        $result['category'] = GoodCategory::select('id', 'name AS cat_name')->whereIn('id', [11, 12, 17])->get();
        $result['color'] = GoodColor::select('good_color.id AS color_id', 'good_color.name As color_name')->get();
        $result['shipping_address'] = $this->getShippingAddress($rdid->id, $shipping_id);
        $result['order_type'] = array(
            ["order_type_id" => 1, "order_type_name" => "FOR RETAILER"],
            ["order_type_id" => 5, "order_type_name" => "FOR APK"]
        );

        $result_good = Good::select('id', 'cat_id AS cat_id', 'name', 'color', 'price_7 AS price', 'desc', 'desc_name')->orderBy('add_time', 'desc')->get();

        foreach ($result_good as $key => $list) {
            $newArray[$key] = $list;
            $color = explode(",", $list->color);

            $newArray[$key]->color_list =  GoodColor::select('id as color_id', 'name as color_name')
                ->whereIn("id", $color)
                ->orderBy('name', 'asc')
                ->get();
        }

        $result['good'] = $result_good;

        return response()->json($result, 200);
    }

    public function getShippingAddress($distributor_id, $shipping_id)
    {
        $result = ShippingAddress::select('shipping_address.id as shipping_id', 'shipping_address.d_id as distributor_id', 'shipping_address.contact_name', 'shipping_address.address', 'sp.provice_name', 'sa.amphure_name', 'sd.district_name', 'sz.zipcode')
            ->LeftJoin('warehouse.shipping_provinces AS sp', 'sp.provice_id', '=', "shipping_address.province_id")
            ->LeftJoin('warehouse.shipping_amphures AS sa', 'sa.amphure_id', '=', "shipping_address.amphures_id")
            ->LeftJoin('warehouse.shipping_districts AS sd', 'sd.district_code', '=', "shipping_address.districts_id")
            ->LeftJoin('warehouse.shipping_zipcodes AS sz', 'sz.zip_id', '=', "shipping_address.zipcodes")
            ->where('shipping_address.d_id', $distributor_id)
            ->whereRaw("shipping_address.status is null");

        if ($shipping_id != '') {
            $result->where('shipping_address.id', $shipping_id);
        }

        $result->orderBy('shipping_address.id', 'asc');

        return $result->get();
    }

    public function getCN(Request $request)
    {
        $distributor_id = $request->distributor_id;
        $creditnote_type = $request->creditnote_type;

        $select = "SELECT c.`distributor_id`,c.`creditnote_type`,c.`creditnote_sn`,c.`total_amount`,c.`balance_total`,c.`use_total` 
		FROM warehouse.credit_note c 
		WHERE 1=1
		AND c.`status`=1
		AND c.`balance_total`>0
		AND c.`distributor_id`='" . $distributor_id . "'";

        if ($creditnote_type != "") {
            $select .= " and c.`creditnote_type`='" . $creditnote_type . "'";
        }

        $select .= " ORDER BY c.`creditnote_sn`";

        $result = DB::connection(MYSQL_MASTER)->select($select);
        return $result;
    }

    function random_number()
    {
        $min = 1000;
        $max = 9999;
        return rand($min, $max);
    }

    function encode($string, $key)
    {
        $key = sha1($key);
        $hash = "";
        $strLen = strlen($string);
        $keyLen = strlen($key);
        $j = 0;
        for ($i = 0; $i < $strLen; $i++) {
            $ordStr = ord(substr($string, $i, 1));
            if ($j == $keyLen) {
                $j = 0;
            }
            $ordKey = ord(substr($key, $j, 1));
            $j++;
            $hash .= strrev(base_convert(dechex($ordStr + $ordKey), 16, 36));
        }
        return $hash;
    }
}
