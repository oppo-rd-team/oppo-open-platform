<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\OppoStaff;
use App\Models\Salary;
use App\Models\MemoList;
use App\Models\MemoDetail;
use App\Models\MemoCC;
use App\Models\MemoAttachFile;
use App\Models\MemoSign;
use App\Models\User;
use App\Models\Department;
use App\Models\DepartmentPosition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use DateTime;

class EMemoController extends Controller
{
    public function __construct()
    {
        // define('ADMINISTRATOR', 1);
        // define('DEPARTMENTS', 2);
        // define('POSITION', 3);
        // define('STAFF_ALL', 1);
        // define('BY_STAFF', 6);
        // define('LIMITATION', 30);
        // define('WAIT_FOR_APPROVAL', 2);
        // define('APPROVAL', 3);
        // define('CANCELED', 7);
        // define('REJECT', 5);
    }

    public function Approval(Request $request)
    {
        $user             = auth()->user();
        $user             = OppoStaff::profile($user->staff_code);
        $app_user         = $user->staff_code;
        $memo_number      = $request->memo_number;
        $memo_type      = $request->memo_type;
        $memo_department      = $request->memo_department;
        $status             = $request->memo_status;

        $users = User::select("staff_code", "firstname", "lastname");
        $department = Department::select("id", "name");

        $memo = MemoList::select(
            'memo_list.id as doc_id',
            'memo_list.*',
            'ms.*',
            "usc.firstname",
            "usc.lastname",
            "usc.staff_code",
            "dep.name as department_name",
            DB::raw('COUNT(ms.id) AS premission_memo')
        );
        $memo->leftJoin('memo_sign as ms', 'memo_list.id', 'ms.memo_id');

        $memo->leftJoinSub($users, 'usc', function ($join) {
            $join->on('usc.staff_code', '=', 'memo_list.created_by');
        });
        $memo->leftJoinSub($department, 'dep', function ($join) {
            $join->on('memo_list.department', '=', 'dep.id');
        })->where('status', 2);

        if (!empty($request->date_from) && !empty($request->date_to)) {
            $memo->whereBetween(DB::raw('DATE(memo_list.created_date)'), [$request->date_from, $request->date_to]);
        }

        if ($user->group_id == 1) {
            $memo->where('memo_list.status', WAIT_FOR_APPROVAL);
        } else {
            $memo->where('ms.app_user', $app_user);
        }

        if (!empty($memo_number)) {
            $memo->where('memo_list.number', 'like', '%' . trim($memo_number) . '%');
        }

        if (!empty($status)) {
            $memo->where('memo_list.status', $status);
        }

        if (!empty($memo_department)) {
            $memo->where('memo_list.department', $memo_department);
        }

        if (!empty($memo_type)) {
            $memo->where('memo_list.memo_type', $memo_type);
        }

        $memo->orderBy('publish_date', 'asc');
        $memo->orderBy('status', 'asc');
        $memo->orderBy('updated_date', 'DESC');
        $memo->groupBy('memo_list.id');
        $result = $memo->get();

        $array_res = array();
        $p = 0;
        foreach ($result as $key => $value) {
            if ($this->checkApprove($value->doc_id, $user->staff_code)) {
                if ($value->status == WAIT_FOR_APPROVAL) {
                    $array_res[] = $value;

                    $date1 = new DateTime(date("Y-m-d", strtotime($value->created_date)));
                    $date2 = new DateTime(date("Y-m-d"));
                    $date_passed = $date1->diff($date2);
                    $array_res[$p]['date_passed'] = $date_passed->days;
                    $p++;
                }
            }
        }

        return response()->json($array_res, 200);
    }

    public function memoOther(Request $request)
    {
        $user             = auth()->user();
        $user             = OppoStaff::profile($user->staff_code);

        $staff_code             = $user->staff_code;
        $memo_type            = $request->memo_type;

        $memo_number            = $request->memo_number;
        $memo_type              = $request->memo_type;
        $memo_department        = $request->memo_department;
        $status                 = $request->memo_status;
        $fetchType              = $request->type;
        

        $depar_position = DepartmentPosition::select('id as position')->where('code', $user->department_position_code)->first();

        if ($fetchType == 'memo_cc') {
            $memo = MemoCC::select(
                'memo_list.id as doc_id',
                'memo_list.*'
            )->join('memo_list', function ($join) use ($staff_code) {
                $join->on('memo_cc.memo_id', 'memo_list.id')
                    ->where('memo_cc.cc_to', $staff_code);
            });
        } else {

            $memo_list_sing = MemoList::select('memo_id')
                ->join('oppohr.memo_sign', function ($join) use ($user) {
                    $join->on('memo_sign.memo_id', 'memo_list.id')
                        ->where('memo_sign.app_user', $user->staff_code)
                        ->Where(function($query) use ($user) {
                            $query->whereNotNull('memo_sign.approve_date');
                            $query->orWhereNotNull('memo_sign.reject_date');
                        });
                })
                ->groupBy('memo_sign.memo_id')
                ->get();

            $memo_list_visi = MemoList::select('memo_id')
                ->join('oppohr.memo_visibility', function ($join) use ($user, $depar_position, $staff_code) {
                    $join->on('memo_visibility.memo_id', 'memo_list.id')
                        ->whereIn('memo_visibility.visibility', [$user->department, 1, $depar_position->position, $staff_code]);
                })
                ->groupBy('memo_visibility.memo_id')
                ->get();

            $list_memo_id = [];
            foreach ($memo_list_sing as $key => $item) {
                array_push($list_memo_id, $item->memo_id);
            }
            foreach ($memo_list_visi as $key => $item) {
                array_push($list_memo_id, $item->memo_id);
            }

            $memo = MemoList::select(
                'memo_list.id as doc_id',
                'memo_list.*',
                "usc.firstname",
                "usc.lastname",
                "usc.staff_code",
                "dep.name as department_name",
            )
                ->leftJoin('users as usc', 'memo_list.created_by', 'usc.staff_code')
                ->leftJoin('department as dep', 'memo_list.department', 'dep.id')
                ->whereIn('memo_list.id', $list_memo_id);

        }

        if (!empty($memo_number)) {
            return "memo_number";
            $memo->where('memo_list.number', 'like', '%' . trim($memo_number) . '%');
        }

        if (!empty($request->date_from) && !empty($request->date_to)) {
            $memo->whereBetween(DB::raw('DATE(memo_list.created_date)'), [$request->date_from, $request->date_to]);
        }

        if (!empty($status)) {
            $memo->where('memo_list.status', $status);
        }

        if (!empty($memo_department)) {
            return "memo_department";
            $memo->where('memo_list.department', $memo_department);
        }

        if (!empty($memo_type)) {
            return "memo_type";
            $memo->where('memo_list.memo_type', $memo_type);
        }

        // if ($title) {
        //     $memo->where('title', 'like', '%' . $title . '%');
        // }

        $memo->orderBy('memo_list.created_date', 'DESC');
        $memo->groupBy('memo_list.id');

        $array_res = [];
        foreach ($memo->get() as $key => $item) {

            $array_res[] = $item;
            $date1 = new DateTime(date("Y-m-d", strtotime($item->created_date)));
            $date2 = new DateTime(date("Y-m-d"));
            $date_passed = $date1->diff($date2);
            $array_res[$key]['date_passed'] = $date_passed->days;
            // $staff = OppoStaff::select('name_en', 'staff_code')->where('staff_code', $item->created_by)->first();
            // if ($staff) {
            //     $item['firstname'] = $staff->name_en;
            //     $item['staff_code'] = $staff->staff_code;
            // }

            // $department = Department::select('*')->where('id', $item->department)->first();
            // if ($department) {
            //     $item['department_name'] = $department->name;
            // }

            // $memo_sign = MemoSign::select('*')
            //     ->where('memo_id', $item->doc_id)
            //     ->where('app_user', $item->staff_code)->first();

            // if ($memo_sign) {
            //     $array_res[$key]['approve_date'] = $memo_sign->approve_date;
            //     $array_res[$key]['reject_date'] = $memo_sign->reject_date;
            //     $array_res[$key]['reject_remark'] = $memo_sign->reject_remark;
            //     $array_res[$key]['premission_memo'] = 1;
            // }
        }

        return response()->json($array_res, 200);
    }

    public function checkApprove($doc_id, $staff_code)
    {
        $sign = MemoSign::where('memo_id', $doc_id)->get();

        foreach ($sign as $key => $item) {
            if ($item->app_user == $staff_code) {
                if ($key > 0) {
                    if ($sign[$key - 1]->approve_date == null) {
                        return false;
                    }
                }
                if ($item->approve_date != null) {
                    return false;
                }
            }
        }
        return true;
    }


    public function action(Request $request)
    {
        if ($request->type == 1) {
            return $this->Approve($request);
        } else if ($request->type == 2) {
            return $this->Reject($request);
        } else {
            return $this->Cancel($request);
        }
    }

    public function Approve(Request $request)
    {
        $approve = [
            'approve_date' => date('Y-m-d H:i:s'),
        ];

        $user             = auth()->user();
        $user             = OppoStaff::profile($user->staff_code);
        $id =              $request->id;

        $count_approve = MemoSign::where('memo_id', $id)->whereNull('approve_date')->count();
        if ($count_approve == 1) {
            $attach_file = MemoAttachFile::where("memo_id", $id)->first();
            if (!empty($attach_file)) {
                $result_att = $this->callFilePurchase(1, $id);
                if ($result_att['success'] == 0) {
                    $this->callFilePurchase(1, $id);
                }
            }
        }

        $app = MemoSign::where('memo_id', $id)
            ->where('app_user', $user->staff_code)
            ->update($approve);

        $doc = MemoList::where('id', $id)->where('template', 1)->first(); // 1=เครื่องอภินันทนาการ

        $CheckApprove = MemoSign::where('memo_id', $id)
            ->whereNull('approve_date')
            ->get();

        $array = count($CheckApprove);
        if ($array == 0) {
            $app = MemoList::where('id', $id)
                ->update(['status' => APPROVAL]);

            if ($doc) {
                $create_pr = $this->CreateComplimentaryPr($id);

                MemoList::where('id', $id)
                    ->update([
                        'pr_number' => $create_pr['pr_id'],
                        'po_number' => $create_pr['po_code'],
                        'status'    => PUBLISH
                    ]);

                // 5902612 ศุภการ
                // 5802086 ชลธิชา
                $auto = ['5902612', '5802086'];
                for ($i = 0; $i < count($auto); $i++) {
                    $data_p = [
                        'memo_id'       => $id,
                        'visibility'    => $auto[$i],
                        'type'          => 6
                    ];
                    DB::connection(MYSQL_MASTER)->table('memo_visibility')->insert($data_p);
                }
            }
        }

        if ($app) {
            return response()->json(['status' => 'Success'], 200);
        } else {
            return response()->json(['status' => 'Error'], 401);
        }
    }

    public function Reject(Request $request)
    {
        $user      = auth()->user();
        $user      = OppoStaff::profile($user->staff_code);
        $id        = $request->id;
        $reason    = $request->reason;

        $count_approve = MemoSign::where('memo_id', $id)->whereNull('approve_date')->count();
        if ($count_approve == 1) {
            $attach_file = MemoAttachFile::where("memo_id", $id)->where("type", 2)->first();
            if (!empty($attach_file)) {
                $result_att = $this->callFilePurchase(2, $id);
                if ($result_att['success'] == 0) {
                    $this->callFilePurchase(2, $id);
                }
            }
        }

        $data = MemoSign::where('memo_id', $id)
            ->where('app_user', $user->staff_code)
            ->first();

        $re = 0;
        if ($data) {
            $reject = [
                'reject_date'   => date('Y-m-d h:i:s'),
                'reject_remark' => $reason,
            ];

            $app =  DB::connection(MYSQL_MASTER)->table('memo_sign')
                ->where('memo_id', $id)
                ->where('app_user', $user->staff_code)
                ->update($reject);

            $app2 =  DB::connection(MYSQL_MASTER)->table('memo_sign')
                ->where('memo_id', $id)
                ->update(['approve_date' => NULL]);

            $list = [
                'status'        => REJECT,
                'updated_date'  => date('Y-m-d H:i:s')
            ];

            $app =  DB::connection(MYSQL_MASTER)->table('memo_list')
                ->where('id', $id)
                ->update($list);
            $re = $app;
        } else {
            $list = [
                'status'        => REJECT,
                'updated_date'  => date('Y-m-d H:i:s')
            ];

            $app =  DB::connection(MYSQL_MASTER)->table('memo_list')
                ->where('id', $id)
                ->update($list);
            $re = $app;
        }

        return $re;
    }

    public function Cancel(Request $request)
    {
        $user             = auth()->user();
        $user             = OppoStaff::profile($user->staff_code);
        $id               = $request->id;
        $reason           = $request->reason;

        $me = MemoList::where('id', $id)->first();

        $count_approve = MemoSign::where('memo_id', $id)->whereNull('approve_date')->count();
        if ($count_approve == 1) {
            $attach_file = MemoAttachFile::where("memo_id", $id)->where("type", 2)->first();
            if (!empty($attach_file)) {
                $result_att = $this->callFilePurchase(2, $id);
                if ($result_att['success'] == 0) {
                    $this->callFilePurchase(2, $id);
                }
            }
        }

        if ($user->staff_code == @$me->created_by) {
            $data = [
                'canceled_by'       => $user->staff_code,
                'canceled_date'     => date('Y-m-d H:i:s'),
                'canceled_reason'   => $reason,
                'status'            => CANCELED
            ];

            $update = DB::connection(MYSQL_MASTER)->table('memo_list')->where('id', $id)->update($data);
            if ($update == 1) {
                $email = $email_sent = [];
                $sign = DB::connection(MYSQL_MASTER)->table('memo_sign')->select('app_user')->where('memo_id', $id)->whereNotNull('approve_date')->get();

                foreach ($sign as $key => $value) {
                    $email[] = getEmailUser($value->app_user);
                }
                $email_sent = array_filter($email);

                if ($user->email) {
                    $email_sent[] = $user->email;
                }

                  // foreach ($email_sent as $key => $email) {
                    //     Mail::send('mobile.memo.mail', ["memo" => $me, 'memoid' => $id, 'user' => $user, 'reason' => $reason], function ($message) use ($email, $me) {
                    //         $message->to($email, $email)
                    //             ->subject('Memo ' . $me->number . ' was canceled. ' . $me->title);
                    //         $message->from('mailer@oppo.in.th', 'HR System');
                    //     });
                    // }
            }
            return response()->json(['10'], 200);
        } else {
                $data = [
                    'canceled_by'       => $user->staff_code,
                    'canceled_date'     => date('Y-m-d H:i:s'),
                    'canceled_reason'   => $reason,
                    'status'            => CANCELED
                ];
                $update = DB::connection(MYSQL_MASTER)->table('memo_list')->where('id', $id)->update($data);
                if ($update == 1) {
                    $email = $email_sent = [];
                    $sign = DB::connection(MYSQL_MASTER)->table('memo_sign')->select('app_user')->where('memo_id', $id)->whereNotNull('approve_date')->get();

                    foreach ($sign as $key => $value) {
                        $email[] = getEmailUser($value->app_user);
                    }
                    $email_sent = array_filter($email);

                    if ($user->email) {
                        $email_sent[] = $user->email;
                    }

                    // foreach ($email_sent as $key => $email) {
                    //     Mail::send('mobile.memo.mail', ["memo" => $me, 'memoid' => $id, 'user' => $user, 'reason' => $reason], function ($message) use ($email, $me) {
                    //         $message->to($email, $email)
                    //             ->subject('Memo ' . $me->number . ' was canceled. ' . $me->title);
                    //         $message->from('mailer@oppo.in.th', 'HR System');
                    //     });
                    // }

                    return response()->json(['10'], 200);
                } else {
                    return response()->json(['1'], 200);
                }
        }
    }

    public function CreateComplimentaryPr($id)
    {
        $user                 = Auth::user();
        $memo_list            = DB::table('memo_list')->where('id', $id)->first();
        $memo_detail          = DB::table('memo_detail')->where('memo_id', $id)->first();
        $memo_t_complimentary = DB::table('memo_t_complimentary')->where('memo_id', $id)->get();

        foreach ($memo_t_complimentary as $key => $value) {
            $discount = '';
            if ($value->discount) {
                if ($value->unit == 1) {

                    $discount = ($value->price * $value->qty) * $value->discount / 100;
                } else {
                    $discount = $value->discount;
                }
            }
            $product[] = [
                'product'       => $this->getProductName($value->product),
                'description'   => 'เบิกเครื่องอภินันทนาการ ' . $this->getProductName($value->product) . ' สี ' . $this->getColorName($value->product_color),
                'qty'           => $value->qty,
                'price'         => $value->price,
                'discount'      => $discount,
                'unit'          => 'เครื่อง'
            ];
        }
        $_token =  "klfleifxDSUYEW9WE67898BVNVFPNG";
        $quotation = $this->memoComplimentaryQuotation($id);
        // getProductName
        $data = [
            '_token'            => $_token,
            'date_urgent'       => $memo_detail->date_use,
            'area_id'           => 48,
            'product'           => @$product,
            'department'        => $memo_list->department,
            'staff_code'        => $memo_list->created_by,
            'quotation'         => 'Complimentary/' . $quotation,
            'remark'            => $memo_list->number . '-' . $memo_list->title,
            'delivery_point'    => 'เลขที่ 89  อาคารเอไอเอ แคปปิตอล เซ็นเตอร์ ชั้น 31 ห้อง 5-7  ถนนรัชดาภิเษก แขวงดินแดง เขตดินแดง กรุงเทพมหานคร 10400'
        ];

        $data = (json_encode($data, true));
        // print_r($data);
        // die;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.trade.oppo.in.th/purchasing/create-hr-po");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);

        $pr_output = json_decode($server_output, true);
        return $pr_output;
    }

    public function memoComplimentaryQuotation($id)
    {

        // $path = '/var/www/html/purchase/application/../public/photo/pr_qt/Complimentary';
        $path = '/home/webroot/purchase/application/../public/photo/pr_qt/Complimentary';
        $date = date('YmdHis');

        $url = "http://203.150.225.139:3008/url-to-image?memo_id=" . $id . "&path=" . $path . "&date=" . $date;
        // echo $url;exit();
        // create curl resource
        $ch = curl_init();
        // dd($url);
        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);
        // dd($output);
        // close curl resource to free up system resources
        curl_close($ch);


        return $output;
    }

    public function callFilePurchase($status, $memo_id)
    {
        $data = [
            'status'        => $status,
            'memo_id'       => $memo_id
        ];

        $data = (json_encode($data, true));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://withholding-training.oppo.in.th/api/update-status-memo");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);

        curl_close($ch);

        $pr_output = json_decode($server_output, true);
        return $pr_output;
    }

    public function getFilter(Request $request)
    {
        $result['memo_type'] = array(
            ["id" => 1, "name" => "Request for Support"],
            ["id" => 2, "name" => "Sales Policy (นโยบายฝ่ายขาย)"],
            ["id" => 3, "name" => "Price Update (ปรับราคา)"],
            ["id" => 4, "name" => "Punishment (ลงโทษ)"],
            ["id" => 5, "name" => "Update Sales Area (พื้นที่การขาย)"],
            ["id" => 6, "name" => "Company Policy (คำสังด่วน)"],
            ["id" => 7, "name" => "Commissions(ค่าคอมฯ)"],
            ["id" => 8, "name" => "EOL"],
            ["id" => 9, "name" => "Special Agreement (ข้อตกลงพิเศษ)"],
            ["id" => 10, "name" => "Promotion Changing (ปรับตำแหน่ง)"],
            ["id" => 11, "name" => "Finance Purchasing (จ่ายเงิน)"],
            ["id" => 12, "name" => "New Product (สินค้าใหม่)"],
            ["id" => 13, "name" => "Pre EOL"],
            ["id" => 14, "name" => "Logistic Claim"],
            ["id" => 15, "name" => "Time Certificate"],
            ["id" => 16, "name" => "Terminate Distributor"],
            ["id" => 17, "name" => "HR Policy"],
            ["id" => 18, "name" => "Complimentary-อภินันทนาการ"],
            ["id" => 19, "name" => "Employee Welfare - สวัสดิการพนักงาน"],
            ["id" => 20, "name" => "Requisition Order"],
            ["id" => 21, "name" => "Request revise information in WMS"],
            ["id" => 22, "name" => "Partner Welfare - สวัสดิการพาร์ทเนอร์"]
        );

        $result['memo_status'] = array(
            ["id" => 1, "name" => "Drafted"],
            ["id" => 2, "name" => "Waiting for Approval"],
            ["id" => 3, "name" => "Approved"],
            ["id" => 4, "name" => "Published"],
            ["id" => 5, "name" => "Reject"],
            ["id" => 6, "name" => "Unpublished"],
            ["id" => 7, "name" => "Canceled"]
        );

        $result['department'] = Department::select('id', 'name')->get();
        $result['date_from'] = date("Y-01-01");
        $result['date_to'] = date("Y-m-d");

        return response()->json($result, 200);
    }
}
