<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>E-memo</title>
    <!-- Scripts -->
    <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Montserrat">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style type="text/css">
        .div-main {
            margin: 15px;
        }

        .div-in {
            margin: 5px;
        }

        .div-f-center {
            margin: auto;
            left: 40%;
            float: center;
            top: 40%;
            position: fixed;
        }

        .div-button-center {
            left: 16%;
            bottom: 2%;
            position: fixed;
        }

        .margin-button {
            margin: 5%;
        }

        body {
            background-color: #fff;
            font-size: 13px;
            font-family: 'Montserrat', serif;
            text-align: right;
        }

        @page {
            margin: 0;
        }


        .fr-box.fr-basic.fr-top .fr-wrapper {
            min-height: 700px;
        }

        .create_by {
            color: #000;
            font-size: 10px;
            opacity: 1;
            z-index: 99;
            position: fixed;
            /*position: absolute;*/
            margin-left: 55px;
            margin-top: 50px;
            pointer-events: none;
        } */

        .date {
            width: 250px;
            float: right;
            text-align: right
        }

        .box-detail {
            padding: 0px 50px 0px 50px;
            word-wrap: break-word;
            line-height: 24px
        }

        .box-btn {
            position: fixed;
            right: 0px;
            top: 30%;
            text-align: right;
        }

        .box-btn div button {
            height: 50px;
            width: 120px;
            margin-top: 0;
        }

        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 20px;
        }

        .box-btn div a {
            height: 48px;
            padding: 15px;
            width: 120px;
            margin-top: 0;
            border-radius: 0;
            border: 0;
            box-shadow: 2px 2px #888888 !important;
        }

        .header {
            font-weight: 800;
            margin-right: 8px;
            /*font-size: 13px;*/
        }

        .sign p {
            margin: 0px;
        }

        .box-big-sign {
            width: 20cm;
            height: 95px;
            padding: 0 50px;
            font-size: 12px;
            position: relative;

        }

        .box-sign {
            padding: 0 10;
            margin-top: 0px;
            color: #000;
            width: 210px;
            height: 50px;
            position: absolute;

        }

        .no_page {
            position: absolute;
            margin-top: 20px;
            margin-left: 20cm;
            display: none;
        }

        .sign-page1,
        .sign-page2,
        .sign-page3 {
            display: none;
        }

        .box-big-sign p {
            margin: -8px;
            font-size: 12px;

        }

        .bg-text {
            color: #d3d3d352;
            font-size: 130px;
            transform: rotate(320deg);
            -webkit-transform: rotate(320deg);
            opacity: 1;
            z-index: 99;
            /* position: fixed; */
            position: absolute;
            margin-left: 220px;
            /*margin-top: 400px;*/
            pointer-events: none;
        }

        .importance {
            color: red;
            font-size: 30px;
            font-weight: bold;
            z-index: 99;
            margin-top: -60px;
            text-align: right;
            margin-left: 565px;
            width: 200px;
            pointer-events: none;
            position: absolute;
        }

        .header-title {
            font-size: 13px;
            width: 50%;
            float: left;
        }

        .header-memo-number {

            font-weight: 600;
            line-height: 32px;
            font-size: 25px;
            width: 50%;
            float: left;
        }

        page {
        /* background-repeat: no-repeat; */
        background-size: 21cm;
        background-color:  white;*/
            padding: 0px;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            /*box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
            display: block;
            margin: 0 auto;
            /*border: 1px solid #ccc;*/
            /*page-break-after: always; */
        }

        page[size="A4"] {
            width: 22.5cm;
            height: 32cm;
        }

        @media print {
            body {
                background-color: #fff;
                font-size: 13px;
                font-family: 'THSarabunNew', 'Arial', 'cn2', 'cn1', 'cn3', 'cn4', 'Angsana', sans-serif;
            }

            .xx {
                display: none
            }

            .bg-text {
                color: #d3d3d352;
                font-size: 130px;
                transform: rotate(320deg);
                -webkit-transform: rotate(320deg);
                opacity: 1;
                z-index: 99;
                /* position: fixed; */
                position: absolute;
                margin-left: 220px;
                margin-top: 400px;
                pointer-events: none;
            }

            .box-detail {
                padding: 0px 50px 0px 50px;
                height: 60%;
            }

            page[size="A4"] {
                width: 22.5cm;
                height: 29.5cm;
                /*height: 32.5cm;*/
            }

            .create_by {
                font-size: 10px;
                /*opacity: 1;*/
                z-index: 999;
                position: fixed;
                /*position: absolute;*/
                margin-left: 55px;
                margin-top: 0px;


            }


            .showSign {
                display: block;

            }
        }

        .oppo_header {
            width: 100% !important;

        }


        .p-0 {
            padding: 0px !important;
        }

        .pic_sign {
            width: 150px;
            margin: 0 auto;
            margin-bottom: -15px
        }

        .no_page_show {
            display: block;
        }

        .content1,
        .content2,
        .content3 {
            margin-top: 25px;
            display: inline-block;

        }

        .showSign {
            display: block;
        }

        table,
        th,
        td {
            border: 1px solid #ccc;
            border-collapse: collapse;
            padding: 0px 8px;
        }

        p {
            font-size: 13px;
            margin: 0px
        }

        .show {
            display: block;
        }

        table,
        tr,
        td>p {
            margin: 0px;
        }

        footer {
            margin-bottom: 10px;
            width: 100%;
            position: fixed;
            bottom: 0;
            padding: 0px 50px 0px 50px;
            font-size: 9px !important;
        }

        #print-jpg {
            width: 100%;
            height: auto;
            text-align: justify;
        } 

    </style>

</head>

<body>
    <div id="loading" class="spinner-border text-success div-f-center" style="width: 5rem; height: 5rem;" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <?php

    $count = count($detail);
    $no_page_show = '';
    if ($count > 1) {
        $no_page_show = 'no_page_show';
    }
    $importance = '';
    if ($memo->importance_doc) {
        switch ($memo->importance_doc) {
            case '2':
                $importance = 'สำคัญมาก';
                break;
            case '3':
                $importance = 'ลับที่สุด';
                break;
    
            default:
                $importance = '';
                break;
        }
    }
    
    $sign_page1 = $sign_page2 = $sign_page3 = '';
    
    if ($memo->show_sign > 1) {
        switch ($memo->show_sign) {
            case '2':
                $sign_page1 = 'showSign';
                $sign_page2 = 'showSign';
                $sign_page3 = 'showSign';
                break;
            case '3':
                if ($count == 1) {
                    $sign_page1 = 'showSign';
                }
                if ($count == 2) {
                    $sign_page2 = 'showSign';
                }
                if ($count == 3) {
                    $sign_page3 = 'showSign';
                }
    
                break;
            default:
                # code...
                break;
        }
    }
    
    $preview_show = $red = $preview_time = '';
    if (in_array(@$memo->status, [CANCELED])) {
        $preview_show = 'Canceled';
        $preview_time = $memo->canceled_date;
        $red = 'red';
    } elseif (in_array(@$memo->status, [DRAFTED, WAIT_FOR_APPROVAL, REJECT, UNPUBLISH])) {
        $preview_show = 'Drafted';
    } else {
        $preview_show = '';
    }
    
    $open_button = true;
    if (@$memo->status != 2) {
        $open_button = false;
    } else {
        if (@$memo->premission_memo == 0) {
            $open_button = false;
        }

        if (@$from_click != 0) {
            $open_button = false;
        }
    }
    ?>

    <?php
    if ($open_button) {
        ?>

    <div class="d-flex justify-content-center align-items-end div-button-center" id="btn-action" >
        <button type="button" class="btn btn-success text-white margin-button"
            onclick="oncickMemo(1,{{ @$memo->id }})">Approve</button>
        <button type="button" class="btn btn-danger text-white margin-button"
            onclick="oncickMemo(2,{{ @$memo->id }})">Reject</button>
        <button type="button" class="btn btn-danger text-white margin-button"
            onclick="oncickMemo(3,{{ @$memo->id }})">Cancel</button>
    </div>

    <?php
    } 
    ?>

    <div class="container-fluid">
        <div class="row" id="print-jpg">
            <div id="a4_page">
                <div class="page">
                    <page size="A4">
                        <p style="line-height: 30px;" class="bg-text {{ $red }}">{{ $preview_show }}<br><span
                                style="font-size: 20px;margin-left: 25px">{{ $preview_time }}</span></p>
                        <div class="no_page {{ $no_page_show }}">Page 1/<span
                                class="noPage">{{ $count }}</span></div>
                        <div class="oppo_header_img"><img class="oppo_header"
                                src="{{ URL_HR }}/img/oppo_header.png" width="100%" alt=""></div>
                        <div class="importance">{{ @$importance }}</div>
                        <div class=" box-detail">
                            <div class="col-lg-12 ">
                                <div class="header-memo-number">{{ @$memo->number }}
                                    {{ @$memo->memo_type == FINANCE_PURCHASE ? ' / ' . @$memo->ref2 : '' }}</div>
                                <div class="header-title">
                                    <div class="date" style="margin-bottom: 20px; ">
                                        <p>วันที่ {{ @$memo->day }} {{ getMonth($memo->month) }}
                                            {{ @$memo->year }}
                                        </p>
                                        <p>Date {{ @$memo->day }} {{ getMonthEN($memo->month) }}
                                            {{ @$memo->year - 543 }}
                                        </p>
                                        {{-- <span>วันที่</span><span style="margin: 0 5px" class="day">{{@$memo->day}}</span><span style="margin: 0 5px">{{getMonth($memo->month)}}</span><span style="margin: 0 5px">{{@$memo->year}}</span>
                 <br/>
                   <span>Date</span><span style="margin: 0 5px" class="day">{{$memo->day}}</span><span style="margin: 0 5px">{{getMonthEN($memo->month)}}</span><span style="margin: 0 5px">{{$memo->year-543}}</span> --}}
                                    </div>
                                </div>

                                <div class="clearfix">...</div>
                                <div class="form-group  p-0">
                                    <div><span class="header">เรื่อง</span> <span
                                            class="title">{{ $memo->title }}</span></div>

                                </div>
                                <div class="form-group  p-0">
                                    <div><span class="header">เรียน</span> <span
                                            class="to">{{ $memo->to }}</span></div>
                                </div>
                                <div class="form-group  content1 fr-view p-0">

                                    {!! @$detail[0]->detail !!}

                                </div>

                            </div>
                        </div>
                        <?php
                        $appSign = [];
                        $ssd = count($sign);
                        for ($i = 0; $i < count($sign); $i++) {
                            $appSign[$sign[$i]->position_sign]['user'] = $sign[$i]->app_user;
                            $appSign[$sign[$i]->position_sign]['app_date'] = $sign[$i]->approve_date;
                        }
                        ?>
                        <div class=" sign-page1  {{ $sign_page1 }}"
                            style="font-size: 12px; {{ $ssd <= 3 ? 'margin-top: 15%' : '' }}">


                            {{-- date('d-m-Y',strtotime($sign[$i]->approve_date)); --}}
                            @if (@$appSign[1] || @$appSign[2] || @$appSign[3])
                                <div style="clear: both;้"></div>
                                <div class="box-big-sign">

                                    @if (@$appSign[1])
                                        <div class="box-sign box-sign1" style="">

                                            @if ($appSign[1]['app_date'])
                                                @if (@$from == 'app')
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo_signature/{{ $appSign[1]['user'] }}.png"
                                                            height="80px"></div>
                                                @else
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[1]['user'] }}"
                                                            height="80px"></div>
                                                @endif
                                                <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ $appSign[1]['user'] }}" height="80px" ></div> -->
                                                <!-- <div class="pic_sign"><img src="/signature/{{ $appSign[1]['user'] }}.png" height="80px" ></div> -->
                                            @else
                                                <div style="height: 85px"></div>
                                            @endif
                                            <span>ลงชื่อ ............................................ ผู้อนุมัติ </span>
                                            <p style="text-align: center;">(<span
                                                    class="sign1">{{ getUserApproveName(@$appSign[1]['user']) }}</span>)
                                            </p>
                                            <p style="text-align: center;;margin-top:-5px">
                                                ({{ getUserPosition(@$appSign[1]['user']) }})</p>

                                            @if ($appSign[1]['app_date'])
                                                <p style="text-align: center ;margin-top:-5px">
                                                    {{ date('d/m/Y', strtotime(@$appSign[1]['app_date'])) }}</p>
                                            @endif
                                            @if ($memo->department == '2')
                                                <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd</p>
                                            @endif

                                        </div>
                                    @endif

                                    @if (@$appSign[2])
                                        <div class="box-sign box-sign2" style="left: 320px;">
                                            @if (@$appSign[2]['app_date'])
                                                @if (@$from == 'app')
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo_signature/{{ $appSign[2]['user'] }}.png"
                                                            height="80px"></div>
                                                @else
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[2]['user'] }}"
                                                            height="80px"></div>
                                                @endif
                                            @else
                                                <div style="height: 85px"></div>
                                            @endif
                                            <span>ลงชื่อ ............................................ ผู้อนุมัติ </span>
                                            <p style="text-align: center;">(<span
                                                    class="sign2">{{ getUserApproveName(@@$appSign[2]['user']) }}</span>)
                                            </p>
                                            <p style="text-align: center;;margin-top:-5px">
                                                ({{ getUserPosition(@$appSign[2]['user']) }})</p>
                                            @if (@$appSign[2]['app_date'])
                                                <p style="text-align: center ;margin-top:-5px">
                                                    {{ date('d/m/Y', strtotime(@$appSign[2]['app_date'])) }}</p>
                                            @endif
                                            @if ($memo->department == '2')
                                                <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd</p>
                                            @endif
                                        </div>
                                    @endif
                                    @if (@$appSign[3])
                                        <div class="box-sign box-sign3" style="left: 590px;">
                                            @if (@$appSign[3]['app_date'])
                                                @if (@$from == 'app')
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo_signature/{{ $appSign[3]['user'] }}.png"
                                                            height="80px"></div>
                                                @else
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[3]['user'] }}"
                                                            height="80px"></div>
                                                @endif
                                                <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[3]['user'] }}" height="80px" ></div> -->
                                            @else
                                                <div style="height: 85px"></div>
                                            @endif
                                            <span>ลงชื่อ ............................................ ผู้อนุมัติ </span>
                                            <p style="text-align: center;">(<span
                                                    class="sign3">{{ getUserApproveName(@$appSign[3]['user']) }}</span>)
                                            </p>
                                            <p style="text-align: center;;margin-top:-5px">
                                                ({{ getUserPosition(@$appSign[3]['user']) }})</p>
                                            @if (@$appSign[3]['app_date'])
                                                <p style="text-align: center ;margin-top:-5px">
                                                    {{ date('d/m/Y', strtotime(@$appSign[3]['app_date'])) }}</p>
                                            @endif
                                            @if ($memo->department == '2')
                                                <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd</p>
                                            @endif
                                        </div>
                                    @endif

                                </div>

                            @endif
                            @if (@$appSign[4] || @$appSign[5] || @$appSign[6])
                                <div class="box-big-sign" style="margin-top: 60px">

                                    @if (@$appSign[4])
                                        <div class="box-sign box-sign4" style="">
                                            @if (@$appSign[4]['app_date'])
                                                <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[4]['user'] }}" height="80px" ></div> -->
                                                @if (@$from == 'app')
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo_signature/{{ $appSign[4]['user'] }}.png"
                                                            height="80px"></div>
                                                @else
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[4]['user'] }}"
                                                            height="80px"></div>
                                                @endif
                                            @else
                                                <div style="height: 85px"></div>
                                            @endif
                                            <span>ลงชื่อ ............................................ ผู้อนุมัติ </span>
                                            <p style="text-align: center;">(<span
                                                    class="sign4">{{ getUserApproveName(@$appSign[4]['user']) }}</span>)
                                            </p>
                                            <p style="text-align: center;;margin-top:-5px">
                                                ({{ getUserPosition(@$appSign[4]['user']) }})</p>
                                            @if (@$appSign[4]['app_date'])
                                                <p style="text-align: center ;margin-top:-5px">
                                                    {{ date('d/m/Y', strtotime(@$appSign[4]['app_date'])) }}</p>
                                            @endif
                                            @if ($memo->department == '2')
                                                <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd</p>
                                            @endif
                                        </div>
                                    @endif
                                    @if (@$appSign[5])
                                        <div class="box-sign box-sign5" style="left: 320px;">
                                            @if (@$appSign[5]['app_date'])
                                                @if (@$from == 'app')
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo_signature/{{ $appSign[5]['user'] }}.png"
                                                            height="80px"></div>
                                                @else
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[5]['user'] }}"
                                                            height="80px"></div>
                                                @endif
                                                <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[5]['user'] }}" height="80px" ></div> -->
                                            @else
                                                <div style="height: 85px"></div>
                                            @endif
                                            <span>ลงชื่อ ............................................ ผู้อนุมัติ </span>
                                            <p style="text-align: center;">(<span
                                                    class="sign5">{{ getUserApproveName(@$appSign[5]['user']) }}</span>)
                                            </p>
                                            <p style="text-align: center;;margin-top:-5px">
                                                ({{ getUserPosition(@$appSign[5]['user']) }})</p>
                                            @if (@$appSign[5]['app_date'])
                                                <p style="text-align: center ;margin-top:-5px">
                                                    {{ date('d/m/Y', strtotime(@$appSign[5]['app_date'])) }}</p>
                                            @endif
                                            @if ($memo->department == '2')
                                                <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd</p>
                                            @endif
                                        </div>
                                    @endif
                                    @if (@$appSign[6])
                                        <div class="box-sign box-sign6" style="left: 590px;">
                                            @if (@$appSign[6]['app_date'])
                                                <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[6]['user'] }}" height="80px" ></div> -->
                                                @if (@$from == 'app')
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo_signature/{{ $appSign[6]['user'] }}.png"
                                                            height="80px"></div>
                                                @else
                                                    <div class="pic_sign"><img
                                                            src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[6]['user'] }}"
                                                            height="80px"></div>
                                                @endif
                                            @else
                                                <div style="height: 85px"></div>
                                            @endif
                                            <span>ลงชื่อ ............................................ ผู้อนุมัติ </span>
                                            <p style="text-align: center;">(<span
                                                    class="sign6">{{ getUserApproveName(@$appSign[6]['user']) }}</span>)
                                            </p>
                                            <p style="text-align: center;;margin-top:-5px">
                                                ({{ getUserPosition(@$appSign[6]['user']) }})</p>
                                            @if (@$appSign[6]['app_date'])
                                                <p style="text-align: center ;margin-top:-5px">
                                                    {{ date('d/m/Y', strtotime(@$appSign[6]['app_date'])) }}</p>
                                            @endif
                                            @if ($memo->department == '2')
                                                <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd</p>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            @endif
                        </div>

                    </page>
                    <div style="page-break-after: always;"></div>
                    @if (@$detail[1])

                        <page size="A4" class=" page2" style="padding: 0px;">
                            <p style="line-height: 30px;" class="bg-text {{ $red }}">
                                {{ $preview_show }}<br><span
                                    style="font-size: 20px;margin-left: 25px">{{ $preview_time }}</span></p>
                            <div class="no_page {{ $no_page_show }}">Page 2/<span
                                    class="noPage">{{ $count }}</span></div>
                            <img class="oppo_header" src="{{ URL_HR }}img/oppo_header.png" width="100%"
                                alt="">
                            <div class="importance">{{ @$importance }}</div>
                            <div class=" box-detail" style="">
                                <div class="col-lg-12 "
                                    style="padding: 0px;margin-top: 0px;min-height: 19cm;max-height: 21cm;display: block;">
                                    <div class="header-memo-number">{{ $memo->number }}
                                        {{ @$memo->memo_type == FINANCE_PURCHASE ? ' / ' . @$memo->ref2 : '' }}</div>
                                    <div class="header-title">
                                        <div class="date" style="margin-bottom: 20px; ">
                                            <span>วันที่</span><span style="margin: 0 5px"
                                                class="day">{{ $memo->day }}</span><span
                                                style="margin: 0 5px">{{ getMonth($memo->month) }}</span><span
                                                style="margin: 0 5px">{{ $memo->year }}</span>
                                            <br />
                                            <span>Date</span><span style="margin: 0 5px"
                                                class="day">{{ $memo->day }}</span><span
                                                style="margin: 0 5px">{{ getMonthEN($memo->month) }}</span><span
                                                style="margin: 0 5px">{{ $memo->year - 543 }}</span>
                                        </div>
                                    </div>
                                    <div style="clear: both;"></div>
                                    <div class="form-group  content2 fr-view p-0">
                                        {!! $detail[1]->detail !!}
                                    </div>

                                </div>
                            </div>
                            <div class=" sign sign-page2  {{ $sign_page2 }}" style="font-size: 12px;">

                                @if (@$appSign[1] || @$appSign[2] || @$appSign[3])
                                    <div class="box-big-sign">

                                        @if (@$appSign[1])
                                            <div class="box-sign box-sign1" style="">

                                                @if ($appSign[1]['app_date'])
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[1]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[1]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ $appSign[1]['user'] }}" height="80px" ></div> -->
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign1">{{ getUserApproveName(@$appSign[1]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[1]['user']) }})</p>

                                                @if ($appSign[1]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[1]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif

                                            </div>
                                        @endif

                                        @if (@$appSign[2])
                                            <div class="box-sign box-sign2" style="left: 320px;">
                                                @if (@$appSign[2]['app_date'])
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[2]['user'] }}" height="80px" ></div> -->
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[2]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[2]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign2">{{ getUserApproveName(@@$appSign[2]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[2]['user']) }})</p>
                                                @if (@$appSign[2]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[2]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                        @if (@$appSign[3])
                                            <div class="box-sign box-sign3" style="left: 590px;">
                                                @if (@$appSign[3]['app_date'])
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[3]['user'] }}" height="80px" ></div> -->
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[3]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[3]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign3">{{ getUserApproveName(@$appSign[3]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[3]['user']) }})</p>
                                                @if (@$appSign[3]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[3]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif
                                            </div>
                                        @endif

                                    </div>
                                @endif
                                @if (@$appSign[4] || @$appSign[5] || @$appSign[6])

                                    <div class="box-big-sign" style="margin-top: 60px">

                                        @if (@$appSign[4])
                                            <div class="box-sign box-sign4" style="">
                                                @if (@$appSign[4]['app_date'])
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[4]['user'] }}" height="80px" ></div> -->
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[4]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[4]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign4">{{ getUserApproveName(@$appSign[4]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[4]['user']) }})</p>
                                                @if (@$appSign[4]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[4]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                        @if (@$appSign[5])
                                            <div class="box-sign box-sign5" style="left: 320px;">
                                                @if (@$appSign[5]['app_date'])
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[5]['user'] }}" height="80px" ></div> -->
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[5]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[5]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign5">{{ getUserApproveName(@$appSign[5]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[5]['user']) }})</p>
                                                @if (@$appSign[5]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[5]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                        @if (@$appSign[6])

                                            <div class="box-sign box-sign6" style="left: 590px;">
                                                @if (@$appSign[6]['app_date'])
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[6]['user'] }}" height="80px" ></div> -->
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[6]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[6]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign6">{{ getUserApproveName(@$appSign[6]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[6]['user']) }})</p>
                                                @if (@$appSign[6]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[6]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                @endif
                            </div>


                        </page>
                        <div style="page-break-after: always;"></div>
                    @endif

                    @if (@$detail[2])

                        <page size="A4" class=" page3" style="padding: 0px;">
                            <p style="line-height: 30px;" class="bg-text {{ $red }}">
                                {{ $preview_show }}<br><span
                                    style="font-size: 20px;margin-left: 25px">{{ $preview_time }}</span></p>

                            <div class="no_page {{ $no_page_show }}">Page 3/<span
                                    class="noPage">{{ $count }}</span></div>

                            <img class="oppo_header" src="{{ URL_HR }}img/oppo_header.png" width="100%"
                                alt="">
                            <div class="importance">{{ @$importance }}</div>
                            <div class=" box-detail" style="">
                                <div class="col-lg-12 "
                                    style="padding: 0px;margin-top: 0px;min-height: 19cm;max-height: 21cm;display: block;">
                                    <div class="header-memo-number">{{ $memo->number }}
                                        {{ @$memo->memo_type == FINANCE_PURCHASE ? ' / ' . @$memo->ref2 : '' }}
                                    </div>
                                    <div class="header-title">
                                        <div class="date" style="margin-bottom: 20px; ">
                                            <span>วันที่</span><span style="margin: 0 5px"
                                                class="day">{{ $memo->day }}</span><span
                                                style="margin: 0 5px">{{ getMonth($memo->month) }}</span><span
                                                style="margin: 0 5px">{{ $memo->year }}</span>
                                            <br />
                                            <span>Date</span><span style="margin: 0 5px"
                                                class="day">{{ $memo->day }}</span><span
                                                style="margin: 0 5px">{{ getMonthEN($memo->month) }}</span><span
                                                style="margin: 0 5px">{{ $memo->year - 543 }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group  content3 fr-view">
                                        {!! $detail[2]->detail !!}
                                    </div>

                                </div>
                            </div>
                            <div class=" sign sign-page3  {{ $sign_page3 }}" style="font-size: 12px;">
                                @if (@$appSign[1] || @$appSign[2] || @$appSign[3])
                                    <div class="box-big-sign">


                                        @if (@$appSign[1])
                                            <div class="box-sign box-sign1" style="">

                                                @if ($appSign[1]['app_date'])
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ $appSign[1]['user'] }}" height="80px" ></div> -->
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[1]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[1]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign1">{{ getUserApproveName(@$appSign[1]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[1]['user']) }})</p>

                                                @if ($appSign[1]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[1]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif

                                            </div>
                                        @endif

                                        @if (@$appSign[2])
                                            <div class="box-sign box-sign2" style="left: 320px;">
                                                @if (@$appSign[2]['app_date'])
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[2]['user'] }}" height="80px" ></div> -->
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[2]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[2]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign2">{{ getUserApproveName(@@$appSign[2]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[2]['user']) }})</p>
                                                @if (@$appSign[2]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[2]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                        @if (@$appSign[3])
                                            <div class="box-sign box-sign3" style="left: 590px;">
                                                @if (@$appSign[3]['app_date'])
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[3]['user'] }}" height="80px" ></div> -->
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[3]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[3]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign3">{{ getUserApproveName(@$appSign[3]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[3]['user']) }})</p>
                                                @if (@$appSign[3]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[3]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                    </div>

                                @endif
                                @if (@$appSign[4] || @$appSign[5] || @$appSign[6])

                                    <div class="box-big-sign" style="margin-top: 60px">

                                        @if (@$appSign[4])
                                            <div class="box-sign box-sign4" style="">
                                                @if (@$appSign[4]['app_date'])
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[4]['user'] }}" height="80px" ></div> -->
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[4]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[4]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign4">{{ getUserApproveName(@$appSign[4]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[4]['user']) }})</p>
                                                @if (@$appSign[4]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[4]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                        @if (@$appSign[5])


                                            <div class="box-sign box-sign5" style="left: 320px;">
                                                @if (@$appSign[5]['app_date'])
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[5]['user'] }}" height="80px" ></div> -->
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[5]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[5]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign5">{{ getUserApproveName(@$appSign[5]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[5]['user']) }})</p>
                                                @if (@$appSign[5]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[5]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif
                                            </div>
                                        @endif
                                        @if (@$appSign[6])
                                            <div class="box-sign box-sign6" style="left: 590px;">
                                                @if (@$appSign[6]['app_date'])
                                                    <!-- <div class="pic_sign"><img src="/memo/get-sign/{{ $memo->id }}/{{ @$appSign[6]['user'] }}" height="80px" ></div> -->
                                                    @if (@$from == 'app')
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo_signature/{{ $appSign[6]['user'] }}.png"
                                                                height="80px"></div>
                                                    @else
                                                        <div class="pic_sign"><img
                                                                src="{{ URL_HR }}memo/get-sign/{{ $memo->id }}/{{ $appSign[6]['user'] }}"
                                                                height="80px"></div>
                                                    @endif
                                                @else
                                                    <div style="height: 85px"></div>
                                                @endif
                                                <span>ลงชื่อ ............................................ ผู้อนุมัติ
                                                </span>
                                                <p style="text-align: center;">(<span
                                                        class="sign6">{{ getUserApproveName(@$appSign[6]['user']) }}</span>)
                                                </p>
                                                <p style="text-align: center;;margin-top:-5px">
                                                    ({{ getUserPosition(@$appSign[6]['user']) }})</p>
                                                @if (@$appSign[6]['app_date'])
                                                    <p style="text-align: center ;margin-top:-5px">
                                                        {{ date('d/m/Y', strtotime(@$appSign[6]['app_date'])) }}</p>
                                                @endif
                                                @if ($memo->department == '2')
                                                    <p style="text-align: center;font-size:12px">POSSEFY GROUP Co.,Ltd
                                                    </p>
                                                @endif
                                            </div>
                                        @endif

                                    </div>
                                @endif
                            </div>
                        </page>

                        <div style="page-break-after: always;"></div>
                    @endif
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">are you sure? you want to confirm this
                        information</h2>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body" id="action-remark">
                    <form>
                      <div class="mb-3">
                        <textarea class="form-control" id="message-action" placeholder="Remark..."></textarea>
                      </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal">Close</button>
                    <button type="button" id="btn-approve" class="btn btn-success text-white">Save changes</button>
                </div>
            </div>
        </div>
    </div>


</body>

<script type="text/javascript" src="https://appdownload.myoppo.com/appMyOPPORelease/JSSDK/jssdk.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}"></script>

{{-- <script src="{{ asset('js/mobile.js') }}" t type="text/javascript"></script> --}}
<script type="text/javascript">
    var myModal = new bootstrap.Modal(document.getElementById("exampleModal"), {});
    startLoading(false);

    function startLoading(type) {
        if (type) {
            $("#loading").show()
        } else {
            $("#loading").hide()
        }
    }

    async function oncickMemo(type, id) {

        if(type == 1){
            $("#action-remark").hide();
        }else{
            $("#action-remark").show();
        }

        await myModal.show();
        document.getElementById("btn-approve").onclick = function fun() {
            actionMemo(type, id);
        }
    }

    async function actionMemo(type, id) {
        var txt_action = $("#message-action").val();
        await myModal.hide()
        await startLoading(true);
        await axios({
                method: 'post',
                url: "{{ env('APP_URL') }}api/mobile/e-memo-action",
                headers: {
                    Authorization: 'Bearer ' + getCookie("OPEN-TOKEN")
                },
                data: {
                    type: type,
                    id: id,
                    reason: txt_action
                }
            })
            .then(function(response) {
                $('#btn-action').addClass('d-none')
                yezi.ui.toast("Data has been saved successfully.");
                // $("#btn-action").hide();
            }).catch(function(error) {
                yezi.ui.toast("Can not do the transaction.");
            });


        await startLoading(false);
        // location.reload();
    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
</script>

</html>
