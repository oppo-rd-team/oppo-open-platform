<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemoDetail extends Model
{
    public $primaryKey = 'id';
    protected $table = 'oppohr.memo_detail';
    protected $connection = MYSQL_MASTER; 
}
