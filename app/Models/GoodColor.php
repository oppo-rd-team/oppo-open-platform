<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodColor extends Model
{
    public $primaryKey = 'id';
    protected $table = 'warehouse.good_color';
    protected $connection = MYSQL_MASTER; 
}
