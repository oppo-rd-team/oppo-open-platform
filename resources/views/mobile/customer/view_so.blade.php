<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>Order</title>
    <!-- Scripts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat"> --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

    <style>
        .div-in {
            margin: 15px;
        }

        .div-item {
            margin-top: 5px;
        }

        .loading-custom {
            margin: auto;
            left: 40%;
            top: 40%;
            position: fixed;
            z-index: 100;
        }

        .div-top {
            margin: 5%;
        }

        .alert-button {
            border: 10px;
            /* margin: 5%; */
        }

    </style>

</head>

<body>
    {{-- <div class="ratio ratio-16x9">
        <iframe id="i-frame" title="YouTube video" allowfullscreen></iframe>
    </div> --}}

    <iframe id="i-frame" title="SO"
        style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:100%;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px;border:none;"></iframe>
</body>

<script type="text/javascript" src="https://appdownload.myoppo.com/appMyOPPORelease/JSSDK/jssdk.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

{{-- <script src="{{ asset('js/mobile.js') }}" t type="text/javascript"></script> --}}
<script type="text/javascript">
    const view_so = JSON.parse(localStorage.getItem("view_so"));

    var url = null;
    $.each(view_so, function(index, value) {
        url = value.url_order;
    });

    document.getElementById('i-frame').src = url;
    localStorage.clear();


</script>

</html>
