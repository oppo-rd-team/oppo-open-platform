<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OppoStaff extends Model
{
    public $primaryKey = 'id';
    protected $table = 'oppohr.oppo_staff';
    protected $connection = MYSQL_MASTER;

    public static function groupDepartment($staff_code)
    {
        $result = OppoStaff::select('department.name AS department_name'
        , 'department.id'
        )
        ->join('oppohr.users', 'users.staff_id', '=', 'oppo_staff.id')
        ->join('oppohr.salary', 'salary.staff_id', '=', 'oppo_staff.id')
        ->join('oppohr.department', 'salary.department', '=', 'department.id')
        ->where('oppo_staff.kpi_by' , $staff_code)
        ->where('oppo_staff.staff_status',"!=", 4)
        ->groupBy('users.department')
        ->get();

        return $result;
    }

    public static function profile($staff_code)
    {
        $result = OppoStaff::select('oppo_staff.*',
        'users.group_id',
        'users.user_group',
        'users.department',
        'salary.position',
        'salary.department_position_code',
        )
        ->join('oppohr.users', 'users.staff_id', '=', 'oppo_staff.id')
        ->join('oppohr.salary', 'salary.staff_id', '=', 'oppo_staff.id')
        ->where('oppo_staff.staff_code' , $staff_code)
        ->where('oppo_staff.staff_status',"!=", 4)
        ->first();

        return $result;
    }

    

    
}
