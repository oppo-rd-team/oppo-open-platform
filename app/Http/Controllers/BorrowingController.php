<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\BorrowingList;
use Illuminate\Support\Facades\Auth;

class BorrowingController extends Controller
{
    //Borrowing Menu
	public function index()
	{
		return view('mobile/borrowing/index');
	}

    //My Request
    public function lists()
    {
    	return view('mobile/borrowing/request-lists');
    }

    //Add
    public function add()
    {
        return view('mobile/borrowing/add');
    }

    //Borrowing Detail
    public function detail(Request $request)
    {
    	$params = $request->all();
    	return view('mobile/borrowing/detail',compact('params'));
    }

    //Borrowing Pending (Wait For Approve)
    public function pending(Request $request)
    {
        return view('mobile/borrowing/pending');
    }

    //Borrowing Track Returned 
    public function return(Request $request)
    {
        return view('mobile/borrowing/return');
    }
}
