<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CustomerController extends Controller
{
    function index(){
        return view("mobile/customer/add_item");
    }

    function order(){
        return view("mobile/customer/order");
    } 

    function view(){
        return view("mobile/customer/view_so");
    } 
}
