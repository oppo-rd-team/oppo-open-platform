<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Borrowing Menu</title>
    <!-- Scripts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>

    <style>
        .div-main {
            margin: 10px;
        }

        .div-in {
            margin: 5px;
        }

        .div-f-center {
            margin: auto;
            left:46%;
            top:40%;
            position: fixed;
        }

        .notification-pending{
            background: red;
            width: 25px;
            height: 25px;
            color: white;
            border-radius: 12px;
            text-align: center;
            display: inline-table;
            font-size: 14px;
        }
    </style>

</head>

<body>

    <div id="loading" class="spinner-border text-success div-f-center" style="width: 5rem; height: 5rem;" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <div id='loaddata'></div>
</body>

<script type="text/javascript" src="https://appdownload.myoppo.com/appMyOPPORelease/JSSDK/jssdk.min.js"></script>
{{-- <script src="{{ asset('js/mobile.js') }}" t type="text/javascript"></script> --}}
<script type="text/javascript">

    getUser();
    var accountId = 0;
    async function getUser() {
        // await yezi.auth.getNativeData({
        //     success: function(result) {
        //         var obj = JSON.parse(result.data);
        //         accountId = JSON.stringify(obj.userInfo.accountId);
        //     },
        //     error: function(error) {
        //         accountId = null;
        //     }
        // });
        const res = await getToken("5500536"); //5900923
        await startLoading(true);
        await getBorrowingMenu();
        // console.log(res.access_token);
    }

    // getToken("6203990");
    async function getToken(accountId) {
        const {
            data
        } = await axios.post("{{ env('APP_URL') }}api/auth/register", {
            staff_code: accountId
        });
        // console.log(data.access_token);
        document.cookie = "OPEN-TOKEN=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
        document.cookie = "OPEN-TOKEN=" + data.access_token;
        return data;
    }

    async function getBorrowingMenu() {
        axios.get("{{ env('APP_URL') }}api/mobile/borrowing", {
                headers: {
                    Authorization: 'Bearer '+getCookie("OPEN-TOKEN")
                }
            })
            .then((response) => {
                if(response.data.result){
                    var html = '';
                    $.each(response.data.result, function(key, value) {
                        var notification_count = '';
                        if(value.id == 3){
                            if(response.data.notification.pending > 0){
                                notification_count = '<p class="notification-pending">'+response.data.notification.pending+'</p>';
                            }
                        }

                        if(value.id == 4){
                            if(response.data.notification_track.returnCount > 0){
                                notification_count = '<p class="notification-pending">'+response.data.notification_track.returnCount+'</p>';
                            }
                        }

                        html += '<div  onclick="AppView('+value.id+')" class="p-6 div-in mx-auto bg-white rounded-xl shadow-md flex items-center space-x-4 transition duration-500 ease-in-out bg-blue-500 hover:bg-red-500 transform hover:-translate-y-1 hover:scale-115 ...">';
                            html += '<div class="flex-shrink-0">';
                                html += '<span class="material-icons">'+value.showimg+'</span></div><div>';
                                // html += '<img class="h-20 w-20" src="'+value.showimg+'" alt="ChitChat Logo"></div><div>';
                                html += '<div class="flex-shrink-0">';
                                    html += '<div class="text-xl font-medium text-black">' + value.showtext +'  '+ notification_count +"</div>";
                                html += '</div>';
                            html += '</div>';
                        html += '</div>';
                    });
                    $('#loaddata').html(html);
                }  
            });

        await startLoading(false);
    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function startLoading(type){
        if(type){
            $("#loading").show()
        }else{
            $("#loading").hide()
        }
    }

    function AppView(id) {
        //2 = Request
        //3 = Wait For Approval
        //4 = Returned

        if(id != ''){
            if(id == 2){
                window.open("{{ env('APP_URL') }}mobile/borrowing-lists?isHybridWebView=true");
            }

            if(id == 3){
                window.open("{{ env('APP_URL') }}mobile/borrowing-pending?isHybridWebView=true");
            }

            if(id == 4){
                window.open("{{ env('APP_URL') }}mobile/borrowing-return?isHybridWebView=true");
            }            
        }
    }

</script>

</html>
