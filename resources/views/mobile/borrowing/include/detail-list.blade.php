<!DOCTYPE html>
<html>
<head>
	<title>{{ isset($data->rq) ? $data->rq : "Borrowing Detail" }}</title>

	<style>
		td,th{
			text-align: center;
			vertical-align: middle;
		}
	</style>
</head>
<body>
	<div class="container mt-2">
		<h1 class="font-bold text-2xl">{{ !empty($data->rq) ? $data->rq : "-" }}</h1>
		<div class="mt-2 flex" style="margin-left: 105px;">
			<b>Status: {!! !empty($data->status_list) ? $data->status_list : "-" !!}</b>
		</div>
		<div class="flex flex-initial gap-10">
			<div>
				Borrower
			</div>
			<div>
				<b>Date Request :</b> {{ isset($data->created_date) ? $data->created_date : "-" }}
			</div>
		</div>

		<!--เส้น-->
		<div class="mt-3 mb-3" style="border: 2px solid green;">
			<hr>
		</div>
		<!--เส้น-->

		<div class="row">
			<div class="col-12">
		     	<b>Name-Surname :</b> {{ !empty($data->staff_name) ? $data->staff_name : "-" }}
		    </div>
		    <div class="col-12 mt-2">
		     	<b>Rank :</b> {{ !empty($data->position_staff) ? $data->position_staff : "-" }}
		    </div>
		    <div class="col-12 mt-2">
		     	<b>Staff Code :</b> {{ !empty($data->code) ? $data->code : "-" }}
		    </div>
		    <div class="col-12 mt-2">
		     	<b>Area :</b> {{ !empty($data->area_name) ? $data->area_name : "-" }}
		    </div>
		    <div class="col-12 mt-2">
		     	<b>Event Name :</b> {{ !empty($data->event_program_name) ? $data->event_program_name : "-" }}
		    </div>
		    <div class="col-12 mt-2">
		     	<b>Select Date Borrow :</b> {{ !empty($data->event_program_start_period_date) ? $data->event_program_start_period_date : "-" }}
		    </div>
		    <div class="col-12 mt-2">
		     	<b>Select Date Return :</b> {{ !empty($data->event_program_end_period_date) ? $data->event_program_end_period_date : "-" }}
		    </div>
		    <div class="col-12 mt-2">
		     	<b>Phone :</b> {{ !empty($data->staff_tel) ? $data->staff_tel : "-" }}
		    </div>
		    <div class="col-12 mt-2">
		     	<b>Address :</b> {{ !empty($data->address) ? $data->address : "-" }}
		    </div>
		    <div class="col-12 mt-2">
		     	<b>CO Number :</b> {{ !empty($data->sn_ref) ? $data->sn_ref : "-" }}
		    </div>
		</div>


		<!--เส้น-->
		<div class="mt-3 mb-3" style="border: 2px solid green;">
			<hr>
		</div>
		<!--เส้น-->

		<div class="col-12">
			<h1 class="font-bold text-lg">Additional notes</h1>
		</div>
		<div class="col-12">
			{{ !empty($data->remark) ? $data->remark : "-" }}
		</div>

		<!--เส้น-->
		<div class="mt-3 mb-3" style="border: 2px solid green;">
			<hr>
		</div>
		<!--เส้น-->

		<div class="col-12 mb-2">
			<h1 class="font-bold text-lg">Borrowing Item</h1>
		</div>

		<div class="table-responsive">
			<table class="table table-bordered">
			  	<thead class="table-primary">
				    <tr>
				      <th>Model</th>
				      <th>Name</th>
				      <th>Color</th>
				      <th>Quantity</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		@php 
			  			$total_qty = 0; 
			  		@endphp
			  		@if(isset($data->item_list) && !empty($data->item_list))
			  			@foreach($data->item_list as $key => $item)
			  				<tr>
						      	<td>{{ $item->good_name }}</td>
						      	<td>{{ $item->desc }}</td>
						      	<td>{{ $item->color_name }}</td>
						      	<td>{{ $item->qty }}</td>
							</tr>
							@php
								$total_qty += $item->qty;
							@endphp
			  			@endforeach
			  		@endif
			  	</tbody>
			</table>
		</div>

		<div class="col-12 mt-2">
	     	<b>Product Type :</b> {{ isset($data->item_list[0]->product_grade) ? $data->item_list[0]->product_grade : "-" }}
	    </div>

	    <div class="col-12 mt-2">
	     	<b>Quantity :</b> {{ isset($total_qty) ? $total_qty : 0 }}
	    </div>

	    <!--เส้น-->
		<div class="mt-3 mb-3" style="border: 2px solid green;">
			<hr>
		</div>
		<!--เส้น-->

		<div class="col-12 mb-2">
			<h1 class="font-bold text-lg">Approve Process</h1>
		</div>

		@if(isset($data->approve_data['approve_list']) && !empty($data->approve_data['approve_list']))
			@foreach($data->approve_data['approve_list'] as $key => $app)
				<div class="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-2xl">
		    		<div class="flex items-center" style="margin: 3%">
		    			@if($app['approve_status'] == 2 || $app['approve_status'] == 99)
		        			<img class="w-10 h-10 rounded-full mr-4" src="{{ asset('image/icons/checked.png') }}">
		    			@elseif($app['approve_status'] == 3)
		    				<img class="w-10 h-10 rounded-full mr-4" src="{{ asset('image/icons/unchecked.png') }}">
		    			@else
		    				<img class="w-10 h-10 rounded-full mr-4" src="{{ asset('image/icons/dry-clean.png') }}">
		    			@endif
		        	<div class="text-sm">
		          		<p class="text-gray-900 leading-none">{{ $app['approve_name'] }} ({{ $app['position_name'] }})</p>
		          		<p class="text-gray-600">{{ $app['approve_date'] }}</p>
		        	</div>
		      		</div>
		  		</div>
	  		@endforeach
  		@endif

  		<div class="mb-5"></div>

  		@if($permission_approve)
  		<div class="row">
	        <div class="col flex justify-center">
	          <button type="button" class="btn btn-success text-white"
	            onclick="BorrowingAction(2,{{ $data->id }},'{{ $data->sn }}','{{ $data->rq }}')">Approve</button>
	                &nbsp;&nbsp;&nbsp;
	          <button type="button" class="btn btn-danger text-white"
	            onclick="BorrowingAction(3,{{ $data->id }},'{{ $data->sn }}','{{ $data->rq }}')">Reject</button>
	        </div>
	    </div>
	    @endif
	    <br><br>
		
	</div>
</body>
</html>