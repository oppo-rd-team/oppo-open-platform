<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>Order</title>
    <!-- Scripts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat"> --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

    <style>
        .div-in {
            margin: 15px;
        }

        .div-item {
            margin-top: 5px;
        }

        .loading-custom {
            margin: auto;
            left: 40%;
            top: 40%;
            position: fixed;
            z-index: 1;
        }

        .loading-main {
            width: 100%;
            height: 100%;
            position: fixed;
            z-index: 2;
            opacity: 0.4;
        }

        .div-top {
            margin: 5%;
        }

        .alert-button {
            border: 10px;
            /* margin: 5%; */
        }

    </style>

</head>

<body>

    <div id="loading" class="loading-main">
        <div class="spinner-border text-success loading-custom" style="width: 5rem; height: 5rem;" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>
    </div>

    <form>
        <div class="div-in">
            <div class="card border-dark">
                <div class="card-header text-center">Purchase Order for Dealer</div>
                <div class="card-body text-dark">
                    <p id="txt-1" class="card-text">Some quick</p>
                    <p id="txt-2" class="card-text">Some quick</p>
                    <p id="txt-3" class="card-text">Some quick</p>
                    <p id="txt-4" class="card-text">Some quick</p>
                    <p id="txt-5" class="card-text">Some quick</p>
                </div>

                <div class="container">
                    <div class="col-12">
                        <table class="table table-bordered" id="tb-product">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Model</th>
                                    <th scope="col">Color</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Sum</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card-body text-dark">
                    <p id="txt-6" class="card-text">Shipping Fee : </p>
                    <p id="txt-7" class="card-text">Discount : </p>
                    <p id="txt-8" class="card-text">Amount to be paid : </p>
                    <p id="txt-9" class="card-text">Remark : </p>
                </div>

            </div>

            <div class="container" style="margin-top: 10px">
                <div class="row">
                    <div class="col d-grid">
                        <button type="button" onclick="back()" class="btn btn-danger text-white">Edit</button>
                    </div>
                    <div class="col d-grid">
                        <button type="button" onclick="modalConfirm()"
                            class="btn btn-success text-white">Confirm</button>
                    </div>
                </div>
            </div>

        </div>
    </form>


    <div class="modal fade " id="exampleModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title">are you sure? you want to confirm this
                        information</h1>
                </div>

                <div class="modal-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col d-grid">
                                <a class="text-center alert-button" data-bs-dismiss="modal">Close</a>
                            </div>
                            <div class="col d-grid">
                                <a class="text-center alert-button" onclick="confirm();">Save changes</a>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- <div class="modal-footer">
                  <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal">Close</button>
                  <button type="button" id="btn-approve" onclick="confirm();" class="btn btn-success text-white">Save changes</button>
              </div> --}}
            </div>
        </div>
    </div>


</body>

<script type="text/javascript" src="https://appdownload.myoppo.com/appMyOPPORelease/JSSDK/jssdk.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

{{-- <script src="{{ asset('js/mobile.js') }}" t type="text/javascript"></script> --}}
<script type="text/javascript">
    var myModal = new bootstrap.Modal(document.getElementById("exampleModal"), {});
    const itemList = JSON.parse(localStorage.getItem("itemList") || "[]");
    const cnList = JSON.parse(localStorage.getItem("cnList") || "[]");
    const detail = JSON.parse(localStorage.getItem("detail") || "[]");
    const distributor_id = localStorage.getItem("distributor_id");
    const distributor_detail = JSON.parse(localStorage.getItem("distributor_detail") || "[]");

    window.onhashchange = function() {
        alert("alert");
    }

    setOrder();
    async function setOrder() {
        var tr = [];
        var count = 1;
        let sum_q = 0;
        let sum_price = 0;

        $("#txt-1").text("Type Order : " + detail.type_order_name);
        $("#txt-2").text("ID : " + distributor_detail.store_code);
        $("#txt-3").text("Name : " + distributor_detail.title);
        $("#txt-4").text("Address : " + distributor_detail.add);
        $("#txt-5").text("Shipping Address : " + detail.address_name);


        await $.each(itemList, function(index, value) {
            tr.push('<tr>' +
                '<td class="text-center">' + count + "</td>" +
                '<td class="text-center">' + value.good_name + "</td>" +
                '<td class="text-center">' + value.color_name + "</td>" +
                '<td class="text-center">' + new Intl.NumberFormat().format(value.qty) + "</td>" +
                '<td class="text-center">' + new Intl.NumberFormat().format(value.price) + "</td>" +
                '</tr>');
            count++;
            sum_price += parseInt(value.price);
            sum_q += parseInt(value.qty);
        });

        await tr.push('<tr>' +
            '<td class="text-center" colspan="3">' + "Total" + "</td>" +
            '<td class="text-center">' + new Intl.NumberFormat().format(sum_q) + "</td>" +
            '<td class="text-center">' + new Intl.NumberFormat().format(sum_price) + "</td>" +
            '</tr>');

        await $("#tb-product").append(tr);
        await $("#txt-8").text("Amount to be paid : " + new Intl.NumberFormat().format(sum_price));
        await $("#txt-9").text("Remark : " + detail.remark);
        await startLoading(false);

    };

    function modalConfirm() {
        myModal.show();
    }

    async function confirm() {
        var set_use_cn = 0;
        if (cnList.length >= 1) {
            set_use_cn = 1;
        }
        myModal.hide();

        await startLoading(true);
        await axios({
                method: 'post',
                url: "{{ env('APP_URL') }}api/customer/create-so",
                data: {
                    distributor_id: distributor_id,
                    staff_code: 111,
                    order_type_id: detail.type_order,
                    warehouse_id: 36,
                    shipping_id: detail.address,
                    sell_remark: detail.remark,
                    item_order: JSON.stringify(itemList),
                    use_cn: set_use_cn,
                    use_cn_list: JSON.stringify(cnList)
                }
            })
            .then(function(response) {
                var url = "";
                $.each(response.data, function(index, value) {
                    url = value.url_order;
                });

                if (url.length >= 1) {
                    yezi.ui.toast("Data has been saved successfully.");
                    localStorage.setItem("view_so", JSON.stringify(response.data));
                    window.open("{{ env('APP_URL') }}customer-so-view", "_self");
                } else {
                    yezi.ui.toast("Sorry, we do not have enough “item” in stock to fulfil your order");
                }

            }).catch(function(error) {
                // \ console.log("error");
                yezi.ui.toast("Can not do the transaction.");
            });

        await startLoading(false);
    }

    function back() {
        window.open("{{ env('APP_URL') }}customer-so", "_self");
        //  window.close();
    }

    function startLoading(type) {
        if (type) {
            $("#loading").show();
        } else {
            $("#loading").hide();
        }
    }
</script>

</html>
