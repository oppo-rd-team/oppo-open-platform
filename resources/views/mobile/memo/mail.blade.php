<body style="background: #dedddd33">
<div style="max-width: 600px;width: 100%;min-width: 300px;margin: auto;margin-top:30px;">
  <div style="width: 100%;"><img src="http://hr.oppo.in.th/img/logo2.png" width="25%" style="float: right;"></div>
  {{-- <div style="clear: both;"></div> --}}
  <div style="width: 100%; background: #FFF;min-height: 400px;padding:90px 30px;">
    <div style="font-size: 20px;font-weight: bold;margin-bottom:30px">E-Memo </div>

    <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Memo NO. {{$memo->number}} {{$memo->title}} was canceled on {{$memo->canceled_date}} by {{$user->staff_code}} {{$user->firstname}} {{$user->lastname}}</div>
    <div>&nbsp;&nbsp;Reason : {{$reason}}</div>

    <div style="margin: 30;border:1px dashed #ccc;height: 35px;padding: 25px;text-align: center;font-size:30px">
        <span style="font-size:18px; float: left;margin-top: -15px;" >Memo No.</span>
        <div style="clear: both;"></div>
      <a href="http://hr.oppo.in.th/memo/view/l/{{$memo->id}}">{{$memo->number}}</a>
    </div>
    <div>&nbsp;&nbsp;&nbsp;&nbsp;Click the memo number to see details on the web. Please login to HR system before opening the link. </div>

   {{-- <div>
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This code will expire in 30 minutes. If you weren't trying to create an OnePlus account, feel free to ignore this message. You can contact us at <a href="#">  https://www.oneplus.com/support</a> if you have questions.</div>--}}
    <div style="margin-top:50px">
        Thank You<br/> 
        HR System</div>

  </div>
  <div>


<p style="text-align: center;font-size: 12px">Copyright © {{date('Y')}} THAI OPPO CO.,LTD, All rights reserved.</p>

  </div>
</div>
</body>