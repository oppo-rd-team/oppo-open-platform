<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	public $primaryKey = 'id';
    public $incrementing = true;
    protected $connection = MYSQL_MASTER;
    protected $fillable = [
        "id",
        "name",
        "status",
        "code",
        "budget",
        "old_code",
    ];
    protected $table = 'oppohr.department';
}
