<?PHP

function getUserApproveName($code)
{

  $user =  DB::connection(MYSQL_SLAVE)->table('users as u')
    ->select(DB::raw('CONCAT(u.firstname," ",u.lastname) as firstname'))
    ->where('staff_code', $code)
    ->first();

  return @$user->firstname ? $user->firstname : '';
}

function getUserPosition($staff_code)
{

  $position = DB::connection(MYSQL_SLAVE)->table('oppo_staff as o')
    ->leftJoin('salary as s', 'o.id', 's.staff_id')
    ->leftJoin('department_position as p', 'p.code', 's.department_position_code')
    ->select('p.position_name')
    ->where('o.staff_code', $staff_code)
    ->first();

  return $position->position_name ? $position->position_name : ' ';
}

function getEmailUser($user_code)
{

  $data_group = DB::connection(MYSQL_SLAVE)->table('users as u')
  ->select('u.email')
  ->where('u.staff_code', $user_code)
  ->first();

  return $data_group->email;
}

function getMonth($m)
{
  $month = [
    '1' => 'มกราคม',
    '2' => 'กุมภาพันธ์',
    '3' => 'มีนาคม',
    '4' => 'เมษายน',
    '5' => 'พฤษภาคม',
    '6' => 'มิถุนายน',
    '7' => 'กรกฎาคม',
    '8' => 'สิงหาคม',
    '9' => 'กันยายน',
    '10' => 'ตุลาคม',
    '11' => 'พฤศจิกายน',
    '12' => 'ธันวาคม'
  ];
  return @$month[$m];
}

function getMonthEN($m)
{
  $month = [
    '1' => 'January',
    '2' => 'February',
    '3' => 'March',
    '4' => 'April',
    '5' => 'May',
    '6' => 'June',
    '7' => 'July',
    '8' => 'August',
    '9' => 'September',
    '10'  => 'October',
    '11'  => 'November',
    '12'  => 'December'
  ];
  return @$month[$m];
}
