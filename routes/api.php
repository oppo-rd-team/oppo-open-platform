<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function () {

    Route::prefix('auth')->group(function () {
        Route::post('/register', 'API\AuthController@register');
        Route::post('/login', 'API\AuthController@login');
        Route::post('/logout', 'API\AuthController@logout');
        Route::post('/refresh', 'API\AuthController@refresh');
        Route::post('/me', 'API\AuthController@me');
    });

    Route::group(['middleware' => 'auth:api'], function () {

        Route::prefix('mobile')->group(function () {
            Route::get('/e-memo', 'API\EmemoController@Approval');
            Route::get('/e-memo-other', 'API\EmemoController@memoOther');
            Route::get('/e-memo-filter', 'API\EmemoController@getFilter');
            Route::post('/e-memo-action', 'API\EmemoController@action');

            Route::get('/borrowing','API\BorrowingController@BorrowingMenu');
            Route::get('/borrowing-lists', 'API\BorrowingController@BorrowingList');
            Route::post('/borrowing-detail','API\BorrowingController@BorrowingDetail')->name('load.borrowing_detail');
            Route::post('/borrowing-pending','API\BorrowingController@BorrowingPending')->name('load.borrowing_pending');
            Route::post('/borrowing-return','API\BorrowingController@BorrowingReturn')->name('load.borrowing_return');
            Route::post('/borrowing-action','API\BorrowingController@BorrowingAction')->name('borrowing.action');

            Route::get('/borrowing-add','API\BorrowingController@BorrowingAdd')->name('load.borrowing_form');
            Route::get('/borrowing-getgood','API\BorrowingController@getGood')->name('get.good');
            Route::get('/borrowing-getcolor','API\BorrowingController@getGoodColor')->name('get.color');
            Route::post('/borrowing-additem', 'API\BorrowingController@Additem')->name('add.item');

            Route::get('/staff-privilege-list', 'API\StaffPrivilegeController@ProductList');

        });
    });

    Route::prefix('customer')->group(function () {
        Route::get('/filter', 'API\CustomerController@filterItem');
        Route::get('/cn', 'API\CustomerController@getCN');
        Route::post('/create-so', 'API\CustomerController@create');
    });
    
});

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
