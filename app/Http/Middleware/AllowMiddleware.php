<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\OppoStaff;
use Response;

class AllowMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        // $value = $request->header('AccountId', false);

        // if ($value) {
        //     $result = OppoStaff::where("staff_code", $value)->first();
        //     if(!empty($result)){
        //         echo $result;
        //         die;
        //         return $next($request);
        //     }else{
        //         $result['message'] = "user not allowed";
        //         return response::json($result, 500);
        //     }
      
        // } else {
        //     $result['message'] = "user not allowed";
        //     return response::json($result, 500);
        // }
        return $next($request);
    }
}
