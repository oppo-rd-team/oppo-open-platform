<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\OppoStaff;
use App\Models\Salary;
use App\Models\BorrowingList;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BorrowingController extends Controller
{
    public function __construct()
    {
    	define('APPROVAL_BY_DIRECTOR', 1);
        define('WAIT_APPROVAL_BY_ADMIN', 2);
        define('WAIT_APPROVAL_BY_ASM', 3);
        define('WAIT_APPROVAL_BY_RD', 4);
        define('WAIT_APPROVAL_BY_AREA_DIRECTOR', 5);
        define('WAIT_APPROVAL_BY_OPER_DIRECTOR', 6);
        define('WAIT_APPROVAL_BY_MANAGER', 7);
        define('REJECTED', 9);
        define('WMS_APPROVAL', 11);
        define('WMS_NOT_APPROVAL', 12);
        define('STAFF_RECEIVE', 13);
        define('RETURN_PRODUCT', 14);
    }

    public function BorrowingMenu(Request $request)
    {
    	$staff_code = auth()->user()->staff_code;
    	$user_group = 21; //Staff
    	$get = DB::connection(MYSQL_MASTER)->table('oppohr.users')->select('user_group')->where('staff_id',auth()->user()->staff_id)->first();
    	if(!empty($get)){
    		$user_group = $get->user_group;
    	}
    	$menu = 1;
    	$sn = date('YmdHis') . substr(microtime(), 2, 4);
    	$page = "";

    	if($menu == 37){
        	$sub_menu =  DB::connection(MYSQL_MASTER)->select("SELECT * FROM user_menu_group WHERE user_group_id = ".$user_group." and app = 1");
         	$index = 0;
        	for($i = 0;$i < count($sub_menu);$i++){
          		$menu_ = explode(',',$sub_menu[$i]->sub_menu_id);
          		foreach ($menu_ as $val ) {
            		$menu_list =  DB::connection(MYSQL_MASTER)->select("SELECT * FROM app_menu WHERE id = ".$val." and app = 1 and approve = 1");
            		$menu_page =  DB::connection(MYSQL_MASTER)->select("SELECT page FROM app_menu WHERE id = ".$sub_menu[$i]->menu_id);
            		$menu_staff00 = json_decode(json_encode($menu_list),true);
            		if(!isset($menu_staff00[0])){
              			continue;
            		}
              		$page = $menu_page[0]->page;  
            		$store[$index]["sub_menu"][] = $menu_staff00[0];
          		}
	          	if(count(@$store[$index]["sub_menu"]) > 0){
	            	$store[$index]["page"] = $page;
	            	$index++;
	          	}
        	}
      	}else{
        	$sub_menu =  DB::connection(MYSQL_MASTER)->select("SELECT * FROM user_menu_group WHERE user_group_id = ".$user_group." and menu_id =".$menu." and app = 1");
        	$menu_staff = json_decode(json_encode($sub_menu),true);
        	$menu_ = explode(',',$sub_menu[0]->sub_menu_id);
        	foreach ($menu_ as $val ) {
          		$menu_list =  DB::connection(MYSQL_MASTER)->select("SELECT * FROM app_menu WHERE id = ".$val ." and app = 1");
          		$menu_staff00 = json_decode(json_encode($menu_list),true);
          		if(!isset($menu_staff00[0])){
            		continue;
          		}
          		$store[] = $menu_staff00[0];
       	 	}
      	}

      	$have_approve = false;
      	$have_track = false;
      	if(!empty($store)){
      		foreach ($store as $key => $value) {
      			if(in_array($value['id'],array(3))){
      				$have_approve = true;
      			}

      			if(in_array($value['id'],array(4))){
      				$have_track = true;
      			}
      			$showimg = null;
      			$showtext = null;
      			switch ($value['icon']) {
      				case 'fa-list':
      					$showtext = 'My Request';
      					$showimg = 'list_alt';
      					break;

      				case 'fa-clock-o':
      					$showtext = 'Wait For Approval';
      					$showimg = 'approval';
      					break;

      				case 'fa-eye':
      					$showtext = 'Track Returned';
      					$showimg = 'assignment_return';
      					break;
      			}

      			$store[$key]['showtext'] = $showtext;
      			$store[$key]['showimg'] = $showimg;
      		}
      	}

      	$notification = array();
      	if($have_approve)
      	{
      		$notification = BorrowingList::getBorrowingPendingCount($staff_code, 'pending');
      	}

      	$notification_track = array();
      	if($have_track)
      	{
      		$notification_track = BorrowingList::getBadge($staff_code);
      	}

      	return response()->json(array('result' => $store, 'notification' => $notification, 'notification_track' => $notification_track));
    }

    public function BorrowingList(Request $data)
    {
        $staff_code = auth()->user()->staff_code;

 		$flag = 0;
		try { 
			//if(isset($data->token) && isset($check_token) && $check_token == $data->token){ 

				$index = 0;
				while ($index < 2) {
					$sql = "
					SELECT   
					CONCAT(
					u.`firstname`,
					' ',
					u.`lastname`
					) as username,  
					u.`department`,                                                                                         
					u.`staff_code`,                         
					ar.`id` AS area_id, 
					rm.`id` AS regional_market,                          
					bi.`product_grade` AS grade,
					ug.`id` AS usergroup,
					os.`tel_number`, 
					(SELECT bb.position_name FROM warehouse.borrowing_approved_tran bb WHERE 1=1 AND bb.approve_date IS NULL and bb.borrowing_sn=bl.sn order by bb.rank limit 1)as next_approve	
					,bl.* 
					FROM
					`warehouse`.`borrowing_list` bl 
					LEFT JOIN hr.`staff` s 
					ON s.`code` = bl.`code` 
					LEFT JOIN hr.`regional_market` rm 
					ON (s.`regional_market` = rm.`id`) 
					LEFT JOIN hr.`area` ar 
					ON (rm.`area_id` = ar.`id`) 
					LEFT JOIN `warehouse`.`borrowing_item` bi 
					ON (bl.`sn` = bi.`sn`) 
					LEFT JOIN oppohr.`oppo_staff` os
					ON(os.`staff_code` = bl.`code`)     
					LEFT JOIN oppohr.`users` u 
					ON(u.`staff_code` = bl.`code`)
					LEFT JOIN oppohr.`user_group` ug
					ON(u.`user_group` = ug.`id`)                       
					WHERE 1=1 ";                    
					
					if(isset($staff_code) && $staff_code){
						$sql .= " AND bl.`code` = '".$staff_code."'";
					}else{
						return json_encode(0);
					}

					if(isset($data->rq) && $data->rq){
						$sql .= " AND bl.`rq` LIKE '%".$data->rq."%'";
					}

					if(isset($data->from) && $data->from){
						$sql .= " AND bl.`created_date` >= '".$data->from." 00:00:00'";
					} 

					if(isset($data->to) && $data->to){
						$sql .= " AND bl.`created_date` <= '".$data->to." 23:59:59'";
					} 

					//print_r("index->".$index);
					//print_r($data->status);//die;b.`wms_by`
					if(isset($data->status)){
						//print_r("A");
						if($data->status==12){

							$sql .= " AND bl.`status` = '".$data->status."' and bl.wms_status=2 and bl.wms_by=144";
							$sql .= " GROUP BY bi.`sn` ORDER BY bl.created_date DESC "; 

							if($index == 0){
								$borrowing_return = DB::connection(MYSQL_MASTER)->select($sql);
							}else{
								$borrowing = DB::connection(MYSQL_MASTER)->select($sql); 
							}
						}else if($data->status==15){

							$sql .= " AND bl.`status`=12 and bl.wms_status=2 and bl.wms_by !=144";
							$sql .= " GROUP BY bi.`sn` ORDER BY bl.created_date DESC "; 

							if($index == 0){
								$borrowing_return = DB::connection(MYSQL_MASTER)->select($sql);
							}else{
								$borrowing = DB::connection(MYSQL_MASTER)->select($sql); 
							}	
						}else{

							if($data->status>=2){
								$sql .= " AND bl.`status` = '".$data->status."'";
								$sql .= " GROUP BY bi.`sn` ORDER BY bl.created_date DESC "; 

								if($index == 0){
									$borrowing_return = DB::connection(MYSQL_MASTER)->select($sql);
								}else{
									$borrowing = DB::connection(MYSQL_MASTER)->select($sql); 
								}
								
							}else if($data->status ==0){
								$sql .= " AND bl.`status` = '1'";
								$sql .= " AND bl.`wms_status` = '".$data->status."'";
								$sql .= " GROUP BY bi.`sn` ORDER BY bl.created_date DESC "; 
								
								if($index == 0){
									$borrowing_return = DB::connection(MYSQL_MASTER)->select($sql);
								}else{
									$borrowing = DB::connection(MYSQL_MASTER)->select($sql); 
								}
							}else if($data->status ==1){
								$sql .= " AND bl.`status` = '1'";
								$sql .= " AND bl.`wms_status` = '".$data->status."'";
								$sql .= " GROUP BY bi.`sn` ORDER BY bl.created_date DESC "; 

								if($index == 0){
									$borrowing_return = DB::connection(MYSQL_MASTER)->select($sql);
								}else{
									$borrowing = DB::connection(MYSQL_MASTER)->select($sql); 
								}
							}else{
								
								//print_r($data->status);//die;
								if($index == 0){
									$sql .= " AND (DATEDIFF(bl.`return_date`, NOW()) <= 15) AND  bl.`status` = '13' ";
									// $sql .= " GROUP BY bi.`sn` ORDER BY bl.read_data DESC,bl.`return_date` ,bl.update_datetime DESC"; 
									$sql .= " GROUP BY bi.`sn` ORDER BY bl.created_date DESC "; 
									
									//echo $sql;die;
									$borrowing_return = DB::connection(MYSQL_MASTER)->select($sql);
								}else{
									 $sql .= " AND ((DATEDIFF(bl.`return_date`, NOW()) > 15  OR  bl.`status` <> '13' OR bl.`return_date` IS NULL) OR(bl.`status` = '13' AND bl.`borrowing_type` = '2'))";
									// $sql .= " GROUP BY bi.`sn` ORDER BY bl.read_data DESC,bl.update_datetime DESC";
									
									$sql .= " GROUP BY bi.`sn` ORDER BY bl.created_date DESC "; 
			
									//echo $sql;die;
									$borrowing = DB::connection(MYSQL_MASTER)->select($sql); 
								}
							}
						}
						
					}else{
						//print_r("B");
						//print_r($data->status);die;
						if($index == 0){
							$sql .= " AND (DATEDIFF(bl.`return_date`, NOW()) <= 15) AND  bl.`status` = '13' ";
							// $sql .= " GROUP BY bi.`sn` ORDER BY bl.read_data DESC,bl.`return_date` ,bl.update_datetime DESC"; 
							$sql .= " GROUP BY bi.`sn` ORDER BY bl.created_date DESC "; 
							
							//echo $sql;die;
							$borrowing_return = DB::connection(MYSQL_MASTER)->select($sql);
						}else{
							$sql .= " AND ((DATEDIFF(bl.`return_date`, NOW()) > 15  OR  bl.`status` <> '13' OR bl.`return_date` IS NULL) OR(bl.`status` = '13' AND bl.`borrowing_type` = '2'))";
							// $sql .= " GROUP BY bi.`sn` ORDER BY bl.read_data DESC,bl.update_datetime DESC";
							
							$sql .= " GROUP BY bi.`sn` ORDER BY bl.created_date DESC "; 
	
							//echo $sql;die;
							$borrowing = DB::connection(MYSQL_MASTER)->select($sql); 
						}
					}                       

					
					
					$index++;                
				}
				//print_r($borrowing_return);die; 
				if(isset($data->page) && $data->page == "return"){
					$flag = $borrowing_return;
				}else if(count($borrowing_return) > 0 && count($borrowing) > 0){
					for($i = 0;$i < count($borrowing);$i++){

						//print_r($borrowing_return[$i]->rq);die;
						//print_r($borrowing[$i]->rq);die;
						$borrowing_return[count($borrowing_return)] = $borrowing[$i];
					}

					$result = array();
					foreach ($borrowing_return as $key => $value){
						if(!in_array($value, $result)){
							$result[]=$value;
						}
						
					}
					//print_r($result);die;
					//print_r(array_unique($borrowing_return));die;
					//$tempRefNoSame = array_values(array_unique($borrowing_return));
					//print_r($borrowing_return);die; 
					$flag = $result;
				}else{
					if(count($borrowing_return) > 0){
						$flag = $borrowing_return;
					}else{
						$flag = $borrowing;
					}
				}
			//}

			if(!empty($flag)){
				foreach ($flag as $key => $value) {
					$value->created_date = $this->changeformatdate($value->created_date);
					$value->flag_status = $this->getStatusList($value->status);
				}
			}

			return json_encode($flag,JSON_UNESCAPED_UNICODE); 
		} catch (\Exception $e) {
			return json_encode($e,JSON_UNESCAPED_UNICODE);
		}  
    }

    public function BorrowingAdd(Request $request)
    {
    	$staff_code = auth()->user()->staff_code;

    	$data['product_type'] = $this->productType();
    	$data['delivery'] = $this->deliveryAddress();
    	$data['info'] = BorrowingList::HRDataList($staff_code);
    	// $itemdata = $request->session()->get('items');

    	// $data = array('a','n','c');
    	// $request->session()->put('items', $data);
    	// $request->session()->forget('items');

    	// echo "<pre>";
    	// print_r ($request->session()->all());
    	// echo "</pre>";
    	// exit();

    	return view('mobile/borrowing/include/form-create',compact('data'));
    }

    public function getGood(Request $request)
    {
    	$params = $request->all();
    	$good = DB::connection(MYSQL_MASTER)->table('warehouse.good as g')
    		->select(
    			'g.cat_id',
    			'g.id as good_id',
    			'gc.id as color_id',
    			'g.name as good_name',
    			'g.desc as good_desc',
    			'gc.name as good_color',
    			'g.price_3'
    		)
    		->Leftjoin('warehouse.good_category as gct','gct.id','=','g.cat_id')
    		->Leftjoin('warehouse.good_color_combined as gcc','gcc.good_id','=','g.id')
    		->Leftjoin('warehouse.good_color as gc','gc.id','=','gcc.good_color_id')
    		->where('g.del',0)
    		->where('g.cat_id', $params['cat_id'])
    		->orderBy('g.add_time','DESC')
    		->get()
    		->toArray();
    	$html = '';
    	$html .= '<option value="">-Choose-</option>';
		if(!empty($good)){
			foreach ($good as $key => $value) {
				$html .= '<option value="'.$value->good_id.'x'.$value->good_desc.'">'.$value->good_name.' - '.$value->good_desc.'</option>';
			}
		}
		echo $html;
		exit();    	
    }

    public function getGoodColor(Request $request)
    {
    	$params = $request->all();
    	$get_color = DB::connection(MYSQL_MASTER)->table('warehouse.good as g')
    		->select('g.color')
    		->where('g.id',$params['good_id'])
    		->first();
    	$color = null;
		if(!empty($get_color)){
			if(strpos($get_color->color, ",") !== false){
				$color = explode(",", $get_color->color);
			}else{
				$color = $get_color->color;
			}
		}

		$query = DB::connection(MYSQL_MASTER)->table('warehouse.good_color')
			->select('id','name');
			if(is_array($color)){
				$data_color = $query->whereIn('id',$color);
			}else{
				$data_color = $query->where('id',$color);
			}

		$data_color = $query->get()->toArray();
		$html = '';
		$html .= '<option value="">-Choose-</option>';
		if(!empty($data_color)){
			foreach ($data_color as $key => $value) {
				$html .= '<option value="'.$value->id.'x'.$value->name.'">'.$value->name.'</option>';
			}
		}

		echo $html;
		exit();
    }

    public function Additem(Request $request)
    {
    	$staff_code = auth()->user()->staff_code;
    	$input = $request->all();

		//Set main data
		$data = array();
		$data['product_type'] = $input['product_type'];
		$data['event_name'] = $input['event_name'];
		$data['date_borrow'] = $input['date_borrow'];
		$data['date_return'] = $input['date_return'];
		$data['delivery'] = $input['delivery'];
		$data['remark'] = $input['remark'];
		$data['image_list'] = ($request->hasFile('filedata')) ? $request->file('filedata') : NULL;
		$data['address'] = isset($input['address']) ? $input['address'] : NULL;
		$data['data_array'] = array(
			'rq_new' => null,
			'currentUser' => BorrowingList::HRDataList($staff_code),
			'requestedDateTime' => date('Y-m-d H:i:s'),
			'created_date' => date('Y-m-d H:i:s'),
			'sn' => date('YmdHis') . substr(microtime(), 2, 4),
			'datenow' => date('Y-m-d H:i:s')
		);

		//Set borrowing item
		$items = array();
		if(isset($input['cat_ids']) && !empty($input['cat_ids'])){
			if(count($input['cat_ids']) == count($input['model_ids'])){
				for ($i=0; $i < count($input['cat_ids']); $i++) { 
					$items[$i]['sn'] = null;
					$items[$i]['cat_id'] = $input['cat_ids'][$i];
					$items[$i]['good_id'] = $input['model_ids'][$i];
					$items[$i]['good_color_id'] = $input['color_ids'][$i];
					$items[$i]['product_grade'] = $input['product_type'];
					$items[$i]['qty'] = $input['qtys'][$i];
				}
			}
		}
		$data['item'] = $items;

		//Save Borrowing
		$result = $this->InsertBorrowing($data);

    }

    public function InsertBorrowing($data)
    {
    	$flag = 0;
		$check_time = $this->check_date_running();

		$HOST_HR = "http://hr.oppo.in.th";
		$url = $HOST_HR ."/staff/approve_flow_borrowing_now";
		$staff_code = $data['data_array']['currentUser']->staff_code;
		$product_type = $data['product_type'];

		// echo "<pre>";
		// print_r ($data);
		// echo "</pre>";
		// exit();

		$approve_list_data = $this->curl_api_get_flow($url,$staff_code,$product_type); //get flow approve in hr
		if(count($approve_list_data) > 0){
			DB::beginTransaction();
			try {
			
					if($check_time)
					{    
						$sql_get_running= "SELECT 
						CONCAT(
						'RQ',
						DATE_FORMAT(
						'".$data['data_array']["datenow"] ."' + INTERVAL 543 YEAR,
						'%y%m%d'
						),
						'-',
						warehouse.mask (
						(SELECT 
						IF(
						(DATE_FORMAT('". $data['data_array']["datenow"] ."', '%Y%m%d')) = (
						DATE_FORMAT(a.update_date, '%Y%m%d')
						),
						a.running_next,
						1
						) AS running_next 
						FROM
						warehouse.running_no a 
						WHERE a.document_type = 'RQ'),
						'#####'
						)
						) AS SN,                       
						(SELECT 
						`running_next`
						FROM
						warehouse.running_no a 
						WHERE a.document_type = 'RQ') AS running_now ";                              
						$result_running = DB::connection(MYSQL_SLAVE)->select($sql_get_running); 

						if($result_running){

							$data['data_array']["rq_new"] = $result_running[0]->SN;
							$running_now = $result_running[0]->running_now;

							$update_running = array(
								'running_no' => $data['data_array']["rq_new"],
								'running_now' => $running_now,
								'running_next' => ((int)$running_now + 1),
								'update_date' => $data['data_array']["datenow"]
							);
							$result_update_running = DB::connection(MYSQL_MASTER)->table('warehouse.running_no')->where('document_type','RQ')->update($update_running);

							$borrowing_code = $this->insert_borrowing_list($data);
							$flag = $borrowing_code;
							if($borrowing_code > 0){
								//------------ Insert Flow ----------
								if(count($approve_list_data) > 0){
									$dataFlow = array(); 
									for($i = 0 ;$i < count($approve_list_data);$i++){
										$dataFlow[$i] = array(
											'borrowing_sn' => $data['data_array']['sn'],   
											'rank' => $approve_list_data[$i]['rank'],
											'staff_code' => $approve_list_data[$i]['staff_code'],
											'create_date' => $data['data_array']['datenow'],
											'department_name' => $approve_list_data[$i]['department'],
											'department_id' => $approve_list_data[$i]['department_id'],
											'department_position_code' => $approve_list_data[$i]['department_position_code'],
											'position_name' => $approve_list_data[$i]['position'],
											'user_group' => $approve_list_data[$i]['user_group'],
											'user_department' => $approve_list_data[$i]['user_department']
										); 
									}
									$resultFlow = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_approved_tran')->insert($dataFlow);    
									if($resultFlow){

										$Flowflag = 1;
										$borrowing_item = $this->insert_borrowing_item($data);


										if($borrowing_item != 0){
											// $flag = 1;

											if(isset($data['image_list'])){
												if(count($data['image_list']) > 0)
												{
													$data_image = [];
													for($i = 0 ;$i < count($data['image_list']);$i++){
														$data_image[$i] = array(
															'rq' => $data['data_array']["rq_new"],
															'sn' => $data['data_array']['sn'],
															'staff_code' => $data['data_array']["currentUser"]->staff_code,
															'image' => $data['image_list'][$i],
															"type" => "insert"
														);     
													} 
													$image = $this->image_upload_file($data_image);                         
												} 
											}

											// echo "<pre>";
											// print_r ($flag);
											// echo "</pre>";
											// exit();

										}else{
											$flag  = 9;
											DB::rollback();  
											$this->delect_rq($data['data_array']["rq_new"]);
										} 
									}else{
										$flag  = 9;
										DB::rollback();  
										$this->delect_rq($data['data_array']["rq_new"]);
									} 

								}else{
									// return no flow
									$flag  = 10;
									DB::rollback();  
									$this->delect_rq($data['data_array']["rq_new"]);
								}
									//print_r($Flowflag);die; 
									//-----------------------------------                   
							}else{ 
								if($flag != -1){
									$flag  = 8;
									DB::rollback();
									$this->delect_rq($data['data_array']["rq_new"]);
								}                                                  
							}

						}                    
					}
				//print_r($flag);die;
				if($flag == -1){
					//echo "Del";die;
					$this->delect_rq($data['data_array']["rq_new"]);
					DB::rollback();
					$flag= 2; 
				}else{       
					if($flag != 8 && $flag != 9 && $flag != 0){
						//echo "Com";die;
						DB::commit();
						$flag = 1;
					}else{
						//echo "Roll";die;
						DB::rollback();
						$this->delect_rq($data['data_array']["rq_new"]);
						$flag=2;
					}     
				}
				
			} catch (\Exception $e) { 
				echo $e->getMessage();die;
				//echo "ex";//die;
				DB::rollback();    
				$flag= 2;                 
			}

		}else{  
			// return no flow 
			$flag=10;
		}

		// dd($flag);

		echo $flag;
		exit();
    }

    //Insert into Borrowing List
    private function insert_borrowing_list($data)
    {
    	$status = 2;
    	if($data['data_array']["currentUser"]->department == 14){
			$status = 2;
		}else if($data['data_array']["currentUser"]->user_group == 21 || $data['data_array']["currentUser"]->user_group == 15 || $data['data_array']["currentUser"]->user_group == 30 || $data['data_array']["currentUser"]->user_group == 31){
			$status = 7;
		}else if($data['data_array']["currentUser"]->user_group == 22){
			$status = 6;
		}

		$flag = 0;
		$value = array(
			'sn' => $data['data_array']['sn'],
			'rq' => $data['data_array']['rq_new'],
			'staff_id' => $data['data_array']['currentUser']->staff_id,
			'code' => $data['data_array']['currentUser']->staff_code,
			'created_by' => $data['data_array']['currentUser']->staff_code,
			'borrowing_type' => $data['product_type'], 
			'created_date' => $data['data_array']['datenow'],
			'status' => $status,
			'remark' => $data['remark'],            
			'read_data' => '0',
			'update_datetime' => $data['data_array']['datenow'],         
			'delivery' => $data['delivery']        
		);

		if($data['data_array']["currentUser"]->user_group == 22){
			$value["admin_approved_by"] = $data['data_array']["currentUser"]->staff_code;
			$value["admin_approved_date"] = $data['data_array']['datenow'];
		}

		if($data['event_name']){
			$value["event_program_name"] = $data['event_name'];
		}

		if($data['date_borrow']){
			$value["event_program_start_period_date"] = $data['date_borrow'];
		}

		if($data['date_return']){
			$value["return_date"] = $data['date_return'];
			$value["event_program_end_period_date"] = $data['date_return'];
		}

		if($data['delivery'] == 3){
			$value["address"] = !empty($data['address']) ? $data['address'] : NULL;
		}else if($data['delivery'] == 1){
			$value["address"] = "Kerry";
		}else if($data['delivery'] == 2){
			$value["address"] = "Rama2";
		}else if($data['delivery'] == 4){
			$value["address"] = "AIA";
		}

		$result_id = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_list')->insertGetId($value);
		if($result_id){
			$flag = $result_id;
			return  $flag;
		}else{
			return   $flag;
		}
    }

    //Insert into Borrowing ITEMS
    private function insert_borrowing_item($data)
    {
    	$check_count = 0;   
		$flag = 0;         
		$value = array();
		if(!empty($data['item'])){
			for($i = 0 ;$i < count($data['item']);$i++){
				$value[$i] = array(
					'sn' => $data['data_array']["sn"],   
					'good_id' => $data['item'][$i]['good_id'],
					'good_color_id' => $data['item'][$i]['good_color_id'],
					'product_grade' => $data['item'][$i]['product_grade'],                
					'qty' => $data['item'][$i]['qty']
				);     
			}

			$result = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_item')->insert($value);
			if($result){
				$flag = 1;
			}
			return $flag; 
		}
    }

    private function check_date_running()
    {
		$check_date = DB::connection(MYSQL_SLAVE)->select("select DATE_FORMAT((SELECT `update_date` FROM warehouse.running_no a WHERE a.document_type = 'RQ'), '%Y%m%d') AS date_database");
		$nowdate = DB::connection(MYSQL_SLAVE)->select("select DATE_FORMAT(Now(), '%Y%m%d') AS date_now");        
	
		if($check_date[0]->date_database < $nowdate[0]->date_now){
			$sql = "UPDATE warehouse.running_no SET running_now = '0', running_next = '1' WHERE document_type = 'RQ' ";   
			$result_id = DB::connection(MYSQL_MASTER)->select($sql); 
		}

		return true;
	}

	private function delect_rq($rq){
		DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_list')->where('rq', '=', $rq)->delete();
		return "";
	}

	public  function image_upload_file($data_image)
	{
		for($i = 0;$i<count($data_image);$i++){
			if($data_image[$i]["type"] == "insert"){
				$imageFileName = uniqid(); 
	            $file = $data_image[$i]["image"];
	            $staff_code = $data_image[$i]["staff_code"];
				$filepath = "fileupload/".$data_image[$i]["rq"];
	            $img_name = $this->uploadName($imageFileName, $file, $staff_code,$filepath);
				$data = array(
					'sn' => $data_image[$i]["sn"],   
					'staff_code' => $data_image[$i]["staff_code"],
					'image_name' => $img_name,
					'update_datetime' => date('Y-m-d H:i:s')                       
				);   
				$result = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_upload_file')->insert($data); 
			}
		}

        return $result;
    }

    public function uploadName($name, $file, $staff_id, $filepath)
	{
	    if (file_exists($file)) {
	        $img_name = $name. '.' . $file->getClientOriginalExtension();
	        $path = public_path($filepath . "/");
	        $file->move($path, $img_name);
	        return $img_name;
	    } else {
	        return null;
	    }
	}

    public function BorrowingDetail(Request $request)
    {
    	$input = $request->all();
    	$data = BorrowingList::getBorrowingById($input['request_id']);

    	if(!empty($data)){

    		$json = array();
    		$data->created_date = $this->changeformatdate(date('Y-m-d',strtotime($data->created_date)));
    		$data->event_program_start_period_date = $this->changeformatdate(date('Y-m-d',strtotime($data->event_program_start_period_date)));
    		$data->event_program_end_period_date = $this->changeformatdate(date('Y-m-d',strtotime($data->event_program_end_period_date)));
    		$data->status_list = $this->getStatusList($data->status);

    		//ข้อมูลส่วนตัวผู้ยืม
    		$data->staff_name = null;
    		$data->position_staff = null;
    		$data->area_name = null;
    		$data->staff_tel = null;
    		$data_hr = BorrowingList::HRDataList($data->code);
    		if(!empty($data_hr)){
    			$data->staff_name = $data_hr->staffname;
    			$data->department = $data_hr->department;
    			$data->usergroup = $data_hr->user_group;
    			$data->position_staff = $data_hr->position_name;
    			$data->staff_tel = $data_hr->tel_number;
    			$data->area_name = $data_hr->area_name;
    		}

    		//ข้อมูลรายละเอียดของที่ยืมไป
    		$data->item_list = null;
    		$data_item = BorrowingList::getBorrowingItem($data->sn);
    		if(!empty($data_item)){
    			$data->item_list = $data_item;
    		}

    		//ข้อมูลการApprove
    		$data->approve_data = null;
    		$params = array();
    		$params = array(
    			'sn' => !empty($data->sn) ? $data->sn : NULL,
    			'code' => !empty($data->code) ? $data->code : NULL,
    			'staff_code' => !empty($data->code) ? $data->code : NULL,
    			'list' => json_encode($data)
    		);
    		// $data_approve = BorrowingList::getBorrowingApprove($data->sn);
    		$data_approve = $this->BorrowingSelectItem($params);
    		if(isset($data_approve['approve_list']) && !empty($data_approve['approve_list'])){
    			foreach ($data_approve['approve_list'] as $key => $arr) {
    				$approve_date = '-';
    				if(!empty($arr['approve_date'])){
    					$approve_date = $this->changeformatdate(date('Y-m-d H:i:s',strtotime($arr['approve_date'])));
    				}
    				$data_approve['approve_list'][$key]['approve_date'] = $approve_date;
    			}
    			$data->approve_data = $data_approve;
    		}
    	}

    	// echo "<pre>";
    	// print_r ($data);
    	// echo "</pre>";
    	// exit();
    	$permission_approve = BorrowingList::CheckUserApprove($data->sn, auth()->user()->staff_code);
    	return view('mobile/borrowing/include/detail-list',compact('data','permission_approve'));
    }

    public function BorrowingPending(Request $data)
    {
   		$staff_code = auth()->user()->staff_code;
   		$flag = 0;
		try{
			if($data->page == "history"){

				$borrowing_sn=" SELECT b.borrowing_sn FROM warehouse.borrowing_approved_tran b WHERE b.staff_code='".$staff_code."' AND b.approve_date IS not NULL 
				";

	            $sql = "
	            SELECT 
	            CONCAT(
	            u.`firstname`,
	            ' ',
	            u.`lastname`
	            ) as username,  
	            u.`department`,                                                            
	            u.`staff_code`,                                                             
	            bi.`product_grade` AS grade,
	            ug.`id` AS usergroup,
	            os.`tel_number`,
	            ar.`id` AS area_id, 
				rm.`id` AS regional_market,
				(SELECT bb.position_name FROM warehouse.borrowing_approved_tran bb WHERE 1=1 AND bb.approve_date IS NULL  and bb.borrowing_sn=bl.sn order by bb.rank limit 1)as next_approve
	            ,bl.* 
	            FROM
	            `warehouse`.`borrowing_list` bl 
	            LEFT JOIN hr.`staff` s 
	            ON s.`code` = bl.`code` 
	            LEFT JOIN hr.`regional_market` rm 
	            ON (s.`regional_market` = rm.`id`) 
	            LEFT JOIN hr.`area` ar 
	            ON (rm.`area_id` = ar.`id`) 
	            LEFT JOIN `warehouse`.`borrowing_item` bi 
	            ON (bl.`sn` = bi.`sn`) 
	            LEFT JOIN oppohr.`oppo_staff` os
	            ON(os.`staff_code` = bl.`code`)     
	            LEFT JOIN oppohr.`users` u 
	            ON(u.`staff_code` = os.`staff_code`)
	            LEFT JOIN oppohr.`user_group` ug
	            ON(u.`user_group` = ug.`id`)                 
	            WHERE 1=1 and  bl.sn IN (".$borrowing_sn.") ";

	            // $sql .= "(bl.`manager_approved_by` = '".$staff_code."'
	            // OR bl.`admin_approved_by` = '".$staff_code."'
	            // OR bl.`rm_approved_by` = '".$staff_code."'
	            // OR bl.`area_director_approved_by` = '".$staff_code."'
	            // OR bl.`operation_director_approved_by` = '".$staff_code."'
	            // OR bl.rejected_by  = '".$staff_code."' ) ";


	            if($data->rq && $data->rq != ""){
	              $sql .= " AND bl.`rq` LIKE '%".$data->rq."%'";
	            }

	            if($data->from && $data->from != ""){
	              $sql .= " AND bl.`created_date` >= '".$data->from." 00:00:00'";
	            } else{
	              if($data->page == "history" && $data->firstDay){
	                $sql .= " AND bl.`created_date` >= '".$data->firstDay." 00:00:00'";
	              }                                                
	            }

	            if($data->to && $data->to != ""){
	              $sql .= " AND bl.`created_date` <= '".$data->to." 23:59:59'";
	            } else{
	             if($data->page == "history" && $data->lastDay){
	              $sql .= " AND bl.`created_date` <= '".$data->lastDay." 23:59:59'";
	            }                              
	          }

	          if($data->status && $data->status != ""){
	            $sql .= " AND bl.`status` = '".$data->status."'";
	          } 

			  $sql .= " group by bi.`sn` order by bl.read_data DESC,bl.update_datetime DESC";  
			  //$sql .= " group by bi.`sn` order by bl.read_data DESC,bl.created_date asc";     

	          //echo $sql;die;
	          $borrowing =  DB::connection(MYSQL_MASTER)->select($sql);              
	          return json_encode( $borrowing); 

	        }else{

		          //$borrowing_sn=" SELECT b.borrowing_sn FROM warehouse.borrowing_approved_tran b WHERE b.staff_code='".$staff_code."' AND b.approve_date IS NULL";

				  $borrowing_sn=" SELECT b.borrowing_sn FROM warehouse.borrowing_approved_tran b WHERE b.staff_code='".$staff_code."' AND b.approve_date IS NULL 
				  and b.rank in(SELECT min(bb.rank) FROM warehouse.borrowing_approved_tran bb WHERE bb.borrowing_sn=b.borrowing_sn AND bb.approve_date IS NULL order by bb.rank )";


		          $sqlII= "SELECT CONCAT(
		          u.`firstname`,
		          ' ',
		          u.`lastname`
		          ) as username,  
		          u.`department`,                                                            
		          u.`staff_code`,  
		          bi.`product_grade` AS grade,    
		          ug.`id` AS usergroup
		          ,bl.status
				  ,(SELECT bb.position_name FROM warehouse.borrowing_approved_tran bb WHERE 1=1 AND bb.approve_date IS NULL  and bb.borrowing_sn=bl.sn order by bb.rank limit 1)as next_approve
		          , bl.id,bl.sn,bl.rq,bl.staff_id,bl.code,bl.created_by,bl.created_date,bl.admin_approved_by,bl.admin_approved_date
					,bl.manager_approved_by,bl.manager_approved_date,bl.asm_approved_by,bl.asm_approved_date,bl.rm_approved_by,bl.rm_approved_date
					,bl.area_director_approved_by,bl.area_director_approved_date,bl.auth_area_director,bl.operation_director_approved_by,bl.operation_director_approved_date
					,bl.rejected_by,bl.rejected_date,bl.rejected_remark,bl.org_full_name,bl.event_program_name,bl.event_program_start_period_date
					,bl.event_program_end_period_date,bl.remark,bl.return_date,bl.return_status,bl.borrowing_type,bl.rejected_by_group,bl.read_data
					,bl.update_datetime,bl.delivery,bl.address,bl.wms_status,bl.wms_datetime,bl.wms_by,bl.wms_remark,bl.wms_return_date,bl.wms_return_by,bl.missing
		          FROM warehouse.borrowing_list bl
		          LEFT JOIN oppohr.users u on bl.code = u.staff_code
		          LEFT JOIN warehouse.borrowing_item bi ON (bl.sn = bi.sn)
		          LEFT JOIN oppohr.oppo_staff os ON(os.staff_code = bl.code) 
		          LEFT JOIN oppohr.user_group ug ON(u.user_group = ug.id)
		          WHERE 1=1 and  bl.sn IN (".$borrowing_sn.") ";

					if($data->rq && $data->rq != ""){
						$sqlII .= " AND bl.`rq` LIKE '%".$data->rq."%'";
					}
		          
					if ($data->from && $data->from != ""){
					  	$sqlII .= " AND bl.`created_date` >= '".$data->from." 00:00:00'";
					}

					if($data->to && $data->to != ""){
					    $sqlII .= " AND bl.`created_date` <= '".$data->to." 23:59:59'";
					}

					if($data->status && $data->status != "" && $data->status != "9") {
	            		$sqlII .= " AND bl.`status` = '".$data->status."'";
	            	} else {
	            		$sqlII .= " AND bl.`status` != '9'";
	            	}

				  //$sqlII .= " group by bi.sn order by bl.read_data DESC,bl.update_datetime DESC ";
				 // $sqlII .= " group by bi.sn order by bl.read_data DESC,bl.created_date asc ";
				$sqlII .= " group by bi.sn order by bl.created_date asc ";

		          // echo "sql : ".$sql;

		          //and bl.status = 7 
		          //1 = Approved, 2 = Wait appoved by Admin, 3 = Wait appoved by ASM, 4 = Wait appoved by RD, 5 = Wait appoved by Area Director, 6 = Wait appoved by Operation Director, 7 = Wait appoved by Manager, 9 = No Approved, 11 = wms appoved, 12 = wms no appoved, 13 = get product, 14 = return product
		          //echo $sqlII;die;
		        $borrowing =  DB::connection(MYSQL_MASTER)->select($sqlII);
		        if(!empty($borrowing)){
		        	foreach ($borrowing as $key => $value) {
		        		$value->created_date = $this->changeformatdate($value->created_date);
		        		$value->flag_status = $this->getStatusList($value->status);
		        	}
		        }

	          return json_encode($borrowing); 

	        }
		}catch (\Exception $e) {               
			return json_encode($e,JSON_UNESCAPED_UNICODE);
		} 

    }

    public function BorrowingReturn(Request $data)
    {
    	$staff_code = auth()->user()->staff_code;
    	$flag = 0;
    	try { 
			//if(isset($data->token) && isset($check_token) && $check_token == $data->token){ 
				
				$borrowing_sn=" SELECT b.borrowing_sn FROM warehouse.borrowing_approved_tran b WHERE b.staff_code='".$staff_code."' AND b.approve_date IS not NULL 
				";

				$sql = "
				SELECT    
				CONCAT(
				u.`firstname`,
				' ',
				u.`lastname`
				) as username, 
				u.`department`,                                                                                         
				u.`staff_code`,                         
				ar.`id` AS area_id, 
				rm.`id` AS regional_market,                          
				bi.`product_grade` AS grade,
				ug.`id` AS usergroup,
				os.`tel_number`,                          
				bl.* 
				FROM
				`warehouse`.`borrowing_list` bl 
				LEFT JOIN hr.`staff` s 
				ON s.`code` = bl.`code` 
				LEFT JOIN hr.`regional_market` rm 
				ON (s.`regional_market` = rm.`id`) 
				LEFT JOIN hr.`area` ar 
				ON (rm.`area_id` = ar.`id`) 
				LEFT JOIN `warehouse`.`borrowing_item` bi 
				ON (bl.`sn` = bi.`sn`) 
				LEFT JOIN `oppo_staff` os
				ON(os.`staff_code` = bl.`code`)     
				LEFT JOIN `users` u 
				ON(u.`staff_code` = bl.`code`)
				LEFT JOIN `user_group` ug
				ON(u.`user_group` = ug.`id`)                       
				WHERE 1=1 and  bl.sn IN (".$borrowing_sn.") ";                    

				if(isset($data->status) && $data->status){
					$sql .= " AND bl.`status` = '".$data->status."'";
				}else{
					$sql .= " AND bl.`status` = '13' ";
				} 

				if(isset($data->rq) && $data->rq){
					$sql .= " AND bl.`rq` LIKE '%".$data->rq."%'";
				}

				if(isset($data->date) && $data->date){
					$start_date = date('Y-m-d');
					$last_date = "";
					if(isset($data->date) && $data->date == 1){                          
						$last_date = date('Y-m-d', strtotime('+1 weeks'));
					}else if(isset($data->date) && $data->date == 2){
						$last_date =date('Y-m-d', strtotime('+2 weeks'));
					}else if(isset($data->date) && $data->date == 3){
						$last_date =date("Y-m-d",strtotime("+1 month"));
					}else if(isset($data->date) && $data->date == 4){
						$last_date =date("Y-m-d",strtotime("+2 month"));
					}else if(isset($data->date) && $data->date == 5){
						$last_date =date("Y-m-d",strtotime("+3 month"));
					}else if(isset($data->date) && $data->date == 6){
						$last_date =date("Y-m-d",strtotime('+15 days'));
					} 
					if(isset($data->date) && $data->date == 7){
						$sql .= " AND bl.`return_date` <= '".$start_date." 00:00:00'";
					}else{
						$sql .= " AND ( bl.`return_date` >= '".$start_date." 00:00:00'";
						$sql .= " AND bl.`return_date` <= '".$last_date." 23:59:59')";
					}            
				}else{
					$start_date = date('Y-m-d');
					$last_date = date('Y-m-d', strtotime('+15 days'));                        
					$sql .= " AND bl.`return_date` <= '".$last_date." 23:59:59'";
				}
				// if(isset($data->group) && $data->group == 1)
				// {

				// }
				// else  if(isset($data->group) && $data->group == 10 && $data->approved_status == 99)
				// {
				// 	$sql .= " AND bl.`operation_director_approved_by` = '".$staff_code."'";
				// }
				// else  if(isset($data->group) && $data->group == 10 && $data->page != "return")
				// {
				// 	$sql .= " AND bl.`area_director_approved_by` = '".$staff_code."'";
				// }else if(isset($data->group) && $data->group == 11 || $data->group == 10){
				// 	$sql .= " AND bl.`manager_approved_by` = '".$staff_code."'";
				// }
				// else if(isset($data->group) && $data->group == 27){
				// 	$sql .= " AND bl.`rm_approved_by` = '".$staff_code."'";
				// }

				$sql .= " GROUP BY bi.`sn` ORDER BY bl.read_data DESC,bl.`return_date` ,bl.update_datetime DESC";
          		// return ($sql);
				$borrowing_return = DB::connection(MYSQL_MASTER)->select($sql); 
				$flag = $borrowing_return;
				if(!empty($flag)){
					foreach ($flag as $key => $value) {
						$value->datecreate = $this->changeformatdate($value->created_date);
						$value->datereturn = $this->changeformatdate($value->return_date);
		        		$value->flag_status = $this->getStatusList($value->status);

		        		//check overdue date
		        		$overdue = null;
		        		if(!empty($value->return_date)){
		        			$rentdate = date_create(date('Y-m-d'));
		        			$returndate = date_create(date('Y-m-d',strtotime($value->return_date)));
		        			$diff = date_diff($rentdate, $returndate);
		        			$overdue = $diff->format("%a Days");
		        		}

		        		$value->overdue_date = $overdue;
					}
				}

			//}          
			return json_encode($flag,JSON_UNESCAPED_UNICODE); 
		}catch (\Exception $e) {               
			return json_encode($e,JSON_UNESCAPED_UNICODE);
		} 
    }

public function BorrowingSelectItem($params){
	$array_output = array();

	$sn = isset($params['sn']) ? $params['sn'] : NULL;
	$code = isset($params['code']) ? $params['code'] : NULL;
	$staff_code = isset($params['staff_code']) ? $params['staff_code'] : NULL;
	$list = isset($params['list']) ? $params['list'] : NULL;

	$data = new \stdClass;
	$data->sn = $sn;
	$data->code = $code;
	$data->staff_code = $staff_code;

	$list = json_decode($list);

	if(isset($list->item_list)){
		unset($list->item_list);
	}

	if(isset($list->approve_list)){
		unset($list->approve_list);
	}

	// echo "<pre>";
	// print_r ($list);
	// echo "</pre>";
	// exit();

	//echo "A";die;
	//print_r($list->code);die;
	//$data =json_decode(file_get_contents("php://input"));
 
	//print_r($list);die;
	$borrowing_item = DB::connection(MYSQL_MASTER)->select("SELECT 
		g.`name` AS model_name,
		g.desc AS good_desc,
		g.`price_3` AS price,
		gc.`name` AS color_name,
		bi.* 
		FROM
		warehouse.`borrowing_item` bi 
		LEFT JOIN warehouse.`good` g 
		ON (g.`id` = bi.`good_id`) 
		LEFT JOIN warehouse.`good_color` gc 
		ON (bi.`good_color_id` = gc.`id`) 
		WHERE bi.`sn` = '".$data->sn."' ");            
	$array_output["data"] = $borrowing_item;
	$ch = curl_init(); 

    // set url 
    curl_setopt($ch, CURLOPT_URL, "http://catty.oppo.in.th/staff/api-get-staff-info?staff_code=".$list->code); //5900018

	//return the transfer as a string 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	// $output contains the output string 
    $area = curl_exec($ch); 

	// close curl resource to free up system resources 
	 curl_close($ch);

	 // print_r($area);
	 
	 if($area == "false"){
		$array_output["area"] = null;
	 }else{
		// $array_output["area"] = json_decode($area);
		$array_output["area"] = json_decode($area);
	 }


        //----------------------start select position------------------------------------------------------------------------------------------//      
    $position = DB::connection(MYSQL_MASTER)->select("
    	select u.`firstname`,u.`lastname`,u.`department` AS department_id,`position_name`,u.`user_group`,os.nick_name 
    	from `users` u 
    	LEFT JOIN `salary` sl ON(u.`staff_id` = sl.`staff_id`)
    	LEFT JOIN `department_position` dp ON(sl.`department_position_code` = dp.`code`)
    	LEFT JOIN oppo_staff os ON (u.`staff_code` = os.`staff_code`)
    	WHERE u.staff_code = '".$data->code."'");                                   
    $array_output["position"] = $position;  

    $admin_sql = "";
    if(isset($list->grade) && $list->grade == 2){
    	$admin_sql = "SELECT CONCAT(users.`firstname`,' ',users.`lastname`) AS admin_name 
    	FROM users 
    	WHERE staff_code = (SELECT CODE FROM `warehouse`.`borrowing_approved` WHERE id = '2')";
    }else{
    	$admin_sql = "SELECT CONCAT(users.`firstname`,' ',users.`lastname`) AS admin_name 
    	FROM users 
    	WHERE staff_code = (SELECT CODE FROM `warehouse`.`borrowing_approved` WHERE id = '1')";

    }
    $admin = DB::connection(MYSQL_MASTER)->select($admin_sql);      
    $array_output["admin"] = $admin;

    $operation_director = DB::connection(MYSQL_MASTER)->select("SELECT CONCAT(users.`firstname`,' ',users.`lastname`) AS operation_director_name 
    	FROM users 
    	WHERE staff_code = (SELECT code FROM `warehouse`.`borrowing_approved` WHERE id = '5')");           
    $array_output["operation_director"] = $operation_director;  


    $manager = null;
    if(isset($list->usergroup) && $list->usergroup == "21" || $list->usergroup == "15" || $list->usergroup == "29" || $list->usergroup == "30" || $list->usergroup == "31"){
    	$sql = "SELECT                 
    	GROUP_CONCAT(u.firstname, ' ', u.lastname) AS manager_name 
    	FROM `approve_group` a 
    	LEFT JOIN approve_group g ON a.approve_by = g.id 
    	LEFT JOIN users u ON u.staff_code = g.staff_code 
    	WHERE a.staff_code = '".$list->code."' AND a.`approve_group_id` = 1
    	GROUP BY a.`staff_code`";

    	$manager = DB::connection(MYSQL_MASTER)->select($sql); 

    	if(count($manager) == 0){
    		$manager = null;
    	}                  
    }
    $array_output["manager"] = $manager;

    $rd = null;
    if(isset($list->usergroup) && isset($list->usergroup) && $list->usergroup == "26" || $list->usergroup == "28"){        
    	$rd = DB::connection(MYSQL_MASTER)->select("SELECT GROUP_CONCAT(s1.`firstname`,' ',s1.`lastname`) AS rd_name 
    		FROM hr.`asm` a 
    		LEFT JOIN hr.`staff` s1 ON (a.`staff_id` = s1.`id`) 
    		WHERE a.`area_id` = (
    		CASE
    		WHEN a.`type` = 2 
    			THEN ".$list->area_id." 
    		WHEN a.`type` = 3 
    			THEN ".$list->regional_market." 
    		END
    		) 
    		AND `group_id` IN ('28', '35')");                   
    }
    $array_output["rd"] = $rd;

    $area_director = null;
    if($list->auth_area_director){        
    	$area_director = DB::connection(MYSQL_MASTER)->select("SELECT CONCAT(firstname,' ',lastname) AS area_director_name 
    		FROM users WHERE `staff_code` = '".$list->auth_area_director."'");                   
    }
    $array_output["area_director"] = $area_director;  
    $co = null;
    $co_sql = "SELECT cso.`sn_ref` FROM `warehouse`.`change_sales_order` cso WHERE cso.`borrowing_id` = '".$list->id."'";         
    $co = DB::connection(MYSQL_MASTER)->select($co_sql);                   
    if(count($co) > 0){
    	$array_output["co"] = $co;
    }

    $missing = null;
    $missing_sql = "SELECT 
    g.`name` as model_name,
    gc.name AS color_name,
    im.`imei_sn` 
    FROM
    `warehouse`.`imei_missing` im 
    LEFT JOIN `warehouse`.`imei` i ON(im.`imei_sn` = i.`imei_sn`)
    LEFT JOIN `warehouse`.`good` g ON(g.`id` = i.`good_id`)
    LEFT JOIN `warehouse`.`good_color` gc ON(gc.`id` = i.`good_color`)
    WHERE  im.`rq_sn` = '".$list->sn."'";         
    $missing = DB::connection(MYSQL_MASTER)->select($missing_sql);                   
    if(count($missing) > 0){
    	$array_output["missing"] = $missing;
    }


    $totalprice = 0;
    $totalamount = 0;
    for($i = 0;$i < count($borrowing_item);$i++){
    	$totalprice += ((int)$borrowing_item[$i]->price)*(int)$borrowing_item[$i]->qty;
    	$totalamount += (int)$borrowing_item[$i]->qty;
    }
    $array_output["totalprice"] = $totalprice;
    $array_output["totalamount"] = $totalamount;

//http://api-training.ioppo.oppo.in.th/fileupload/RQ621212-00004/1576123364080.jpg
   // $da = url("/");
    //print_r(URL_API);die;

    // URL API // use for production
    define('API_URL', 'http://api-training.ioppo.oppo.in.th'); //Test
    
    $image_list = null;
    $image_list_sql = "SELECT 
    im.image_name,CONCAT('".API_URL."','/fileupload/','".$list->rq."/',im.image_name) as path_name

    FROM
    `warehouse`.`borrowing_upload_file` im 
    WHERE  im.`sn` = '".$list->sn."'";         

    $image_list = DB::connection(MYSQL_MASTER)->select($image_list_sql);                   
    if(count($image_list) > 0){
    	$array_output["image_list"] = $image_list;
    }

    // echo "<pre>";
    // print_r ($list);
    // echo "</pre>";
    // exit();

    //SELECT * FROM `approve_flow_borrowing` where group_id=21 ORDER BY `approve_flow_borrowing`.`rank` ASC

    /*$approve_list_sql = "SELECT 
	'2020-03-14 15:38:07' as approve_date
	,ap.rank as sequence
	,ap.staff_code
	,'2' as approve_status
    ,CONCAT(u.firstname, ' ', u.lastname) AS approve_name
    ,'IT' as position_name
    FROM `oppohr`.`approve_flow_borrowing` ap 
    LEFT JOIN `oppohr`.`users` u ON u.staff_code = ap.staff_code collate utf8_general_ci
    WHERE  ap.`group_id` = '".$list->usergroup."'
    order by ap.rank";  */  

    $approve_list_sql = "SELECT 
	ap.rank
	,ap.staff_code
	,CONCAT(u.firstname, ' ', u.lastname) AS approve_name
	,dp.position_name
	,u.`user_group`
	,u.`department` AS department_id
	,CASE
	WHEN bl_ma.manager_approved_date is not null THEN bl_ma.manager_approved_date
	WHEN bl_ad.admin_approved_date is not null THEN bl_ad.admin_approved_date
	WHEN bl_od.operation_director_approved_date is not null THEN bl_od.operation_director_approved_date
	ELSE null
	END as approve_date

	FROM `oppohr`.`approve_flow_borrowing` ap 
	LEFT JOIN `oppohr`.`users` u ON u.staff_code = ap.staff_code collate utf8_general_ci
	LEFT JOIN `oppohr`.`salary` sl ON(u.`staff_id` = sl.`staff_id`)
	LEFT JOIN `oppohr`.`department_position` dp ON(sl.`department_position_code` = dp.`code`)
	left join warehouse.borrowing_list bl_ma on ap.staff_code=bl_ma.manager_approved_by and bl_ma.sn=".$list->sn."
	left join warehouse.borrowing_list bl_ad on ap.staff_code=bl_ad.admin_approved_by and bl_ad.sn=".$list->sn."
	left join warehouse.borrowing_list bl_od on ap.staff_code=bl_od.operation_director_approved_by and bl_od.sn=".$list->sn."
	WHERE 1=1
	and ap.is_enables=1 and ap.`group_id` = '".$list->usergroup."' 
	and ap.department_id='".$list->department."' 
	order by ap.rank";     

 	//'".$list->sn."'
    //$list->usergroup
    //echo $approve_list_sql;die;
    //$approve_list_data = DB::connection(MYSQL_MASTER)->select($approve_list_sql);        

    //http://hr-training.oppo.in.th/staff/approve_flow_borrowing_now
    $HOST_WMS="http://hr.oppo.in.th";
    $url=$HOST_WMS ."/staff/approve_flow_borrowing_now";
    /*$data =[];
    $data["staff_code"]=5701536;
    $data["product_type"]=1;*/

    //$staff_code=5701536;
    $staff_code=$list->code;
	//$product_type=1;
	$product_type=$list->borrowing_type;


	//get flow approve in hr
    $approve_list_data = $this->curl_api_get_flow($url,$staff_code,$product_type);

    //print_r($approve_list_data);die;
    //$flow_list = json_decode($dataFlow, true);
    //print_r($dataFlow);die;

    // return $approve_list_data;



    if(count($approve_list_data) > 0){
    	$row=0;
    	$next_rank=1;
    	$sequence = 1;
    	for($i = 0 ;$i < count($approve_list_data);$i++){
    		$approve_list_data[$i]['approve_date']="";
    		$approve_list_data[$i]['remark']="";

			$approve_status="";
    		$approve_date="";
    		$approve_rank="";
    		$approve_remark="";

    		$approve_staff_code="";
			$list_staff_code = $approve_list_data[$i]['staff_code'];
			$list_staff_rank = $approve_list_data[$i]['rank'];
			$approve_status_sql="SELECT * FROM warehouse.borrowing_approved_tran b 
			WHERE b.borrowing_sn='".$list->sn."' and b.staff_code='".$list_staff_code."' and b.rank='".$list_staff_rank."' ";

			$approve_status_data = DB::connection(MYSQL_MASTER)->select($approve_status_sql); 
			//print_r($approve_status_data);die;
			//echo count($approve_status_data);die;
			//if(count($approve_status_data) > 0){
				//print_r($approve_status_data[0]);die;
			$approve_remark="";
				for($j = 0 ;$j < count($approve_status_data);$j++)
				{

					//echo $staff_code;die;
					$approve_staff_code=$approve_status_data[$j]->staff_code;
					$approve_status=$approve_status_data[$j]->approve_status;
					if($approve_status_data[$j]->remark!="" || $approve_status_data[$j]->remark !=null)
					{
						$approve_remark=$approve_status_data[$j]->remark;
					}
					
					//$approve_name=$approve_status_data[$j]->approve_name;
					$approve_date=$approve_status_data[$j]->approve_date;
					$approve_rank=$approve_status_data[$j]->rank;

				}

    		if($approve_date != ""){

    			//$approve_status=2;
    			$next_rank = $approve_list_data[$i]['rank']+1;

    			if($approve_staff_code==$list_staff_code){
	    			$approve_list[$i] = array(
						"approve_date" => $approve_date,
						"sequence" => $approve_rank,
						"remark" => $approve_remark,
						"staff_code" => $approve_list_data[$i]['staff_code'],
						"approve_status" => $approve_status, //1=wait approve 2=approved 3=notapprove 4=hold
						"approve_name" => $approve_list_data[$i]['name'],
						"position_name" => $approve_list_data[$i]['position'],   
						"next_rank" => $next_rank 
					);	
    			}
    			
    		}else{
    			//echo "1";die;
    			//print_r($approve_list_data[$i]['rank']);die;
    			if($next_rank==$approve_list_data[$i]['rank']){
    				$approve_status=1;
	    			$approve_list[$i] = array(
						"approve_date" => $approve_list_data[$i]['approve_date'],
						"sequence" => $approve_list_data[$i]['rank'],
						"remark" => $approve_remark,
						"staff_code" => $approve_list_data[$i]['staff_code'],
						"approve_status" => $approve_status, //1=wait approve 2=approved 3=notapprove 4=hold
						"approve_name" => $approve_list_data[$i]['name'],
						"position_name" => $approve_list_data[$i]['position'],
						"next_rank" => $next_rank  
					);
    			}else{
    				$approve_status=4;
	    			$approve_list[$i] = array(
						"approve_date" => $approve_list_data[$i]['approve_date'],
						"sequence" => $approve_list_data[$i]['rank'],
						"remark" => $approve_remark,
						"staff_code" => $approve_list_data[$i]['staff_code'],
						"approve_status" => $approve_status, //1=wait approve 2=approved 3=notapprove 4=hold
						"approve_name" => $approve_list_data[$i]['name'],
						"position_name" => $approve_list_data[$i]['position'],
						"next_rank" => $next_rank  
					);
    			}
    			
    		}

			$row=$i;
			$sequence = $sequence + 1;

		} 


		//print_r($approve_list);die;
		$approve_list_wms_sql="SELECT b.id,b.sn,b.`wms_by`,b.`wms_datetime` as approve_date,b.`return_date`,b.`return_status`
		,s.staff_code
		,CONCAT(u.firstname, ' ', u.lastname) AS approve_name
		,dp.position_name
		,u.`user_group`
		,u.`department` AS department_id,
		b.wms_status,
		b.wms_remark,
		b.wms_by
		FROM warehouse.borrowing_list b
		LEFT JOIN warehouse.`staff` s on s.`id`=b.`wms_by`
		LEFT JOIN `oppohr`.`users` u ON u.staff_code =s.staff_code collate utf8_general_ci
		LEFT JOIN `oppohr`.`salary` sl ON(u.`staff_id` = sl.`staff_id`)
		LEFT JOIN `oppohr`.`department_position` dp ON(sl.`department_position_code` = dp.`code`)
		WHERE b.sn='".$list->sn."'
		ORDER BY b.`created_date`";
		//echo $approve_list_wms_sql;die;
		$approve_list_wms_data = DB::connection(MYSQL_MASTER)->select($approve_list_wms_sql);  

		$borrowing_id="";
		$approve_name="";
		$approve_date=""; 
		$staff_code=""; 
		$approve_status=""; 
		$position_name=""; 
		$return_date=""; 
		$return_status=""; 
		$wms_status = "";
		$wms_by="";
		$approve_remark = "";
		if(count($approve_list_wms_data) > 0){
			for($i = 0 ;$i < count($approve_list_wms_data);$i++){
				$borrowing_id=$approve_list_wms_data[$i]->id;
				$approve_name=$approve_list_wms_data[$i]->approve_name;
				$approve_date=$approve_list_wms_data[$i]->approve_date;
				$staff_code=$approve_list_wms_data[$i]->staff_code;
				$position_name=$approve_list_wms_data[$i]->position_name;
				$wms_status=$approve_list_wms_data[$i]->wms_status;
				$approve_remark=$approve_list_wms_data[$i]->wms_remark;
				$wms_by=$approve_list_wms_data[$i]->wms_by;

				$return_date=$approve_list_wms_data[$i]->return_date;
				$return_status=$approve_list_wms_data[$i]->return_status;
			}
		}

		$row = $row + 1;
		$approve_name="ชลธิชา พรมนัส";
		if ($wms_status == "1") {
			$approve_status=2;
		} else if ($wms_status == "2") {
			if ($wms_by == "144") {
				$approve_status=3;
			} else {
				$approve_status=2;
			}
		} else {
			$approve_status=4;
		}
		// if($approve_date!=""){
		// 	$approve_status=2;
		// }else{
		// 	$approve_status=4;
		// }
		$approve_list[$row] = array(
			"approve_date" => $approve_date,
			"sequence" => $sequence,
			"remark" => $approve_remark,
			"staff_code" => $staff_code,
			"approve_status" => $approve_status, //1=wait approve 2=approved 3=notapprove 4=hold
			"approve_name" => $approve_name,
			"position_name" => "Accounting" ,
			"next_rank" => $next_rank        
		);

		$approve_date="";
		$delete_by="";
		$approve_remark="";
		$approve_status;
		$delect_date="";
	    $co_sql = "SELECT cso.`sn_ref`,cso.`scanned_out_at` as approve_date,cso.`scanned_in_by`, cso.delete_by,cso.delete_reason, cso.delete_date
	    FROM `warehouse`.`change_sales_order` cso WHERE cso.`borrowing_id` = '".$borrowing_id."'";
	    $co = DB::connection(MYSQL_MASTER)->select($co_sql);          
	    if(count($co) > 0){
	    	for($i = 0 ;$i < count($co);$i++){
	    		if ($co[$i]->approve_date) {
	 				$approve_date=$co[$i]->approve_date;
	    		} else {
	    			$delect_date=$co[$i]->delete_date;
	    		}
				$delete_by=$co[$i]->delete_by;
				$approve_remark=$co[$i]->delete_reason;
			}
	    }
	    if($approve_date!=""){
			$approve_status=2;
		}else if ($delete_by != "") {
			$approve_date=$delect_date;
			$approve_status=3;
		}else{
			$approve_status=4;
		}


		$row +=1;
		$sequence +=1;

		if($return_status=="1"){
			$approve_name="Return";
		}else{
			$approve_name="Staff Received";
		}

		
		$approve_list[$row] = array(
			"approve_date" => $approve_date,
			"sequence" => $sequence,
			"staff_code" => '',
			"remark" => $approve_remark,
			"approve_status" => $approve_status, //1=wait approve 2=approved 3=notapprove 4=hold
			"approve_name" => $approve_name,
			"approve_by" => $approve_date,
			"position_name" => "Warehouse" ,
			"next_rank" => $next_rank        
		);

    	$array_output["approve_list"] = $approve_list;
    	
    }

    return $array_output;
    // return json_encode($array_output,JSON_UNESCAPED_UNICODE);
}

	public function curl_api_get_flow($url,$staff_code,$product_type)
	{
    	$response = json_decode(file_get_contents($url."?staff_code=".$staff_code."&product_type=".$product_type),true);
    	return $response;
	}


public function BorrowingAction(Request $data){

	$staff_code = auth()->user()->staff_code;
	$flag = 0;
	$status = 0;

	$value = new \stdClass;
	$value->sn = $data->sn;
	$value->borrowing_id = $data->borrowing_id;
	$value->isApproved = $data->isApproved;
	$value->status_id = $data->status_id;
	$value->rq = $data->rq;
	$value->rank = null;
	$value->user_approve_group_id = null;
	$value->remark = isset($data->remark) ? $data->remark : null;

	// echo "<pre>";
	// print_r ($data->all());
	// echo "</pre>";
	// exit();

	$getrank = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_approved_tran')
			->select('rank','user_group')
			->where('borrowing_sn', $data->sn)
			->where('staff_code', $staff_code)
			->first();
	if(!empty($getrank)){
		$value->rank = $getrank->rank;
		$value->user_approve_group_id = $getrank->user_group;
	}

	// echo "<pre>";
	// print_r ($value);
	// echo "</pre>";
	// exit();

	//print_r($data);die;
	DB::beginTransaction();
	try { 
		//print_r($data->isApproved);die;
		//print_r($data->rank);die;
		//print_r(3);die;

		$sql_approved = "SELECT id, borrowing_sn, 'rank', staff_code, approve_status, approve_date, remark FROM warehouse.borrowing_approved_tran WHERE borrowing_sn = '$value->sn' AND 'rank' = '$value->rank' AND approve_date is NOT NULL";
		$result_approved = DB::connection(MYSQL_MASTER)->select($sql_approved);

		// print_r($result_approved);
		// exit;

		// echo "s: ".json_encode($result_approved);die;
		// return $result_approved;

		if ($result_approved) {
			$flag = 3;
			return json_encode($flag,JSON_UNESCAPED_UNICODE);
		} else {

			if($value->isApproved=="false")
			{
				//Staff Select	
				$approve_status="3";
				$data_value = array(
					'approve_status' => $approve_status,
					'approve_date' => date('Y-m-d H:i:s'),
					'remark' => $value->remark,
					'update_by' => $staff_code,
					'update_date' => date('Y-m-d H:i:s')
				);

				$condition = array(
					'borrowing_sn' => $value->sn,
					'rank' => $value->rank,
					'staff_code' => $staff_code
				);

				$sql = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_approved_tran')->where($condition)->update($data_value);

				// $sql = "UPDATE 
				// warehouse.borrowing_approved_tran 
				// SET                      
				// approve_status = '".$approve_status."',
				// approve_date = '".date('Y-m-d H:i:s')."',
				// remark = '".$value->remark."' ,
				// update_by = '".$staff_code."' ,
				// update_date = '".date('Y-m-d H:i:s')."'                                        
				// WHERE borrowing_sn = '".$value->sn."' and rank = '".$value->rank."' and staff_code = '".$staff_code."' ";
				// $result_id = DB::connection(MYSQL_MASTER)->select($sql);  

				//Staff Not Select	
				$approve_status="99";
				$data_value = array(
					'approve_status' => $approve_status,
					'approve_date' => date('Y-m-d H:i:s'),
					'update_by' => $staff_code,
					'update_date' => date('Y-m-d H:i:s')
				);

				$condition = array(
					'borrowing_sn' => $value->sn,
					'rank' => $value->rank,
				);

				$sql = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_approved_tran')->where($condition)->where('staff_code','!=',$staff_code)->update($data_value);

				// $sql = "UPDATE 
				// warehouse.borrowing_approved_tran 
				// SET                      
				// approve_status = '".$approve_status."',
				// approve_date = '".date('Y-m-d H:i:s')."',
				// update_by = '".$staff_code."' ,
				// update_date = '".date('Y-m-d H:i:s')."'                                        
				// WHERE borrowing_sn = '".$value->sn."' and rank = '".$value->rank."' and staff_code != '".$staff_code."' ";
				// $result_id = DB::connection(MYSQL_MASTER)->select($sql);            
				//$flag = $result_id;
				DB::commit();

				/*$sql = "INSERT INTO 
				warehouse.borrowing_approved_tran 
				(borrowing_sn,rank,staff_code,approve_status,approve_date,remark,update_date,update_by)values
				('".$data->sn."','".$data->rank."','".$staff_code."','".$approve_status."','".$data->remark."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','".$staff_code."')	
				"; 

				$result_id = DB::connection(MYSQL_MASTER)->select($sql);             
				//$flag = $result_id;
				DB::commit();*/

				$data_main = array(
					'status' => '9',
					'rejected_by' => $staff_code,
					'rejected_date' => date('Y-m-d H:i:s'),
					'rejected_remark' => $value->remark,
					'read_data' => '1',
					'update_datetime' => date('Y-m-d H:i:s')
				);

				$condition_main = array(
					'sn' => $value->sn
				);

				$sql = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_list')->where($condition_main)->update($data_main);
				// $sql = "UPDATE 
				// warehouse.borrowing_list 
				// SET                      
				// status = '9',
				// rejected_by = '".$staff_code."' ,
				// rejected_date = '".date('Y-m-d H:i:s')."',
				// rejected_remark = '".$value->remark."',                      
				// read_data = '1',
				// update_datetime = '".date('Y-m-d H:i:s')."'   
				// WHERE sn = '".$value->sn."' "; 
				// //echo $sql;die;
				// 	$result_id = DB::connection(MYSQL_MASTER)->select($sql);          
					//$flag = $result_id;
					DB::commit(); 

					//-----------Notification
					$datapush = array(            
						'status' => 9,                            
						'approve_staff_code' => $staff_code,
						'approve_department' => 'reject',
						'code' =>  $staff_code,
						'rq' => $value->rq                             
					);         

					//Notification::setNotification($datapush);
					//print_r($datapush);die;
					//-----------End Notification
					$flag = 2; // reject OK
			}else if($value->isApproved=="true"){

				// print_r($value);
				// exit;

			/*

			status=
			1 = Approved by operation director, 2 = Wait appoved by Admin, 3 = Wait appoved by ASM, 4 = Wait appoved by RD, 5 = Wait appoved by Area Director, 6 = Wait appoved by Operation Director, 7 = Wait appoved by Manager, 9 = No Approved, 11 = wms appoved, 12 = wms no appoved, 13 = get product, 14 = return product

			*/
				// INSERT INTO `borrowing_approved_tran` (borrowing_sn,rank,staff_code,approve_status,approve_date,remark,update_date,update_by) 
				
				/*$sql = "INSERT INTO 
				warehouse.borrowing_approved_tran 
				(borrowing_sn,rank,staff_code,approve_status,approve_date,update_date,update_by)values
				('".$data->sn."','".$data->rank."','".$staff_code."','".$approve_status."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','".$staff_code."')	
				"; 

				$result_id = DB::connection(MYSQL_MASTER)->select($sql);             
				//$flag = $result_id;
				DB::commit(); */
				//Staff Select
				$approve_status="2";
				$data_value = array(
					'approve_status' => $approve_status,
					'approve_date' => date('Y-m-d H:i:s'),
					'update_by' => $staff_code,
					'update_date' => date('Y-m-d H:i:s')
				);

				$condition = array(
					'borrowing_sn' => $value->sn,
					'rank' => $value->rank,
					'staff_code' => $staff_code
				);

				$sql = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_approved_tran')->where($condition)->update($data_value);
				
				// $sql = "UPDATE warehouse.borrowing_approved_tran SET approve_status = '".$approve_status."', approve_date = '".date('Y-m-d H:i:s')."', update_by = '".$staff_code."' ,update_date = '".date('Y-m-d H:i:s')."' WHERE borrowing_sn = '".$value->sn." and rank = '".$value->rank."' and staff_code = '".$staff_code."' ";
				// $result_id = DB::connection(MYSQL_MASTER)->select($sql);

				// echo"<pre>";
				// print_r($result_id);
				// echo"</pre>";
				// die;

				//$flag = $result_id;
				//Staff Not Select 
				$approve_status="99"; //อนุมัติโคย user ที่มี rank เดียวกันระบบจะ Approve ให้  (กรณีมีผู้อนุมัติใน step นี้เดียวกัน)
				$data_value = array(
					'approve_status' => $approve_status,
					'approve_date' => date('Y-m-d H:i:s'),
					'update_by' => $staff_code,
					'update_date' => date('Y-m-d H:i:s')
				);
				$condition = array(
					'borrowing_sn' => $value->sn,
					'rank' => $value->rank,
					'staff_code' => $staff_code
				);
				$sql = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_approved_tran')->where($condition)->update($data_value);

				// $sql = "UPDATE warehouse.borrowing_approved_tran SET approve_status = '".$approve_status."', approve_date = '".date('Y-m-d H:i:s')."',update_by = '".$staff_code."' ,update_date = '".date('Y-m-d H:i:s')."' WHERE borrowing_sn = '".$value->sn." and rank = '".$value->rank."' and staff_code != '".$staff_code."' ";
				// $result_id = DB::connection(MYSQL_MASTER)->select($sql);             
				//$flag = $result_id;
				$flag = 1;
				DB::commit(); 

				// echo"<pre>";
				// print_r($value);
				// echo"</pre>";
				// die;

				$sql_MaxApproved = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_approved_tran')
					->select('rank as max_rank')
					->where('borrowing_sn', $value->sn)
					->orderBy('rank', 'DESC')
					->get()
					->toArray();
				// $sql_MaxApproved = "SELECT MAX('rank') as max_rank FROM warehouse.borrowing_approved_tran WHERE borrowing_sn = '".$value->sn."'";
				// $result_MaxApproved = DB::connection(MYSQL_MASTER)->select($sql_MaxApproved);

				// echo"<pre>";
				// print_r($sql_MaxApproved);
				// echo"</pre>";
				// die;

				try{

					if($sql_MaxApproved[0]->max_rank==$value->rank){
						DB::beginTransaction();
				  		$status = 1;
				  		$data_approve = array(
				  			'status' => $status,
				  			'operation_director_approved_by' => $staff_code,
				  			'operation_director_approved_date' => date('Y-m-d H:i:s'),
				  			'read_data' => '1',
				  			'update_datetime' => date('Y-m-d H:i:s')
				  		);

				  		$condition_approve = array(
				  			'sn' => $value->sn
				  		);

				  		$sql = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_list')->where($condition_approve)->update($data_approve);

						// $sql = "UPDATE warehouse.borrowing_list SET                      
						// status = '".$status."',
						// operation_director_approved_by = '".$staff_code."' ,
						// operation_director_approved_date = '".date('Y-m-d H:i:s')."',
						// read_data = '1',
						// update_datetime = '".date('Y-m-d H:i:s')."'  
						// WHERE sn = '".$value->sn."' "; 
						// $result_id = DB::connection(MYSQL_MASTER)->select($sql);
						//$flag = $result_id;
						DB::commit();
						$flag = 1;
					}else{
						$manager_approved_by = array(11);
						$admin_approved_by = array(22);
						$operation_director_approved_by = array(10);
						if (in_array($value->user_approve_group_id, $manager_approved_by)){
						  DB::beginTransaction();

						    	print_r('aaaa');
						  	exit();

						  	$status = 2;
						  	$data_approve = array(
					  			'status' => $status,
					  			'manager_approved_by' => $staff_code,
					  			'manager_approved_date' => date('Y-m-d H:i:s'),
					  			'read_data' => '1',
					  			'update_datetime' => date('Y-m-d H:i:s')
					  		);

					  		$condition_approve = array(
					  			'sn' => $value->sn
					  		);

				  			$sql = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_list')->where($condition_approve)->update($data_approve);
								
								// $sql = "UPDATE 
								// warehouse.borrowing_list 
								// SET                      
								// status = '".$status."',
								// manager_approved_by = '".$staff_code."' ,
								// manager_approved_date = '".date('Y-m-d H:i:s')."',
								// read_data = '1',
								// update_datetime = '".date('Y-m-d H:i:s')."'                                            
								// WHERE sn = '".$value->sn."' ";
								// $result_id = DB::connection(MYSQL_MASTER)->select($sql);             
								DB::commit(); 
								//this Approve
								$datapush = array(
									'status' => $status,                            
									'approve_staff_code' => $staff_code,
									'approve_department' => 'staff',
									'rq' => $value->rq                                                    
								);   
								$flag = 1;
						}else if (in_array($value->user_approve_group_id, $admin_approved_by)){
						  DB::beginTransaction();

						  	print_r('ssss');
						  	exit();
						  		$status = 6;
						  		$data_approve = array(
						  			'status' => $status,
						  			'admin_approved_by' => $staff_code,
						  			'admin_approved_date' => date('Y-m-d H:i:s'),
						  			'read_data' => '1',
						  			'update_datetime' => date('Y-m-d H:i:s')
						  		);

						  		$condition_approve = array(
						  			'sn' => $value->sn
						  		);

				  				$sql = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_list')->where($condition_approve)->update($data_approve);

						  		
								// $sql = "UPDATE 
								// warehouse.borrowing_list 
								// SET                      
								// status = '".$status."',
								// admin_approved_by = '".$staff_code."' ,
								// admin_approved_date = '".date('Y-m-d H:i:s')."' ,
								// read_data = '1',
								// update_datetime = '".date('Y-m-d H:i:s')."'                                         
								// WHERE sn = '".$value->sn."' "; 

								// $result_id = DB::connection(MYSQL_MASTER)->select($sql);             
								DB::commit(); 

								$flag = 1;
						}
						// else if (in_array($data->user_approve_group_id, $operation_director_approved_by)){
						//   DB::beginTransaction();
						//   $status = 1;
						// 		$sql = "UPDATE 
						// 		warehouse.borrowing_list 
						// 		SET                      
						// 		status = '".$status."',
						// 		operation_director_approved_by = '".$staff_code."' ,
						// 		operation_director_approved_date = '".date('Y-m-d H:i:s')."',
						// 		read_data = '1',
						// 		update_datetime = '".date('Y-m-d H:i:s')."'  
						// 		WHERE sn = '".$data->sn."' "; 
						// 		$result_id = DB::connection(MYSQL_MASTER)->select($sql);
						// 		DB::commit();  
						// 		$flag = 1; 
						// }
					}

				} catch (\Exception $e) { 
						//echo "Error";
						echo $e->getMessage();
						die();
						$flag = 3; 
						DB::rollback();            
						return json_encode($flag,JSON_UNESCAPED_UNICODE);
				} 


				
			}
		}

		//echo $sql;die;
		//$flag = 1;
		return json_encode($flag,JSON_UNESCAPED_UNICODE); 
	} catch (\Exception $e) { 
		echo $e->getMessage();
		die;
		$flag = 3; 
		DB::rollback();            
		return json_encode($flag,JSON_UNESCAPED_UNICODE);
	} 

}

    public function changeformatdate($date = null)
    {
    	if(!empty($date)){

    		$monthname = $this->getMonth();

    		$d = date('d',strtotime($date));
    		$m = date('m',strtotime($date));
    		$y = date('Y',strtotime($date));
    		$t = date('H:i:s',strtotime($date));

    		if($t == '00:00:00'){
    			$t = null;
    		}

    		return $d.' '.$monthname[$m].' '.$y.' '.$t;
    	}
    }

    public function getMonth()
    {
		return array(
			'01' => 'January',
			'02' => 'February',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
    }

    private function getStatusList($status_id)
    {
    	$data = '-';
    	if(!empty($status_id))
    	{
    		switch ($status_id) {
				case APPROVAL_BY_DIRECTOR:
					$data = '<b class="txt-green">Director Approve</b>';
					break;

				case WAIT_APPROVAL_BY_MANAGER:
					$data = '<b class="txt-orange">Wait Manager Approve</b>';
					break;

				case WMS_APPROVAL:
					$data = '<b class="txt-orange">Ready to shipping</b>';
					break;

				case WMS_NOT_APPROVAL:
					$data = '<b class="txt-purple">RQ cancle by Warehouse</b>';
					break;

				case STAFF_RECEIVE:
					$data = '<b class="txt-green">Staff Receive</b>';
					break;

				case RETURN_PRODUCT:
					$data = '<b class="txt-green">Returned</b>';
					break;

				case REJECTED:
					$data = '<b class="txt-red">Rejected</b>';
					break;
				
				default:
					$data = '-';
					break;
			}
    	}

    	return $data;
    }

    public function productType(){
		$array = [
			["id"=>1,"name"=>"A"],
			["id"=>2,"name"=>"B"],
			// ["id"=>3,"name"=>"Demo"],
			["id"=>4,"name"=>"APK"],
			["id"=>5,"name"=>"Prototype"],
		];

		return $array;
	}
	
	public function deliveryAddress(){
		$array = [
			["id" => 1, "name" => 'kerry'],
			["id" => 2, "name" => 'rama2'],
			["id" => 4, "name" => "AIA" ],
			["id" => 3, "name" => 'other'],
		];

		return $array;
	}

}
