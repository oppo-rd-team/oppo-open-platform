<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>E-memo</title>
    <!-- Scripts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat"> --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .div-main {}

        .div-in {
            margin: 5px;
        }

        .div-f-center {
            margin: auto;
            left: 40%;
            top: 40%;
            position: fixed;
        }

        body {
            /* font-family: 'Montserrat', serif; */
            padding: 10px;
            padding-bottom: 50px;
        }

        .mobile-bottom-nav {
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 1000;
            will-change: transform;
            transform: translateZ(0);
            display: flex;
            height: 50px;
            box-shadow: 0 -2px 5px -2px #333;
            background-color: #fff;
        }

        .mobile-bottom-nav__item {
            flex-grow: 1;
            text-align: center;
            font-size: 12px;
            display: flex;
            flex-direction: column;
            justify-content: center;
        }

        .mobile-bottom-nav__item--active {
            color: green;
        }

        .mobile-bottom-nav__item-content {
            display: flex;
            flex-direction: column;
        }

    </style>

</head>

<body>

    <div id="loading" class="spinner-border text-success div-f-center" style="width: 5rem; height: 5rem;" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <div class="row">
        <form>
            <div class="row">
                <div class="col-sm-12">
                    <p>
                        <a style="float:right;" data-bs-toggle="collapse" role="button" onclick="collapseClick()"
                            aria-expanded="false" aria-controls="collapseExample">
                            <span class="material-icons">manage_search</span>
                        </a>
                        <a id="text-count" style="float:right;margin-right:10px;margin-top:5px;"> </a>
                    <div id="loading-list" class="spinner-border spinner-border-sm text-success align-items-center"
                        style="float:right;margin-right:5px;margin-top:5px;" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                    </p>
                </div>
                {{-- <p>
                    <a style="float:right;" data-bs-toggle="collapse" role="button" onclick="collapseClick()"
                        aria-expanded="false" aria-controls="collapseExample">
                        <span class="material-icons">manage_search</span>
                    </a>
                </p> --}}
            </div>
            <div class="row">
                <div class="collapse" id="collapse-search">
                    <div class="card card-body">
                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Memo Number</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="memo-number">
                            </div>
                        </div>

                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Department</label>
                            <div class="col-sm-10">
                                <select class="form-select" id="memo-department" aria-label="Default select example">
                                    <option selected></option>
                                </select>
                            </div>
                        </div>

                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Memo Type</label>
                            <div class="col-sm-10">
                                <select class="form-select" id="memo-type" aria-label="Default select example">
                                    <option selected></option>
                                </select>
                            </div>
                        </div>

                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-select" id="memo-status" aria-label="Default select example">
                                    <option selected></option>
                                </select>
                            </div>
                        </div>

                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Date From</label>
                            <div class="col-sm-10">
                                <input type="date" id="date-from" class="form-control datepicker"
                                    data-date-format="mm/dd/yyyy">
                            </div>
                        </div>


                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Date To</label>
                            <div class="col-sm-10">
                                <input type="date" id="date-to" class="form-control datepicker"
                                    data-date-format="mm/dd/yyyy">
                            </div>
                        </div>


                        <div class="modal-footer">
                            <button type="reset" id="btn-reset" class="btn btn-danger text-white">Reset</button>
                            <button type="button" id="btn-search" class="btn btn-success text-white"
                                onclick="searchMemo()">Search</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div id='approval-fetch' class="div-main"></div>
    </div>


    <nav class="mobile-bottom-nav">
        <div class="mobile-bottom-nav__item mobile-bottom-nav__item--active" value="h1">
            <div class="mobile-bottom-nav__item-content">
                <span class="material-icons">pending</span>
                Approval
            </div>
        </div>
        <div class="mobile-bottom-nav__item" value="h2">
            <div class="mobile-bottom-nav__item-content">
                <span class="material-icons">history</span>
                E-MEMO All
            </div>
        </div>
        <div class="mobile-bottom-nav__item" value="h3">
            <div class="mobile-bottom-nav__item-content">
                <span class="material-icons">reorder</span>
                E-MEMO CC
            </div>
        </div>

    </nav>

</body>

<script type="text/javascript" src="https://appdownload.myoppo.com/appMyOPPORelease/JSSDK/jssdk.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}"></script>
{{-- <script src="{{ asset('js/mobile.js') }}" t type="text/javascript"></script> --}}
<script type="text/javascript">
    var type = 0;
    var navItems = document.querySelectorAll(".mobile-bottom-nav__item");
    navItems.forEach(function(e, i) {
        e.addEventListener("click", function(e) {
            navItems.forEach(function(e2, i2) {
                e2.classList.remove("mobile-bottom-nav__item--active");
            })
            this.classList.add("mobile-bottom-nav__item--active");
            getApproval(i);
        });
    });

    getUser();
    var LoginName = 0;
    async function getUser() {
        await yezi.auth.getNativeData({
            success: function(result) {
                var obj = JSON.parse(result.data);
                LoginName = obj.userInfo.loginName;
            },
            error: function(error) {
                LoginName = null;
            }
        });

        // const res = await getToken("5600478");
        const res = await getToken(LoginName);
        await getFilter();
        await getApproval(0);
    }

    // getToken("6203990");
    async function getToken(accountId) {
        const {
            data
        } = await axios.post("{{ env('APP_URL') }}api/auth/register", {
            staff_code: accountId
        });

        //console.log(data.access_token);
        document.cookie = "OPEN-TOKEN=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
        document.cookie = "OPEN-TOKEN=" + data.access_token;
        return data;
    }

    async function getApproval(i) {
        type = i;
        var url = null;
        var fetch_type = null;
        if (type == 0) {
            url = "{{ env('APP_URL') }}api/mobile/e-memo";
        } else if (type == 1) {
            url = "{{ env('APP_URL') }}api/mobile/e-memo-other";
        } else {
            url = "{{ env('APP_URL') }}api/mobile/e-memo-other";
            fetch_type = 'memo_cc'
        }

        var memo_number = $("#memo-number").val();
        var memo_department = $("#memo-department").val();
        var memo_type = $("#memo-type").val();
        var memo_status = $("#memo-status").val();
        var date_from = $("#date-from").val();
        var date_to = $("#date-to").val();

        await clearDiv();
        await startLoading(true);
        await axios({
            method: 'get',
            url: url,
            headers: {
                Authorization: 'Bearer ' + getCookie("OPEN-TOKEN")
            },
            params: {
                type: fetch_type,
                memo_number: memo_number,
                memo_department: memo_department,
                memo_type: memo_type,
                memo_status: memo_status,
                date_from: date_from,
                date_to: date_to,
            }
        }).then(function(response) {
            var tr = [];

            $.each(response.data, function(index, value) {
                // tr.push('<div  onclick="viewMemo(' + value.doc_id +
                //     ')" class="p-6 div-in mx-auto bg-white rounded-xl shadow-md flex items-center space-x-4 transition duration-500 ease-in-out bg-blue-500 hover:bg-red-500 transform hover:-translate-y-1 hover:scale-115 ..."><div>' +

                //     '<div class="flex-shrink-0">' +
                //     '<div class="text-xl font-medium text-black">' + value.number + "</div>" +
                //     '<p class="text-gray-500">Title : ' + value.title + '</p>' +
                //     '<p class="text-gray-500">Staff Name : ' + value.staff_code + " " + value
                //     .firstname + " " + value.lastname + '</p>' +
                //     '<p class="text-gray-500">Department : ' + value.department_name +
                //     '</p></div></div>');

                tr.push('<div class="card text-center div-in">' +
                    '<div class="card-header">' + value.number + "</div>" +
                    '<div class="card-body">' +
                    '<h5 class="card-title">Department : ' + value.department_name + "</h5>" +
                    '<p class="card-text">Title : ' + value.title + '</p>' +
                    status(value.status) +
                    '<a href="#" style="margin-top:10px" onclick="viewMemo(' + value.doc_id +
                    ')" class="btn btn-primary text-white">View</a>' +
                    '</div><div class="card-footer text-muted">' + value.date_passed +
                    ' days ago</div></div>');
            });
            $("#approval-fetch").append(tr);
            $("#text-count").text("list:" + response.data.length);

        }).catch(function(error) {
            console.log(error);
        });

        await startLoading(false);
    }

    function status(status) {
        var name = "";
        var color = "";
        switch (status) {
            case 1:
                name = "Drafted";
                color = "#000000";
                break;
            case 2:
                name = "Waiting for Approval";
                color = "#FFC107";
                break;
            case 3:
                name = "Approved";
                color = "#28a745";
                break;
            case 4:
                name = "Published";
                color = "#28a745";
                break;
            case 5:
                name = "Reject";
                color = "#CC0000";
                break;
            case 6:
                name = "Unpublished";
                color = "#CC0000";
                break;
            case 7:
                name = "Canceled";
                color = "#CC0000";
                break;
            default:
                name = "";
                color = "#000000";
        }

        return '<p class="card-text" style="color:'+color+'">Status : ' + name + '</p>';
    }

    function getFilter() {
        axios({
            method: 'get',
            url: "{{ env('APP_URL') }}api/mobile/e-memo-filter",
            headers: {
                Authorization: 'Bearer ' + getCookie("OPEN-TOKEN")
            }
        }).then(function(response) {

            var f1 = [];
            var f2 = [];
            var f3 = [];
            $.each(response.data.department, function(index, value) {
                f1.push('<option value=' + value.id + '>' + value.name + '</option>');
            });

            $.each(response.data.memo_status, function(index, value) {
                f2.push('<option value=' + value.id + '>' + value.name + '</option>');
            });

            $.each(response.data.memo_type, function(index, value) {
                f3.push('<option value=' + value.id + '>' + value.name + '</option>');
            });

            $("#memo-department").append(f1);
            $("#memo-status").append(f2);
            $("#memo-type").append(f3);
            $("#date-from").val(getFormattedDate(new Date(response.data.date_from)));
            $("#date-to").val(getFormattedDate(new Date(response.data.date_to)));

        }).catch(function(error) {});
    }

    function getFormattedDate(date) {
        return date.getFullYear() +
            "-" +
            ("0" + (date.getMonth() + 1)).slice(-2) +
            "-" +
            ("0" + date.getDate()).slice(-2);
    }

    function clearDiv() {
        var list = document.getElementById("approval-fetch"); // Get the <ul> element with id="myList"
        list.innerHTML = '';
    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function startLoading(type) {
        if (type) {
            $("#text-count").text('');
            $("#loading").show();
            $("#loading-list").show();
        } else {
            $("#loading").hide();
            $("#loading-list").hide();
        }
    }

    function viewMemo($id) {
        window.open("{{ env('APP_URL') }}mobile/e-memo-view?isHybridWebView=true&id=" + $id + "&from_click=" + type).focus();
    }

    async function searchMemo() {
        await collapseClick();
        await getApproval(type);
    }

    function collapseClick() {
        $("#collapse-search").collapse('toggle');
    }
</script>

</html>
