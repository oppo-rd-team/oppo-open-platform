<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class BorrowingList extends Model
{
    public $primaryKey = 'id';
    protected $table = 'warehouse.borrowing_list';
    protected $connection = MYSQL_MASTER; 

    static public function getBorrowingById($borrowing_id)
    {
    	return DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_list')
    		->select(
    			'borrowing_list.*',
    			'change_sales_order.sn_ref'
    		)
    		->Leftjoin('warehouse.change_sales_order', 'borrowing_list.id','=','change_sales_order.borrowing_id')
    		->where('borrowing_list.id', $borrowing_id)
    		->first();
    }

    static public function getBorrowingItem($sn)
    {
    	$query = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_item')
    		->select(
    			'borrowing_item.*',
    			'good.name as good_name',
    			'good.desc',
    			'good_color.name as color_name',
    			DB::raw('
    				(CASE 
    					WHEN borrowing_item.product_grade = 1 THEN "Grade A" 
    					WHEN borrowing_item.product_grade = 2 THEN "Grade B"
    					WHEN borrowing_item.product_grade = 3 THEN "Demo"
    					WHEN borrowing_item.product_grade = 4 THEN "APK"
    					WHEN borrowing_item.product_grade = 5 THEN "Prototype"
    				ELSE
    					"-"
    				END
    			) as product_grade')
    		)
    		->Leftjoin('warehouse.good', 'borrowing_item.good_id','=','good.id')
    		->Leftjoin('warehouse.good_color', 'borrowing_item.good_color_id','=','good_color.id')
    		->where('borrowing_item.sn', $sn)
    		->get()
    		->toArray();

    	return $query;
    }

    static public function getBorrowingApprove($sn)
    {
    	$query = DB::connection(MYSQL_MASTER)->table('warehouse.borrowing_approved_tran')
    		->select(
    			'borrowing_approved_tran.*',
    			'oppo_staff.name_en'
    		)
    		->Leftjoin('oppohr.oppo_staff','borrowing_approved_tran.staff_code','=','oppo_staff.staff_code')
    		->where('borrowing_approved_tran.borrowing_sn', $sn)
    		->orderBy('borrowing_approved_tran.rank','ASC')
    		->get()
    		->toArray();

    	return $query;
    }

    static public function HRDataList($staff_code)
    {
    	$query = DB::connection(MYSQL_MASTER)
    		->table('oppohr.users as us')
    		->select(
    			'us.staff_id',
    			'us.staff_code',
                'us.user_group',
                'us.department',
    			'dp.position_name',
    			'op.tel_number',
                'd.name as department_name',
    			'a.name as area_name',
    			DB::raw('CONCAT(us.firstname," ",us.lastname) as staffname')
    		)

    		//Connection with OppoHR
    		->Leftjoin('oppohr.salary as sl', 'us.staff_id','=','sl.staff_id')
            ->Leftjoin('oppohr.department as d', 'us.department','=','d.id')
    		->Leftjoin('oppohr.department_position as dp', 'sl.department_position_code','=','dp.code')
    		->Leftjoin('oppohr.oppo_staff as op', 'us.staff_code','=','op.staff_code')
    		
    		//Connection with Catty Oppo
    		->Leftjoin('hr.staff as cs', 'us.staff_code','=','cs.code')
    		->Leftjoin('hr.regional_market as rmg', 'cs.regional_market','=','rmg.id')
    		->Leftjoin('hr.area as a', 'rmg.area_id','=','a.id')

    		//Condition
    		->where('us.staff_code', $staff_code)
    		->first();

    	return $query;
    }

    static public function CheckUserApprove($sn, $staffcode)
    {
        $query = DB::connection(MYSQL_MASTER)
            ->table('warehouse.borrowing_approved_tran')
            ->select('staff_code','approve_status')
            ->where('staff_code', $staffcode)
            ->where('borrowing_sn',$sn)
            ->whereNull('approve_status')
            ->first();
        $have_approve = false;
        if(!empty($query)){
            $have_approve = true;
        }
        
        return $have_approve;
    }

    static public function getBorrowingPendingCount($staff_code, $type = 'pending')
    {
        $data = new \stdClass;
        $data->page = $type;
        $get = DB::connection(MYSQL_MASTER)->table('oppohr.users')->select('user_group')->where('staff_id',auth()->user()->staff_id)->first();
        if(!empty($get)){
            $data->group = $get->user_group;
        }

        $pending = 0;
        $list_staff_code = "SELECT g.staff_code FROM `approve_group` g 
        WHERE g.`approve_by` IN  (SELECT a.`id` FROM `approve_group` a 
        WHERE a.staff_code = '".$staff_code."' 
        AND a.`approve_group_id` = 1 AND a.`approve_by` IS NULL) 
        AND g.`approve_group_id` = 1 ";

        $sql = "SELECT COUNT(DISTINCT(bl.id)) AS pending_count FROM warehouse.borrowing_list bl LEFT JOIN oppohr.users u on bl.code = u.staff_code LEFT JOIN warehouse.borrowing_item bi ON (bl.sn = bi.sn) LEFT JOIN oppohr.oppo_staff os ON (os.staff_code = bl.code) LEFT JOIN user_group ug ON (u.user_group = ug.id) WHERE (u.user_group = 21 or u.user_group = 15 || u.user_group = 30 || u.user_group = 29) and bl.status = 7 and u.staff_code IN (".$list_staff_code.")";  
            //echo $sql;die;
        $pending_manager = DB::connection(MYSQL_MASTER)->select($sql);    

        $sql_on_manager = "SELECT code FROM `warehouse`.`borrowing_approved` WHERE code = '".$staff_code."'";  
                $on_manager = DB::connection(MYSQL_MASTER)->select($sql_on_manager);  
                //if ( count($on_manager) == 0 && (int)$pending_manager[0]->pending_count > 0) { 
                    if (isset($pending_manager[0]->pending_count) && (int)$pending_manager[0]->pending_count > 0) { 
                        $sql_read = "SELECT COUNT(bl.code) AS readCount FROM warehouse.`borrowing_list` bl WHERE bl.read_data = '1' AND bl.code = '".$staff_code."' GROUP BY bl.code";

                        $homecount = DB::connection(MYSQL_MASTER)->select($sql_read);  

                        if($homecount){
                            $dataall['readCount'] = $homecount[0]->readCount;
                        }else{
                            $dataall['readCount'] = 0;
                        }                            
                        $dataall['pending'] = $pending_manager[0]->pending_count;

                        return $dataall;
                        // return json_encode($dataall,JSON_UNESCAPED_UNICODE);              
                    }
                    else{ 

                        $sql = "SELECT COUNT(DISTINCT(bl.id)) AS pending_count FROM `warehouse`.`borrowing_list` bl LEFT JOIN hr.`staff` s ON s.`code` = bl.`code` LEFT JOIN hr.`regional_market` rm ON (s.`regional_market` = rm.`id`) LEFT JOIN hr.`area` ar ON (rm.`area_id` = ar.`id`) LEFT JOIN `warehouse`.`borrowing_item` bi ON (bl.`sn` = bi.`sn`) LEFT JOIN `oppo_staff` os ON(os.`staff_code` = bl.`code`) LEFT JOIN `users` u ON(u.`staff_code` = os.`staff_code`) LEFT JOIN `user_group` ug ON(u.`user_group` = ug.`id`) WHERE 1=1";

                        if(isset($data->page) && $data->page == "pending"){
                            $checkapproved = DB::connection(MYSQL_MASTER)->select("SELECT * FROM  `warehouse`.`borrowing_approved` WHERE code = '".$staff_code."'");

                            if($data->group == 1)
                            {
                                if(count($checkapproved) > 0)
                                {
                                    if(isset($checkapproved[0]->id) && $checkapproved[0]->id == 2)
                                    {
                                        $sql .= "AND bi.`product_grade` = '2' ";
                                    }
                                    else if(isset($checkapproved[0]->id) && $checkapproved[0]->id == 1)
                                    {
                                        $sql .= "AND bi.`product_grade` IN('1','3','4','5') ";
                                    }
                                    $sql .= " AND bl.`status` = '2' AND bl.rejected_by IS NULL";
                                }
                                else
                                {                            
                                    $sql .= "AND bl.`code` = '".$staff_code."'";                                
                                }
                            }
                            else if(isset($data->group) && $data->group == 27)
                            {

                                $sql .= " And bl.`status` = '4' AND bl.rejected_by IS NULL AND ar.`id` IN(SELECT a.`area_id` FROM hr.staff sta INNER JOIN hr.asm a ON(sta.`id` = a.`staff_id`) WHERE sta.`code` = '".$staff_code."')";
               
                            }
                            else if(isset($data->group) && $data->group == 10)
                            {

                      if($staff_code == "5800892" ){ // if นี้ เพิ่มเพื่อให้ เต๋อ มองเห็นการอนุมัติ
                        $sql .= "AND bl.`status` = '4' AND bl.rejected_by IS NULL AND ar.`id` IN(SELECT a.`area_id` FROM hr.staff sta INNER JOIN hr.asm a ON (sta.`id` = a.`staff_id`) WHERE sta.`code` = '".$staff_code."' )";
                      }else{

                        if(count($checkapproved) > 0)
                        {                              
                            if(isset($checkapproved[0]->id) && $checkapproved[0]->id == 5){
                                $sql .= " AND  bl.`status` = '6' AND bl.rejected_by IS NULL";
                            }else{
                                $sql .= "  (bl.`status` = '5' AND bl.rejected_by IS NULL AND auth_area_director = '".$staff_code."')
                                OR (bl.`status` = '7' AND u.`department` = '24')";
                            }
                        }
                        else
                        {                            
                            $sql .= " bl.`code` = '".$staff_code."'";
                        }
                      }
                  }else{
                    $sql = "";
                  }
              }

            if($sql != ""){
                $pending = DB::connection(MYSQL_MASTER)->select($sql); 
                $dataall['pending'] = (int)$pending[0]->pending_count + (int)$pending_manager[0]->pending_count;
            }else{
                $dataall['pending'] = 0;
            }
            
            return $dataall;          
        }          
           
    }

    static public function getBadge($staff_code){

        $pending = 0;      
        $pendingCount = 0;
        $readCount = 0;  
        $returnCount = 0;    
        $badge = 0;
        $approved_status = 0;  
        $data = array(
            "pendingCount"=>0,
            "readCount"=>0,
            "returnCount"=>0,
            "badge"=>0,
            "poCount"=>0,
            "prCount"=>0
        );
        
        $last_date = date('Y-m-d', strtotime('-60 days'));
        $today = date('Y-m-d');

        // 1 = Approved, 2 = Wait appoved by Admin, 3 = Wait appoved by ASM, 4 = Wait appoved by RD, 5 = Wait appoved by Area Director, 6 = Wait appoved by Operation Director
        //, 7 = Wait appoved by Manager, 9 = No Approved, 11 = wms appoved, 12 = wms no appoved, 13 = get product, 14 = return product

        //readCount
            $sql_read = "SELECT COUNT(bl.code) AS readCount FROM warehouse.`borrowing_list` bl WHERE bl.read_data = '1' AND bl.code = '".$staff_code."' GROUP BY bl.code";
            $homecount =  DB::connection(MYSQL_SLAVE)->select($sql_read);

            if($homecount)
            {
                $data["readCount"] = (int)$homecount[0]->readCount;
            }

        //pendingCount
            //$borrowing_sn=" SELECT b.borrowing_sn FROM warehouse.borrowing_approved_tran b WHERE b.staff_code='".$staff_code."' AND b.approve_date IS NULL ";
            $borrowing_sn=" SELECT b.borrowing_sn FROM warehouse.borrowing_approved_tran b WHERE b.staff_code='".$staff_code."' AND b.approve_date IS NULL AND b.rank in(SELECT min(bb.rank) FROM warehouse.borrowing_approved_tran bb WHERE bb.borrowing_sn=b.borrowing_sn AND bb.approve_date IS NULL order by bb.rank )";

            $sqlPending = "SELECT COUNT(DISTINCT(bl.sn)) AS pending_count FROM warehouse.borrowing_list bl LEFT JOIN oppohr.users u on bl.code = u.staff_code LEFT JOIN oppohr.oppo_staff os ON(os.staff_code = bl.code) LEFT JOIN oppohr.user_group ug ON(u.user_group = ug.id) WHERE 1=1 and  bl.sn IN (".$borrowing_sn.") AND bl.status != '9' ";
            $sqlPending .= " AND bl.`created_date` >= '".$last_date." 00:00:00'";
            $sqlPending .= " AND bl.`created_date` <= '".$today." 23:59:59'";
            
            $pending =  DB::connection(MYSQL_SLAVE)->select($sqlPending);    
            if($pending)
            {
                $data["pendingCount"] = (int)$pending[0]->pending_count;
            }
        
            //returnCount
            $return_date = date('Y-m-d', strtotime('+15 days'));
            $return_borrowing_sn=" SELECT b.borrowing_sn FROM warehouse.borrowing_approved_tran b WHERE b.staff_code='".$staff_code."' AND b.approve_date IS NOT NULL";
            $sqlreturn = "SELECT COUNT(DISTINCT(bl.id)) AS return_count FROM warehouse.borrowing_list bl LEFT JOIN oppohr.users u on bl.code = u.staff_code LEFT JOIN warehouse.borrowing_item bi ON (bl.sn = bi.sn) LEFT JOIN oppohr.oppo_staff os ON (os.staff_code = bl.code) LEFT JOIN user_group ug ON (u.user_group = ug.id) WHERE 1=1 AND bl.status = 13 AND bl.sn IN (".$return_borrowing_sn.")  AND bl.`return_date` <= '".$return_date." 23:59:59'";  
            $return =  DB::connection(MYSQL_SLAVE)->select($sqlreturn);    
            if($return)
            {
                $data["returnCount"] = (int)$return[0]->return_count;
            }

        return $data;            
    }


}
