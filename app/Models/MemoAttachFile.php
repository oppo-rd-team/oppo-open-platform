<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemoAttachFile extends Model
{
	public $primaryKey = 'id';
    public $incrementing = true;
    protected $connection = MYSQL_MASTER;
    protected $fillable = [
        'id', 'memo_id', 'file', 'type', 'created_date'
    ];
    protected $table = 'oppohr.memo_attach_file';
}
