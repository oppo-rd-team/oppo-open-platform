<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\OppoStaff;
use Illuminate\Support\Facades\Hash;
use App\Models\OpenPlatformUsers;
use Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Response;

class AuthController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth:api', ['except' => ['login', 'register']]);
  }

  public function register(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'staff_code' => 'required|string'
    ]);

    if ($validator->fails()) {
      $result["error"] = $validator->errors();
      $result["status"] = false;
      return response::json($result, 500);
    }

    $result = OppoStaff::where("staff_code", $request->staff_code)->first();

    if (!empty($result)) {

      $user = OpenPlatformUsers::where("staff_code", $request->staff_code)->first();
      if (empty($user)) {
        $user = OpenPlatformUsers::create([
          'staff_id' => $result->id,
          'staff_code' => $request->staff_code,
          'password' => bcrypt($this->getPassword($request->staff_code)),
        ]);
      }

      $token = auth()->login($user);

      return $this->respondWithToken($token);
    } else {
      $result["error"] = "user not allowed";
      $result["status"] = false;
      return response::json($result, 500);
    }
  }

  public function getPassword($code)
  {
    return "oppo" . $code . "@!#" . $code;
  }

  public function login(Request $request)
  {
    // $credentials = request(['staff_code', 'password']);

    $credentials["staff_code"] = $request->staff_code;
    $credentials["password"] = $this->getPassword($request->staff_code);

    //return response()->json($credentials);
    if (!$token = auth()->attempt($credentials)) {


      return response()->json(['error' => 'Unauthorized'], 401);
    }

    return $this->respondWithToken($token);
  }

  public function me()
  {
    return response()->json(auth()->user());
  }

  public function logout()
  {
    auth()->logout();

    return response()->json(['message' => 'Successfully logged out']);
  }

  public function refresh()
  {
    return $this->respondWithToken(auth()->refresh());
  }

  protected function respondWithToken($token)
  {
    return response()->json([
      'status' => true,
      'access_token' => $token,
      'expires_in' => auth()->factory()->getTTL() * 60
    ]);
  }
}
