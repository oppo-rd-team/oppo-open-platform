<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemoCC extends Model
{
    public $primaryKey = 'id';
    protected $table = 'oppohr.memo_cc';
    protected $connection = MYSQL_MASTER; 
}
