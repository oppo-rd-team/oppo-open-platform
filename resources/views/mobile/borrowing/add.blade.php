<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Create Borrowing Request Items</title>
    <!-- Scripts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <link rel="stylesheet" href="{{ asset('lib') }}/image-uploader.css">
    <script src="{{ asset('lib') }}/image-uploader.js"></script>
    <style>
        .div-main {
            margin: 10px;
        }

        .div-in {
            margin: 5px;
        }

        .div-f-center {
            margin: auto;
            left:46%;
            top:40%;
            position: fixed;
        }

        .txt-green{
            font-weight: bold;
            color:green;
        }

        .txt-orange{
            font-weight: bold;
            color:orange;
        }

        .txt-red{
            font-weight: bold;
            color:red;
        }

        .txt-purple{
            font-weight: bold;
            color:purple;
        }

        .need,.error{
        	color:red;
        }

                @media screen and (max-width: 1366px) {
            body {
                font-size: 15px;
            }

            nav ul li a {
                font-size: 1.1em;

            }
        }

        @media screen and (max-width: 992px) {
            main {
                margin: 2rem 0;
            }

            nav {
                margin-left: -10em;
            }
        }

        @media screen and (max-width: 786px) {
            body {
                font-size: 14px;
            }

            nav {
                display: none;
            }

            .container {
                width: 80%;
            }
        }

        @media screen and (max-width: 450px) {
            .container {
                width: 90%;
            }
        }
    </style>
</head>

<body>
    
    <div id="showloading" class="spinner-border text-success div-f-center" style="width: 5rem; height: 5rem;" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <div id='loaddata' class="div-main"></div>

</body>

<script type="text/javascript" src="https://appdownload.myoppo.com/appMyOPPORelease/JSSDK/jssdk.min.js"></script>
{{-- <script src="{{ asset('js/mobile.js') }}" t type="text/javascript"></script> --}}
<script type="text/javascript">

    

	$(document).on('focus','#date_borrow',function() {
        $(this).attr('type','date');
        if($('#date_return').val() != ''){
            $('#date_borrow').attr('max',$('#date_return').val())
        }
    });

	$(document).on('focusout','#date_borrow',function() {
        $(this).attr('type','text');
    });

	$(document).on('focus','#date_return',function() {
        $(this).attr('type','date');
        if($('#date_borrow').val() != ''){
            $('#date_return').attr('min',$('#date_borrow').val())
        }
    });

	$(document).on('focusout','#date_return',function() {
        $(this).attr('type','text');
    });

    $(document).on('click','.additem',function() {
    	$('#exampleModal').modal('show');
    });

    $(document).on('change','#delivery', function() {
        if(this.value == 3){
            $('div#showaddress').removeClass('hidden');
        }else{
            $('div#showaddress').addClass('hidden');
        }
    });

    $(document).on('change','#cat_id',function() {
    	if(this.value != ''){
    		$.ajax({
	            url: '{{ route('get.good') }}',
	            type: 'GET',
	            headers: {
	                Authorization: 'Bearer ' + getCookie("OPEN-TOKEN")
	            },
	            data: {
	                cat_id: this.value,
	                "_token": "{{ csrf_token() }}",
	            },
	            success: function(data){
	                if(data)
	                {
	                    $('#model').html(data);
	                }
	            }
	        });
    	}
    });

    $(document).on('change','#model',function() {
    	if(this.value != ''){
    		$.ajax({
	            url: '{{ route('get.color') }}',
	            type: 'GET',
	            headers: {
	                Authorization: 'Bearer ' + getCookie("OPEN-TOKEN")
	            },
	            data: {
	                good_id: this.value,
	                "_token": "{{ csrf_token() }}",
	            },
	            success: function(data){
	                if(data)
	                {
	                    $('#color').html(data);
	                }
	            }
	        });
    	}
    });

    // $(document).on('change','input[name=filedata]',function() {
    //     var arr = [];
    //     for (var i = 0; i < $(this).get(0).files.length; ++i) {
    //         arr.push($(this).get(0).files[i].name);
    //     }

    //     $("input[name=file]").val(arr);
    // });

    $(document).on('click','input[type="checkbox"]',function() {
    	if($(this).prop("checked") == true){
    		$('button#send_request').prop('disabled', false);
    	}else{
    		$('button#send_request').prop('disabled', true);
    	}
    });

	$(document).on('click','#cancle',function() {
		window.location.href = "{{ env('APP_URL') }}mobile/borrowing-lists?isHybridWebView=true&id";
	});

    var length = 0;
    $(document).on('click','#send',function() {
    	length++;
        AddItem(length);
    });

	getFormRequest();
	async function getFormRequest()
    {
        await LoadFormRequest();
    }

    async function LoadFormRequest() {
    	await ClearDiv();
    	$.ajax({
            url: '{{ route('load.borrowing_form') }}',
            type: 'GET',
            headers: {
                Authorization: 'Bearer ' + getCookie("OPEN-TOKEN")
            },
            data: {
                request: 1,
                "_token": "{{ csrf_token() }}",
            },
            beforeSend: function(){
                $('#showloading').css('display','block');
            },
            success: function(data){
                if(data)
                {
                    $('#showloading').css('display','none');
                    $('#loaddata').html(data);
                }
            }
        });
    }

    async function AddItem(length)
    {
        const complete = await CheckValue();
        if(complete === true){
            await CreateItem(length);
        }
    }

    async function CreateItem(length)
    {
        var html = '';
        var field = '';
        var catdata = $('#cat_id').val();
        var modeldata = $('#model').val();
        var colordata = $('#color').val();
        var qty = $('#number').val();

        let cat = catdata.split("x");
        let model = modeldata.split("x");
        let color = colordata.split("x");

        html += '<div class="card mb-3" id="column_'+length+'">';
        html += '<div class="card-header text-right"><span class="material-icons" style="color:red" onclick="removefield('+length+')">delete_sweep</span></div>';
        html += '<div class="card-body text-center">';
        html += '<p class="card-text"><b>Category</b>: '+cat[1]+'</p>';
        html += '<p class="card-text"><b>Model</b>: '+model[1]+'</p>';
        html += '<p class="card-text"><b>Color</b>: '+color[1]+'</p>';
        html += '<p class="card-text"><b>Quantity</b>: '+qty+'</p>';
        html += '</div>';
        html += '</div>';

        field += '<div id="field_'+length+'">';
        field += '<input type"hidden" name="cat_ids[]" value="'+cat[0]+'">';
        field += '<input type"hidden" name="model_ids[]" value="'+model[0]+'">';
        field += '<input type"hidden" name="color_ids[]" value="'+color[0]+'">';
        field += '<input type"hidden" name="qtys[]" value="'+qty+'">';
        field += '</div>';

        $('.showitem').append(html);
        $('.sendvalue').append(field);

        $('#form-additem')[0].reset();
        $('#exampleModal').modal('hide');
    }

    async function CheckValue()
    {
        let error = true;
        if($('#cat_id').val() == ''){
            error = false;
            Swal.fire({
                icon: 'warning',
                title: '',
                html: '<b>Please Select Category !.</b>',
                showCloseButton: true,
                confirmButtonColor: '#3085d6',
            });
            return false;
        }

        if($('#model').val() == ''){
            error = false;
            Swal.fire({
                icon: 'warning',
                title: '',
                html: '<b>Please Select Model !.</b>',
                showCloseButton: true,
                confirmButtonColor: '#3085d6',
            });
            return false;
        }

        if($('#color').val() == ''){
            error = false;
            Swal.fire({
                icon: 'warning',
                title: '',
                html: '<b>Please Select Color !.</b>',
                showCloseButton: true,
                confirmButtonColor: '#3085d6',
            });
            return false;
        }

        if($('#number').val() == ''){
            error = false;
            Swal.fire({
                icon: 'warning',
                title: '',
                html: '<b>Please Enter Quantity !.</b>',
                showCloseButton: true,
                confirmButtonColor: '#3085d6',
            });
            return false;
        }

        return error;
    }

    async function removefield(i)
    {
    	await RemoveHiddenValue(i);
    	await RemoveColumn(i);
    }

    async function RemoveHiddenValue(i)
    {
    	$('div#field_'+i).remove();
    }

    async function RemoveColumn(i)
    {
    	$('div#column_'+i).remove();
    }

    async function ClearDiv()
    {
    	$('#loaddata').html('');
    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

</script>
</html>
