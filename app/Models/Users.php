<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
	public $primaryKey = 'id';
    protected $table = 'oppohr.users';
    protected $connection = MYSQL_MASTER;
}
