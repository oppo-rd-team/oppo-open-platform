<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Borrowing</title>
    <!-- Scripts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <style>
        .div-main {
            margin: 10px;
        }

        .div-in {
            margin: 5px;
        }

        .txt-green{
            font-weight: bold;
            color:green;
        }

        .txt-orange{
            font-weight: bold;
            color:orange;
        }

        .txt-red{
            font-weight: bold;
            color:red;
        }

        .txt-purple{
            font-weight: bold;
            color:purple;
        }

        .div-f-center {
            margin: auto;
            left: 46%;
            top: 40%;
            position: fixed;
            width: 5rem;
            height: 5rem;
            display: none;
        }

        .div-button-center {
            margin: auto;
            left: 30%;
            bottom: 5%;
            position: fixed;
        }

    </style>
</head>

<body>
    @php
        $request_id = $params['id'];
    @endphp

    {{-- <div class="div-button-center" id="btn-action">
        <button type="button" class="btn btn-success text-white"
            onclick="oncickMemo(1,{{ @$memo->id }})">Approve</button>
        <button type="button" class="btn btn-danger text-white"
            onclick="oncickMemo(2,{{ @$memo->id }})">Reject</button>
        <button type="button" class="btn btn-danger text-white"
            onclick="oncickMemo(3,{{ @$memo->id }})">Cancel</button>
    </div> --}}

    <div id="showloading" class="spinner-border text-success div-f-center" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <div id="showdetail"></div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Are you sure? you want to confirm this
                        information</h2>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body" id="action-remark">
                    <form>
                      <div class="mb-3">
                        <textarea class="form-control" id="message-action" placeholder="Remark..."></textarea>
                      </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal">Close</button>
                    <button type="button" id="btn-approve" class="btn btn-success text-white">Confirm</button>
                </div>
            </div>
        </div>
    </div>

</body>

<script type="text/javascript" src="https://appdownload.myoppo.com/appMyOPPORelease/JSSDK/jssdk.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script type="text/javascript">
    var myModal = new bootstrap.Modal(document.getElementById("exampleModal"), {});

    $(function(){
        var request_id = "{{ $request_id }}";
        getBorrowingDetail(request_id);
    });

    
    async function getBorrowingDetail(request_id)
    {
        await LoadBorrowingDetail(request_id);
    }

    function LoadBorrowingDetail(request_id) {
        $('#showdetail').html('');
        if(request_id != ''){
            $.ajax({
                url: '{{ route('load.borrowing_detail') }}',
                type: 'POST',
                headers: {
                    Authorization: 'Bearer ' + getCookie("OPEN-TOKEN")
                },
                data: {
                    request_id: request_id,
                    "_token": "{{ csrf_token() }}",
                },
                beforeSend: function(){
                    $('#showloading').css('display','block');
                },
                success: function(data){
                    if(data)
                    {
                        $('#showloading').css('display','none');
                        $('#showdetail').html(data);
                    }
                }
            });
        }
    }

    async function BorrowingAction(type, borrowing_id, sn, rq) {

        //1 = Approve
        //2 = Reject

        if(type == 2){
            $("#action-remark").hide();
        }else{
            $("#action-remark").show();
        }

        await myModal.show();
        document.getElementById("btn-approve").onclick = function fun() {
            ActionSave(type, borrowing_id, sn, rq);
        }
    }

    async function ActionSave(type, borrowing_id, sn, rq) {
        var remark = $("#message-action").val();
        var isapprove = 'false';

        // console.log(rq)

        if(type == 2){
            isapprove = 'true';
        }

        await myModal.hide()
        $.ajax({
            url: '{{ route('borrowing.action') }}',
            type: 'POST',
            headers: {
                Authorization: 'Bearer ' + getCookie("OPEN-TOKEN")
            },
            data: {
                status_id: type,
                borrowing_id: borrowing_id,
                sn:sn,
                rq:rq,
                isApproved:isapprove,
                remark:remark,
                "_token": "{{ csrf_token() }}",
            },
            beforeSend: function(){
                $('#showloading').css('display','block');
            },
            success: function(data){
                $('#showloading').css('display','none');
                // console.log(data)
                if(data == 1){
                    Swal.fire({
                        icon: 'success',
                        title: 'Success',
                        text: 'Approve Success',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    setTimeout(function(){ 
                        location.reload();
                    }, 2000);
                }else if(data == 2){
                    Swal.fire({
                        icon: 'success',
                        title: 'Success',
                        text: 'Reject Success',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    setTimeout(function(){ 
                        location.reload();
                    }, 2000);
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Something Wrong !.',
                        showConfirmButton: false,
                        timer: 4000
                    });
                }
            }
        });
    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
</script>

</html>
