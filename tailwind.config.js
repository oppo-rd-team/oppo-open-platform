module.exports = {
  purge: [
    './resources/views/mobile/*.blade.php',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      'tablet': '640px',
      'laptop': '1024px',
      'desktop': '1280px',
    },
    extend: {},
  },
  variants: {
    extend: {
      transitionDuration: ['hover', 'focus'],
      alignItems: ['hover', 'focus'],
    },
  },
  plugins: []
}
