<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/customer-so', 'CustomerController@index');
Route::get('/customer-so-order', 'CustomerController@order');
Route::get('/customer-so-view', 'CustomerController@view');

Route::prefix('web')->group(function () {
    Route::get('/e-memo', 'EmemoController@redirectToHR');
});

Route::prefix('mobile')->group(function () {
    Route::get('/e-memo', 'EmemoController@mobileIndex');
    Route::get('/e-memo-view', 'EmemoController@mobileView');

    Route::get('/borrowing', 'BorrowingController@index');
    Route::get('/borrowing-lists','BorrowingController@lists');
    Route::get('/borrowing-detail', 'BorrowingController@detail');
    Route::get('/borrowing-pending', 'BorrowingController@pending');
    Route::get('/borrowing-return', 'BorrowingController@return');
    Route::get('/borrowing-add','BorrowingController@add');

    Route::get('/staff-privilege', 'StaffPrivilegeController@index');
    Route::get('/staff-privilege-view', 'StaffPrivilegeController@view');
    Route::get('/staff-privilege-create', 'StaffPrivilegeController@create');
    
    Route::get('/customer-so-view', 'CustomerController@view');
    
});
