<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{
    public $primaryKey = 'id';
    protected $table = 'warehouse.shipping_address';
    protected $connection = MYSQL_SLAVE; 
}
