<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>My Request</title>
    <!-- Scripts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>

    <style>
        .div-main {
            margin: 10px;
        }

        .div-in {
            margin: 5px;
        }

        .div-f-center {
            margin: auto;
            left:46%;
            top:40%;
            position: fixed;
        }

        .txt-green{
            font-weight: bold;
            color:green;
        }

        .txt-orange{
            font-weight: bold;
            color:orange;
        }

        .txt-red{
            font-weight: bold;
            color:red;
        }

        .txt-purple{
            font-weight: bold;
            color:purple;
        }

    </style>
</head>

<body>
    
    <div id="loading" class="spinner-border text-success div-f-center" style="width: 5rem; height: 5rem;" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <form>
        <div class="row">
            <div class="col-sm-12">
                <p>
                    <a style="float:right;" data-bs-toggle="collapse" role="button" onclick="collapseClick()"
                        aria-expanded="false" aria-controls="collapseExample">
                        <span class="material-icons">manage_search</span>
                    </a>

                    <a style="float:right;" data-bs-toggle="collapse" role="button" onclick="AddRequest()"
                        aria-expanded="false" aria-controls="collapseExample">
                        <span class="material-icons">add</span>
                    </a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="collapse" id="collapse-search">
                <div class="card card-body">
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Request Number</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="rq-number" placeholder="Request Number">
                        </div>
                    </div>

                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Select Date Borrow</label>
                        <div class="col-sm-10">
                            <input type="date" id="date-borrow" class="form-control datepicker"
                                data-date-format="mm/dd/yyyy">
                        </div>
                    </div>


                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Select Date Return</label>
                        <div class="col-sm-10">
                            <input type="date" id="date-return" class="form-control datepicker"
                                data-date-format="mm/dd/yyyy">
                        </div>
                    </div>

                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                            <select class="form-select" id="borrow-status" aria-label="Default select example">
                                <option selected></option>
                                <option value="-1">Select Status All</option>
                                <option value="0">Waiting approve from Accounting</option>
                                <option value="1">Prepare product</option>
                                <option value="2">Waiting for approval from Admin</option>
                                <option value="7">Waiting approval from manager</option>
                                <option value="9">Rejected</option>
                                <option value="11">Ready to shipping</option>
                                <option value="12">RQ cancled by the account</option>
                                <option value="15">RQ cancle by warehouse</option>
                                <option value="13">Staff Receive</option>
                                <option value="14">Returned</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="reset" id="btn-reset" class="btn btn-danger text-white">Reset</button>
                        <button type="button" id="btn-search" class="btn btn-success text-white"
                            onclick="searchAction()">Search</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div id='loaddata' class="div-main"></div>

</body>

<script type="text/javascript" src="https://appdownload.myoppo.com/appMyOPPORelease/JSSDK/jssdk.min.js"></script>
{{-- <script src="{{ asset('js/mobile.js') }}" t type="text/javascript"></script> --}}
<script type="text/javascript">

    getBorrowingList();
    async function getBorrowingList() {

        var request_number = $('#rq-number').val();
        var borrow_status = $('#borrow-status').val();
        var borrow_date = $('#date-borrow').val();
        var borrow_return = $('#date-return').val();

        await clearDiv();
        await startLoading(true);
        await axios({
            method: 'get',
            url: "{{ env('APP_URL') }}api/mobile/borrowing-lists",
            headers: {
                Authorization: 'Bearer ' + getCookie("OPEN-TOKEN")
            },
            params: {
                rq: request_number,
                status: borrow_status,
                from: borrow_date,
                to: borrow_return,
            }
        }).then((response) => {
                var tr = [];
                $.each(response.data, function(index, value) {
                    tr.push('<div  onclick="viewDetail(' + value.id +')" class="p-6 div-in mx-auto bg-white rounded-xl shadow-md flex items-center space-x-4 transition duration-500 ease-in-out bg-blue-500 hover:bg-red-500 transform hover:-translate-y-1 hover:scale-115 ...">' +
                        '<div class="flex-shrink-0">' +
                        // '<img class="h-12 w-12" src="{{ asset('image/icons/doc.svg') }}" alt="ChitChat Logo"></div><div>' +
                        '</div><div>' +
                        '<div class="flex-shrink-0">' +
                        '<div class="text-xl font-medium text-black">' + value.rq + "</div>" +
                        '<p class="text-gray-500">Request Date : ' + value.created_date + '</p>' +
                        '<p class="text-gray-500">Name : ' + value.username + '</p>' +
                        '<p class="text-gray-500">Status : ' + value.flag_status +
                        '</p></div></div>');
                });

            $("#loaddata").append(tr);
        });
        await startLoading(false);
    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function clearDiv() {
        var list = document.getElementById("loaddata");
        list.innerHTML = '';
    }

    function viewDetail(id) {
        window.open("{{ env('APP_URL') }}mobile/borrowing-detail?isHybridWebView=true&id="+id);
    }

    function startLoading(type){
        if(type){
            $("#loading").show()
        }else{
            $("#loading").hide()
        }
    }

    async function searchAction() {
        await collapseClick();
        await getBorrowingList();
    }

    function AddRequest() {
        window.open("{{ env('APP_URL') }}mobile/borrowing-add?isHybridWebView=true&id");
    }

    function collapseClick() {
        $("#collapse-search").collapse('toggle');
    }

</script>

</html>
