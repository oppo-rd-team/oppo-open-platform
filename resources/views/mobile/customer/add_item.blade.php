<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>Customer SO</title>
    <!-- Scripts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat"> --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

    <style>
        .div-in {
            margin: 15px;
        }

        .div-item {
            margin-top: 5px;
        }

        .loading-custom {
            margin: auto;
            left: 40%;
            top: 40%;
            position: fixed;
            z-index: 1;
        }

        .loading-main {
            width: 100%;
            height: 100%;
            position: fixed;
            z-index: 2;
            opacity: 0.4;
        }

    </style>

</head>

<body>

    <div id="loading" class="loading-main">
        <div class="spinner-border text-success loading-custom" style="width: 5rem; height: 5rem;" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>
    </div>

    <form id="form-main" class="needs-validation" novalidate>
        <div class="row">
            <div class="card card-body div-in">
                <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Type</label>
                    <div class="col-sm-10">
                        <select class="form-select" id="type" aria-label="Default select example" disabled required>
                            <option selected></option>
                        </select>
                    </div>
                </div>

                <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                        <select class="form-select" id="address" aria-label="Default select example" disabled
                            required>
                            <option selected></option>
                        </select>
                    </div>
                </div>

                <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">Remark</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="remark" rows="3" disabled></textarea>
                    </div>
                </div>

                <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label">CN CP</label>
                    <div class="col-sm-10">
                        <select class="form-select" id="cn-cp" onchange="changeCn()"
                            aria-label="Default select example" disabled>
                            <option selected></option>
                        </select>
                    </div>
                </div>

                <div class="mb-3 row">
                    <div id='cn-fetch'></div>
                </div>

                <div class="d-grid gap-2">
                    <button class="btn btn-primary text-white" type="button" onclick="openAddItem()">Add Item</button>
                    <button class="btn btn-success text-white" type="submit" id="open-order" onclick="openOrder()"
                        disabled>Open Order</button>
                </div>

                <div class="mb-3 row">
                    <div id='item-fetch'></div>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <form id="form-item" class="needs-validation" novalidate>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Add Item</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <label class="col-form-label">Category</label>
                        <div class="col-sm-12">
                            <select class="form-select" id="category" aria-label="Default select example"
                                onchange="setModel()" required>
                                <option selected></option>
                            </select>
                        </div>

                        <label class="col-form-label">Model</label>
                        <div class="col-sm-12">
                            <select class="form-select" id="model" aria-label="Default select example"
                                onchange="setColor()" disabled required>
                                <option selected></option>
                            </select>
                        </div>

                        <label class="col-form-label">Color</label>
                        <div class="col-sm-12">
                            <select class="form-select" id="color" aria-label="Default select example" disabled
                                required>
                                <option selected></option>
                            </select>
                        </div>

                        <label class="col-form-label">Quantity</label>
                        <div class="col-sm-12">
                            <input type="number" class="form-control" min="1" id="quantity" required>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" onclick="addItem()" class="btn btn-primary">Add Item</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="modalCN" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="modalCNLabel" aria-hidden="true">
        <form id="form-cn" class="needs-validation" novalidate>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalCNLabel">CN-CP</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <label id="txtCn" class="col-form-label">Available balance :</label>
                        <div class="col-sm-12">
                            <input type="number" class="form-control" min="1" id="available-cn" required>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" onclick="addCn()" id="btn-add-cn" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</body>

<script type="text/javascript" src="https://appdownload.myoppo.com/appMyOPPORelease/JSSDK/jssdk.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

{{-- <script src="{{ asset('js/mobile.js') }}" t type="text/javascript"></script> --}}
<script type="text/javascript">
    var itemList = [];
    var cnList = [];
    var category = null;
    var good = null;
    var list_cn = null;
    var cn_p = null;
    var distributor_detail = null;
    var distributor_id = 30344;
    var addItemModal = new bootstrap.Modal(document.getElementById('staticBackdrop'));
    var cnModal = new bootstrap.Modal(document.getElementById('modalCN'));
    getAPI();

    async function setDefaultData() {
        if (localStorage.getItem("detail") !== null) {
            itemList = await JSON.parse(localStorage.getItem("itemList") || "[]");
            cnList = await JSON.parse(localStorage.getItem("cnList") || "[]");
            detail = await JSON.parse(localStorage.getItem("detail") || "[]");
            distributor_id = await localStorage.getItem("distributor_id");
            distributor_detail = await JSON.parse(localStorage.getItem("distributor_detail") || "[]");

            selectElement('remark', detail.remark);
            selectElement('type', detail.type_order);
            selectElement('address', detail.address);

            await setItemList();
            await setItemCn();
        }
    }

    function selectElement(id, valueToSelect) {
        let element = document.getElementById(id);
        element.value = valueToSelect;
    }

    async function openAddItem() {
        await setCategory();
        addItemModal.toggle();
    }

    function setCategory() {
        $('#category').empty();
        var f1 = [];
        f1.push('<option></option>');

        $.each(category, function(index, value) {
            f1.push('<option value=' + value.id + '>' + value.cat_name +
                '</option>');
        });
        $("#category").append(f1);
    }

    function setModel() {
        $('#model').empty();
        $('#model').prop('disabled', false);
        var category_id = $("#category").val();
        var f1 = [];
        f1.push('<option></option>');

        $.each(good, function(index, value) {
            if (value.cat_id == category_id) {
                f1.push('<option value=' + value.id + '>' + value.desc +
                    '</option>');
            }
        });
        $("#model").append(f1);
    }

    function setColor() {
        $('#color').empty();
        $('#color').prop('disabled', false);
        var model_id = $("#model").val();
        var f1 = [];
        f1.push('<option></option>');

        $.each(good, function(index, value) {
            if (value.id == model_id) {
                $.each(value.color_list, function(index2, value2) {
                    f1.push('<option value=' + value2.color_id + '>' + value2.color_name + '</option>');
                });
                return false;
            }

        });

        $("#color").append(f1);
    }

    async function addItem() {
        const category_id = $("#category").val();
        const category_name = $('#category option:selected').text();
        const model_id = $("#model").val();
        const model_name = $('#model option:selected').text();
        const color_id = $("#color").val();
        const color_name = $('#color option:selected').text();
        const quantity = $("#quantity").val();
        var d = true;

        if (category_id && quantity >= 1 && model_id && color_id) {

            var price = 0;
            await $.each(good, function(index, value) {
                if (model_id == value.id) {
                    price = value.price;
                    return false;
                }
            });

            await $.each(itemList, function(index, value) {
                if (model_id == value.good_id && color_id == value.good_color) {
                    itemList[index].qty = parseInt(quantity) + parseInt(value.qty);
                    d = false;
                    return false;
                }
            });

            if (d) {
                const list = {
                    "cat_id": category_id,
                    "category_name": category_name,
                    "qty": quantity,
                    "good_id": model_id,
                    "good_name": model_name,
                    "good_color": color_id,
                    "color_name": color_name,
                    "price": price
                }
                await itemList.push(list);
            }

            await setItemList();
            await $("#form-item").trigger('reset');
            addItemModal.toggle();
        }
    }

    // <span class="visually-hidden">Loading...</span>
    // <label class="col-sm-2 col-form-label"></label>
    //                 <div class="col-sm-10">
    //                 </div>
    async function setItemList() {
        var list = document.getElementById("item-fetch"); // Get the <ul> element with id="myList"
        list.innerHTML = '';

        var tr = [];
        $.each(itemList, function(index, value) {
            tr.push('<div class="card border-secondary div-item">' +
                '<div class="card-header">' + value.category_name + "</div>" +
                '<div class="card-body text-dark">' +
                '<div class="row">' +
                '<div class="col-10">' +
                '<p class="card-text">Model : ' + value.good_name + "</p>" +
                '<p class="card-text">Color : ' + value.color_name + '</p>' +
                '<p class="card-text">Quantity : ' + value.qty + '</p>' +
                '</div><div class="col-2">' +
                '<span class="material-icons" onclick="deleteItem(' + index +
                ')">delete_outline</span>' +
                '</div></div></div></div>');
        });
        $("#item-fetch").append(tr);


        if (itemList.length >= 1) {
            $('#open-order').prop('disabled', false);
        } else {
            $('#open-order').prop('disabled', true);
        }
    }

    async function deleteItem(index) {
        await itemList.splice(index, 1);
        await setItemList();
    }

    async function getAPI() {
        await startLoading(true);
        await yezi.auth.getNativeData({
            success: function(result) {
                var obj = JSON.parse(result.data);

                $.each(obj.userInfo.extentions.depotList, function(index, value) {
                    distributor_id = value.distributorId
                    return false;
                });

            },
            error: function(error) {
                // LoginName = null;
            }
        });
        await getFilter(distributor_id);
        await getCnCp(distributor_id);
        await setDefaultData();
        await startLoading(false);
    }

    async function getFilter(distributor_id) {
        await axios({
            method: 'get',
            url: "{{ env('APP_URL') }}api/customer/filter",
            params: {
                distributor_id: distributor_id,
            }
        }).then(function(response) {

            if (response.data.distributor === null) {
                yezi.ui.toast("Sorry, Distributor is not found in WMS System!!");
            } else {
                var f1 = [];
                var f2 = [];
                $.each(response.data.order_type, function(index, value) {
                    f1.push('<option value=' + value.order_type_id + '>' + value.order_type_name +
                        '</option>');
                });

                $.each(response.data.shipping_address, function(index, value) {
                    f2.push('<option value=' + value.shipping_id + '>' + value.address + " " + value
                        .district_name + " " + value.amphure_name + " " + value.provice_name +
                        " " +
                        value.zipcode + '</option>');
                });

                $("#type").append(f1);
                $("#address").append(f2);

                distributor_id = response.data.distributor.id;
                category = response.data.category;
                good = response.data.good;
                distributor_detail = response.data.distributor;
            }

        }).catch(function(error) {

        });
    }

    function getCnCp(distributor_id) {
        axios({
            method: 'get',
            url: "{{ env('APP_URL') }}api/customer/cn",
            params: {
                distributor_id: distributor_id,
            }
        }).then(function(response) {
            var f1 = [];
            $.each(response.data, function(index, value) {
                f1.push('<option value=' + value.creditnote_sn + '>' + value.creditnote_type + " : " +
                    value.creditnote_sn + " : ฿" + new Intl.NumberFormat().format(value
                        .total_amount) + '</option>');
            });

            list_cn = response.data;

            $("#cn-cp").append(f1);

        }).catch(function(error) {});
    }

    async function changeCn() {
        const cn = $("#cn-cp option:selected").val();
        const cn_name = $('#cn-cp option:selected').text();
        var check = true;

        await $.each(cnList, function(index, value) {
            if (cn == value.creditnote_sn) {
                check = false;
                return false;
            }
        });

        if (check) {
            if (list_cn.length >= 1) {
                $.each(list_cn, function(index, value) {
                    if (cn == value.creditnote_sn) {
                        cn_p = value;
                        return false;
                    }
                });
            }
            $("#txtCn").text("Available balance : " + new Intl.NumberFormat().format(cn_p.total_amount));
            $("#available-cn").val(cn_p.total_amount);
            document.getElementById("available-cn").setAttribute("max", cn_p.total_amount); // set a new value;
            cnModal.toggle();
        }
    }

    async function addCn() {
        const cn_v = $("#available-cn").val();

        if (cn_v >= 1) {
            const list = {
                "distributor_id": cn_p.distributor_id,
                "creditnote_type": cn_p.creditnote_type,
                "creditnote_sn": cn_p.creditnote_sn,
                "total_amount": cn_p.total_amount,
                "balance_total": cn_p.balance_total,
                "use_total": cn_v
            }

            await cnList.push(list);
            await setItemCn();
            cnModal.toggle();
        }
    }

    function openOrder() {
        const type_order = $("#type").val();
        const type_order_name = $('#type option:selected').text();
        const address = $("#address").val();
        const address_name = $('#address option:selected').text();
        const remark = $("#remark").val();

        if (type_order && address && itemList.length >= 1) {
            const detail = {
                "type_order": type_order,
                "type_order_name": type_order_name,
                "address": address,
                "address_name": address_name,
                "remark": remark,
            }
            localStorage.setItem("detail", JSON.stringify(detail));
            localStorage.setItem("distributor_id", distributor_id);
            localStorage.setItem("distributor_detail", JSON.stringify(distributor_detail));
            localStorage.setItem("itemList", JSON.stringify(itemList));
            localStorage.setItem("cnList", JSON.stringify(cnList));

            // window.open("http://dev-open-platform.test/customer-so-order");
            window.open("{{ env('APP_URL') }}customer-so-order", "_self");
        }
    }

    function setItemCn() {
        var list = document.getElementById("cn-fetch"); // Get the <ul> element with id="myList"
        list.innerHTML = '';

        var tr = [];
        $.each(cnList, function(index, value) {
            tr.push('<div class="card border-success div-item">' +
                '<div class="card-body text-dark">' +
                '<div class="row">' +
                '<div class="col-6">' +
                '<p class="card-text">' + value.creditnote_sn + "</p>" +
                '</div><div class="col-4">' +
                '<p class="card-text text-right">' + new Intl.NumberFormat().format(value.use_total) +
                "</p>" +
                '</div><div class="col-2">' +
                '<span class="material-icons" onclick="deleteCn(' + index +
                ')">delete_outline</span>' +
                '</div></div></div></div>');
        });
        $("#cn-fetch").append(tr);
    }

    async function deleteCn(index) {
        await cnList.splice(index, 1);
        await setItemCn();
    }

    function startLoading(type) {
        if (type) {
            $("#loading").show();
        } else {
            $("#loading").hide();
            $('#type').prop('disabled', false);
            $('#address').prop('disabled', false);
            $('#remark').prop('disabled', false);
            $('#cn-cp').prop('disabled', false);
        }
    }

    $("#form-main").submit(function(e) {
        e.preventDefault();
    });

    $("#form-cn").submit(function(e) {
        e.preventDefault();
    });

    (function() {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function(form) {
                form.addEventListener('submit', function(event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })()
</script>

</html>
