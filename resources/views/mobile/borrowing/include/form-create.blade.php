<form method="post" id="form-create" enctype="multipart/form-data">
	@csrf
	<div class="container mt-2">
		<div class="row">
			<div class="mb-3">
			  	<label for="exampleFormControlInput1" class="form-label"><b>Type Product</b></label>
			  	<select class="form-select" name="product_type" id="product_type">
			  		<option value="">-Choose-</option>
			  		@if(isset($data['product_type']) && !empty($data['product_type']))
			  			@foreach($data['product_type'] as $v => $pro)
			  				<option value="{{ $pro['id'] }}">{{ $pro['name'] }}</option>
			  			@endforeach
			  		@endif
			  	</select>
			  	<small class="error" id="#product_type_error"></small>
			</div>

			<div class="mb-3">
			  	<label for="exampleFormControlInput1" class="form-label"><b>Department</b></label>
			  	<input type="text" class="form-control" name="department" id="department" value="{{ !empty($data['info']->department_name) ? $data['info']->department_name : "" }}" readonly>
			</div>

			<div class="mb-3">
			  	<label for="exampleFormControlInput1" class="form-label"><b>Event Name</b></label>
			  	<input type="text" class="form-control" name="event_name" id="event_name" autocomplete="off">
			</div>

			<div class="mb-3">
			  	<label for="exampleFormControlInput1" class="form-label"><b>Select Date Borrow</b></label>
			  	<input type="text" class="form-control" name="date_borrow" id="date_borrow" autocomplete="off" min="{{ date('Y-m-d') }}">
			</div>

			<div class="mb-3">
			  	<label for="exampleFormControlInput1" class="form-label"><b>Select Date Return</b></label>
			  	<input type="text" class="form-control" name="date_return" id="date_return" autocomplete="off">
			</div>

			<div class="mb-3">
			  	<label for="exampleFormControlInput1" class="form-label"><b>Delivery</b></label>
			  	<select class="form-select" name="delivery" id="delivery">
			  		<option value="">-Choose-</option>
			  		@if(isset($data['delivery']) && !empty($data['delivery']))
			  			@foreach($data['delivery'] as $k => $de)
			  				<option value="{{ $de['id'] }}">{{ $de['name'] }}</option>
			  			@endforeach
			  		@endif
			  	</select>
			</div>

			<div class="mb-3 hidden" id="showaddress">
				<label for="exampleFormControlInput1" class="form-label"><b>Address</b></label>
				<input type="text" class="form-control" name="address" id="address" autocomplete="off">
			</div>

			<div class="mb-3">
			  	<label for="exampleFormControlInput1" class="form-label"><b>Remark</b></label>
			  	<input type="text" class="form-control" name="remark" id="remark" autocomplete="off">
			</div>

			<div class="mb-3">
				<label for="exampleFormControlInput1" class="form-label"><b>Borrowing Items <span class="need">*</span></b></label>
				<button type="button" class="btn btn-primary additem">Add Item</button>
			</div>

			<div class="mb-3">
				<div class="showitem"></div>
				<div class="sendvalue" style="display: none;"></div>
			</div>

			<div class="mb-3 mb-4">
				<hr>
			</div>

			<div class="mb-3">
				<label for="exampleFormControlInput1" class="form-label"><b>Upload the picture and borrowing documents.</b></label>
				<input type="file" class="form-control" name="filedata[]" id="filedata" multiple>
			</div>

			<div class="col-12 mb-3 flex flex-row gap-1.5" id="showimage">
				{{-- <div class="preview" id="img_box">
					<img class="rounded img-thumbnail" width="150px" src='{{ asset('image/icons/exchange.png') }}'>
					<div class="text-center">
						<span class="material-icons" style="color:red">delete_sweep</span>
					</div>
				</div> --}}
			</div>

			<div class="mb-3 mt-4">
				<hr>
			</div>

			<div class="mb-3">
				<div class="form-check">
  					<input class="form-check-input" type="checkbox" id="agree">
  					<label class="form-check-label" for="agree">
    					I agree that THAI OPPO Co.,Ltd will deduct the amount of money and the price that I have borrowed in the attachment at the end of the month if I can not return within the specified period.
  					</label>
				</div>
			</div>

			<div class="mb-3 flex justify-end">
				<button type="submit" class="btn btn-success" id="send_request" disabled>Send Request</button>
				&nbsp;&nbsp;
				<button type="button" class="btn btn-secondary" id="cancle">Cancle</button>
			</div>
		</div>
	</div>
</form>


<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title" id="exampleModalLabel">Add Borrowing Items</h5>
        		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      		</div>
	      	<form method="post" id="form-additem">
	      		@csrf
		      	<div class="modal-body">
		       		<div class="row">
		       			<div class="mb-3">
			       			<label for="exampleFormControlInput1" class="form-label"><b>Category</b></label>
			       			<select class="form-select" name="cat_id" id="cat_id">
						  		<option value="">-Choose-</option>
						  		<option value="12xAccessories">Accessories</option>
						  		<option value="11xPhone">Phone</option>
						  		<option value="17xIOT">IOT</option>
						  	</select>
						</div>
						<div class="mb-3">
			       			<label for="exampleFormControlInput1" class="form-label"><b>Model</b></label>
			       			<select class="form-select" name="model" id="model">
						  	</select>
						</div>
						<div class="mb-3">
			       			<label for="exampleFormControlInput1" class="form-label"><b>Color</b></label>
			       			<select class="form-select" name="color" id="color">
						  	</select>
						</div>
						<div class="mb-3">
			       			<label for="exampleFormControlInput1" class="form-label"><b>Quantity</b></label>
			       			<input type="text" class="form-control" name="number" id="number" autocomplete="off">
						</div>
		       		</div>
		      	</div>
		      	<div class="modal-footer">
	      			<button type="button" class="btn btn-danger" data-bs-dismiss="modal" id="close">Close</button>
	        		<button type="button" class="btn btn-success" id="send">Save</button>
		      	</div>
	      	</form>
   	 	</div>
  	</div>
</div>

<script>
	$(function(){

		$("#form-create").submit(function(e) {
    		e.preventDefault();
    		BorrowingAction(this); //SaveData
	    });
	});

	$(document).on('change','#filedata',function() {
		 imagesPreview(this, 'div#showimage');
	});

	function imagesPreview(input, placeToInsertImagePreview) {
		if (input.files) {
            var filesAmount = input.files.length;
            var countPreview = $('div.preview').length+1;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {

                	$('div#showimage').append('<div class="preview" id="img_box_'+countPreview+'"><img class="rounded img-thumbnail" width="200px" src='+event.target.result+'><div class="text-center"><span class="material-icons" style="color:red" onclick="RemoveTemp('+countPreview+')">delete_sweep</span></div></div>');
                    // $($.parseHTML('<img class="rounded img-thumbnail" width="150px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
	}

	async function RemoveTemp(i)
	{
		await RemoveImage(i);
	}

	async function RemoveImage(i)
	{
		$('div#img_box_'+i).remove();
	}

	async function BorrowingAction(form)
	{
		const complete = await CheckValidate();
		if(complete === true)
		{
			await SaveBorrowing(form);
		}
	}

	async function SaveBorrowing(form)
	{
  		var formData = new FormData($(form)[0]);
		$.ajax({
            url: "{{ route('add.item') }}",
            headers: {
	            Authorization: 'Bearer ' + getCookie("OPEN-TOKEN")
	        },
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function(){
            	$('body').css('overflow','hidden');
            	$('#showloading').css('display','block');
            },
            success: function(data) {
            	$('body').css('overflow','');
            	$('#showloading').css('display','none');
            	if(data == 1){
            		Swal.fire({
					  	icon: 'success',
					  	title: '',
					  	html: '<b>The request was sent successfully. Please wait for approval.</b>',
					  	showCloseButton: true,
					  	confirmButtonColor: '#3085d6',
					  	timer: 2000
					});
            		setTimeout(function(){ 
            			window.location.href = "{{ env('APP_URL') }}mobile/borrowing-lists?isHybridWebView=true&id";
            		}, 2000);
            	}else if(data == 10){
            		Swal.fire({
					  	icon: 'warning',
					  	title: '',
					  	html: '<b>The approval process was not found. Please contact an IT staff.</b>',
					  	showCloseButton: true,
					  	confirmButtonColor: '#3085d6',
					});
            	}else{
            		Swal.fire({
					  	icon: 'error',
					  	title: '',
					  	html: '<b>Not Success.</b>',
					  	showCloseButton: true,
					  	confirmButtonColor: '#3085d6',
					});
            	}
            }
        });
	}

	async function CheckValidate()
	{
		let error = true;
		if($('#product_type').val() == ''){
			error = false;
			Swal.fire({
			  	icon: 'warning',
			  	title: '',
			  	html: '<b>Please Select Type Product !.</b>',
			  	showCloseButton: true,
			  	confirmButtonColor: '#3085d6',
			});
			return false;
		}

		if($('#event_name').val() == ''){
			error = false;
			Swal.fire({
			  	icon: 'warning',
			  	title: '',
			  	html: '<b>Please Enter Event name !.</b>',
			  	showCloseButton: true,
			  	confirmButtonColor: '#3085d6',
			});
			return false;
		}

		if($('#date_borrow').val() == ''){
			error = false;
			Swal.fire({
			  	icon: 'warning',
			  	title: '',
			  	html: '<b>Please Select Date Borrow !.</b>',
			  	showCloseButton: true,
			  	confirmButtonColor: '#3085d6',
			});
			return false;
		}

		if($('#date_return').val() == ''){
			error = false;
			Swal.fire({
			  	icon: 'warning',
			  	title: '',
			  	html: '<b>Please Select Date Return !.</b>',
			  	showCloseButton: true,
			  	confirmButtonColor: '#3085d6',
			});
			return false;
		}

		if($('#delivery').val() == ''){
			error = false;
			Swal.fire({
			  	icon: 'warning',
			  	title: '',
			  	html: '<b>Please Select Delivery !.</b>',
			  	showCloseButton: true,
			  	confirmButtonColor: '#3085d6',
			});
			return false;
		}

		let num_item = $('div.showitem').find('div.card').length;
		if(num_item <= 0){
			error = false;
			Swal.fire({
			  	icon: 'warning',
			  	title: '',
			  	html: '<b>Please Select Borrowing Items !.</b>',
			  	showCloseButton: true,
			  	confirmButtonColor: '#3085d6',
			});
			return false;
		}

		return error;
	}
</script>
