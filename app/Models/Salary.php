<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    public $primaryKey = 'id';
    protected $table = 'oppohr.salary';
    protected $connection = MYSQL_MASTER; 
}
