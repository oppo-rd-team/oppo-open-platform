<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wait For Approve</title>
    <!-- Scripts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>

    <style>
        .div-main {
            margin: 10px;
        }

        .div-in {
            margin: 5px;
        }

        .txt-green{
            font-weight: bold;
            color:green;
        }

        .txt-orange{
            font-weight: bold;
            color:orange;
        }

        .txt-red{
            font-weight: bold;
            color:red;
        }

        .txt-purple{
            font-weight: bold;
            color:purple;
        }

        .div-f-center {
            margin: auto;
            left: 46%;
            top: 40%;
            position: fixed;
            width: 5rem;
            height: 5rem;
            display: none;
        }

    </style>
</head>

<body>
    <div id="showloading" class="spinner-border text-success div-f-center" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>

    <form>
        <div class="row">
            <div class="col-sm-12">
                <p>
                    <a style="float:right;" data-bs-toggle="collapse" role="button" onclick="collapseClick()"
                        aria-expanded="false" aria-controls="collapseExample">
                        <span class="material-icons">manage_search</span>
                    </a>
                    <a id="text-count" style="float:right;margin-right:10px;margin-top:5px;"> </a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="collapse" id="collapse-search">
                <div class="card card-body">
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Request Number</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="rq-number" placeholder="Request Number">
                        </div>
                    </div>

                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Select Date Borrow</label>
                        <div class="col-sm-10">
                            <input type="date" id="date-borrow" class="form-control datepicker"
                                data-date-format="mm/dd/yyyy">
                        </div>
                    </div>


                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Select Date Return</label>
                        <div class="col-sm-10">
                            <input type="date" id="date-return" class="form-control datepicker"
                                data-date-format="mm/dd/yyyy">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" id="btn-reset" class="btn btn-danger text-white">Reset</button>
                        <button type="button" id="btn-search" class="btn btn-success text-white"
                            onclick="searchAction()">Search</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div id="showdata"></div>
</body>

<script type="text/javascript" src="https://appdownload.myoppo.com/appMyOPPORelease/JSSDK/jssdk.min.js"></script>
<script type="text/javascript">

    $(function(){
        getBorrowingPending();
    });

    
    async function getBorrowingPending()
    {
        await clearDiv();
        await LoadBorrowingPending();
    }

    function LoadBorrowingPending() {

        var request_number = $('#rq-number').val();
        var borrow_date = $('#date-borrow').val();
        var borrow_return = $('#date-return').val();

        // $('#showdata').html('');
        $.ajax({
            url: '{{ route('load.borrowing_pending') }}',
            type: 'POST',
            headers: {
                Authorization: 'Bearer ' + getCookie("OPEN-TOKEN")
            },
            data: {
                rq: request_number,
                from: borrow_date,
                to: borrow_return,
                "_token": "{{ csrf_token() }}",
            },
            beforeSend: function(){
                $('#showloading').css('display','block');
            },
            success: function(data){
            	if(data)
            	{
            		$('#showloading').css('display','none');
            		var tr = [];
            		var query = JSON.parse(data);
	                $.each(query, function(index, value) {
	                    tr.push('<div  onclick="viewDetail(' + value.id +')" class="p-6 div-in mx-auto bg-white rounded-xl shadow-md flex items-center space-x-4 transition duration-500 ease-in-out bg-blue-500 hover:bg-red-500 transform hover:-translate-y-1 hover:scale-115 ...">' +
	                        '<div class="flex-shrink-0">' +
	                        '</div><div>' +
	                        '<div class="flex-shrink-0">' +
	                        '<div class="text-xl font-medium text-black">' + value.rq + "</div>" +
	                        '<p class="text-gray-500">Request Date : ' + value.created_date + '</p>' +
	                        '<p class="text-gray-500">Name : ' + value.username + '</p>' +
	                        '<p class="text-gray-500">Status : ' + value.flag_status +
	                        '</p></div></div>');
	                });

                	$("#showdata").append(tr);
            	}
            }
        });
    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function clearDiv() {
        var list = document.getElementById("showdata");
        list.innerHTML = '';
    }

    function viewDetail(id) {
        window.open("{{ env('APP_URL') }}mobile/borrowing-detail?isHybridWebView=true&id="+id);
    }

    async function searchAction() {
        await collapseClick();
        await getBorrowingPending();
    }

    function collapseClick() {
        $("#collapse-search").collapse('toggle');
    }
</script>

</html>
