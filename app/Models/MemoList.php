<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemoList extends Model
{
    public $primaryKey = 'id';
    protected $table = 'oppohr.memo_list';
    protected $connection = MYSQL_MASTER; 
}
