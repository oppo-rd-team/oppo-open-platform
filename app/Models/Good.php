<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    public $primaryKey = 'id';
    protected $table = 'warehouse.good';
    protected $connection = MYSQL_MASTER; 
}
